#!/bin/bash

# Installation directory prefix
PREFIX=/var/userspace/tim/libs

# XCode SDK versions
OSX_VER=10.6
IOS_VER=5.0

# Compilers
X86_CC=/usr/bin/llvm-gcc-4.2
X86_CXX=/usr/bin/llvm-g++-4.2
ARM_CC=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/llvm-gcc-4.2
ARM_CXX=/Developer/Platforms/iPhoneOS.platform/Developer/usr/bin/llvm-g++-4.2

# Boost libraries we want to build (BJam command line parameters)
BJAM_WITH="--with-system --with-test --with-iostreams --with-regex --with-program_options --with-filesystem"

# Dependancy identifiers (used in file and directory names)
BOOST_ID=boost_1_46_1
PROTOBUF_ID=protobuf-2.4.0a
ZLIB_ID=zlib-1.2.5
ASSIMP_ID=assimp--2.0.863-sdk

# Download archive filenames
BOOST_ARCHIVE=$BOOST_ID.tar.bz2
PROTOBUF_ARCHIVE=$PROTOBUF_ID.tar.bz2
ZLIB_ARCHIVE=$ZLIB_ID.tar.gz
ASSIMP_ARCHIVE=$ASSIMP_ID.zip

# URLs for dependancy downloads
BOOST_URL=http://sourceforge.net/projects/boost/files/boost/1.46.1/$BOOST_ARCHIVE/download
PROTOBUF_URL=http://protobuf.googlecode.com/files/$PROTOBUF_ARCHIVE
ZLIB_URL=http://sourceforge.net/projects/libpng/files/zlib/1.2.5/$ZLIB_ARCHIVE/download
ASSIMP_URL=http://sourceforge.net/projects/assimp/files/assimp-2.0/$ASSIMP_ARCHIVE/download

# Construct dependancy roots
BOOST_ROOT=$PREFIX/$BOOST_ID
PROTOBUF_ROOT=$PREFIX/$PROTOBUF_ID
ZLIB_ROOT=$PREFIX/$ZLIB_ID
ASSIMP_ROOT=$PREFIX/$ASSIMP_ID

# Get some current stuff
TIMESTAMP=`date +%s`
START_DIR=$PWD

# Check prefix directory exists
if [ ! -d $PREFIX ]; then
	echo "ERROR: Prefix directory $PREFIX does not exist" 1>&2
	exit 1
fi

# Check CMake is installed
if ! type -P cmake > /dev/null; then     
	echo "ERROR: CMake is not installed, please download CMake before continuing" 1>&2
	exit 1
fi

# Change to prefix directory
cd $PREFIX

# If Boost is not detected then re-download and extract
if [ ! -d $BOOST_ROOT ]; then
	wget $BOOST_URL
	tar zxvf $BOOST_ARCHIVE
	rm $BOOST_ARCHIVE
fi

# If Protobuf is not detected then re-download and extract
if [ ! -d $PROTOBUF_ROOT ]; then
    wget $PROTOBUF_URL
    tar zxvf $PROTOBUF_ARCHIVE
    rm $PROTOBUF_ARCHIVE
fi

# If ZLib is not detected then re-download and extract
if [ ! -d $ZLIB_ROOT ]; then
	wget $ZLIB_URL
	tar zxvf $ZLIB_ARCHIVE
	rm $ZLIB_ARCHIVE
fi

# If Assimp is not detected then re-download and extract
if [ ! -d $ASSIMP_ROOT ]; then
	wget $ASSIMP_URL
	unzip $ASSIMP_ARCHIVE
	rm $ASSIMP_ARCHIVE
fi

# Change to Boost directory
cd $BOOST_ROOT

# Build BJam if required
if [ ! -f bjam ]; then ./bootstrap.sh --prefix=$BOOST_ROOT; fi

# Link to some missing headers for iOS build (?!)
ln -sf /Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator$IOS_VER.sdk/usr/include/bzlib.h
ln -sf /Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator$IOS_VER.sdk/usr/include/crt_externs.h

# Backup old Jam configuration file
if [ -f project-config.jam ]; then mv project-config.jam ~project-config.jam.$TIMESTAMP; fi

# Create Jam configuration for using LLVM compiler back-end
echo "
using darwin : $OSX_VER : $X86_CXX : <striper> : <architecture>x86 <address-model>32_64 ;
using darwin : $IOS_VER~iphone : $ARM_CXX : <striper> : <architecture>arm <target-os>iphone ;
" > project-config.jam

# Build Boost for Max OS X and iOS simulator (i386 and x86_64 architectures)
./bjam --toolset=darwin --prefix=$BOOST_ROOT/build/x86 $BJAM_WITH architecture=x86 address-model=32_64 macosx-version=$OSX_VER variant=release link=static -sZLIB_SOURCE=$ZLIB_ROOT install

# Build Boost for iOS
./bjam --toolset=darwin --prefix=$BOOST_ROOT/build/arm $BJAM_WITH architecture=arm target-os=iphone macosx-version=iphone-$IOS_VER define=_LITTLE_ENDIAN variant=release link=static -sZLIB_SOURCE=$ZLIB_ROOT install

# Combine 32_64 and arm libs into universal fat files
mkdir -p lib
lipo -create build/x86/lib/libboost_system.a              build/arm/lib/libboost_system.a              -output lib/libboost_system.a
lipo -create build/x86/lib/libboost_iostreams.a           build/arm/lib/libboost_iostreams.a           -output lib/libboost_iostreams.a
lipo -create build/x86/lib/libboost_prg_exec_monitor.a    build/arm/lib/libboost_prg_exec_monitor.a    -output lib/libboost_prg_exec_monitor.a
lipo -create build/x86/lib/libboost_program_options.a     build/arm/lib/libboost_program_options.a     -output lib/libboost_program_options.a
lipo -create build/x86/lib/libboost_filesystem.a          build/arm/lib/libboost_filesystem.a          -output lib/libboost_filesystem.a
lipo -create build/x86/lib/libboost_regex.a               build/arm/lib/libboost_regex.a               -output lib/libboost_regex.a
lipo -create build/x86/lib/libboost_test_exec_monitor.a   build/arm/lib/libboost_test_exec_monitor.a   -output lib/libboost_test_exec_monitor.a
lipo -create build/x86/lib/libboost_unit_test_framework.a build/arm/lib/libboost_unit_test_framework.a -output lib/libboost_unit_test_framework.a
lipo -create build/x86/lib/libboost_wserialization.a      build/arm/lib/libboost_wserialization.a      -output lib/libboost_wserialization.a
lipo -create build/x86/lib/libboost_zlib.a                build/arm/lib/libboost_zlib.a                -output lib/libboost_zlib.a

# Copy headers
cp -Rf build/x86/include include

# Change to Protobuf directory
cd $PROTOBUF_ROOT

# x86_64 static libs
./configure --prefix=$PROTOBUF_ROOT/build/x86_64 --disable-shared --enable-static CC=$X86_CC CXX=$X86_CXX CFLAGS="-arch x86_64" CXXFLAGS="-arch x86_64" LDFLAGS="-arch x86_64"
make
make install
make clean

# i386 static libs
./configure --prefix=$PROTOBUF_ROOT/build/x86_32 --disable-shared --enable-static CC=$X86_CC CXX=$X86_CXX CFLAGS="-arch i386" CXXFLAGS="-arch i386" LDFLAGS="-arch i386"
make
make install
make clean

# TODO: http://www.mail-archive.com/protobuf@googlegroups.com/msg02776.html
# arm static libs
#./configure --prefix=$PROTOBUF_ROOT/build/arm --disable-shared --enable-static CC=$ARM_CC CXX=$ARM_CXX CFLAGS="-arch arm" CXXFLAGS="-arch arm" LDFLAGS="-arch arm"
#make
#make install
#make clean

# Create universal protoc and install a symlink to it (x86 only)
mkdir -p bin
lipo -create build/x86_64/bin/protoc build/x86_32/bin/protoc -output bin/protoc

if [ -d ~/bin ]; then ln -sf $PROTOBUF_ROOT/bin/protoc ~/bin; fi

# Create universal static libs (TODO: x86 and ARM)
mkdir -p lib
#lipo -create build/x86_64/lib/libprotobuf.a build/x86_32/lib/libprotobuf.a build/arm/lib/libprotobuf.a -output lib/libprotobuf.a
lipo -create build/x86_64/lib/libprotobuf.a build/x86_32/lib/libprotobuf.a -output lib/libprotobuf.a

# Copy headers
cp -Rf build/x86_32/include include

# Build Assimp
cd $ASSIMP_ROOT
cmake -DCMAKE_OSX_ARCHITECTURES="i386;x86_64" -DBOOST_ROOT=$BOOST_ROOT
make

# Change back to original directory before we exit
cd $START_DIR


