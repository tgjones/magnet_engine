@echo off

rem ** THIS HAS NOT BEEN TESTED **

call "%VS100COMNTOOLS%"\vsvars32.bat

set MSVC_VER=10.0
set BOOST_ROOT=C:\Development\boost_1_46_1
set COLLADA_ROOT=C:\Development\collada-dom-2.3.1

cd %BOOST_ROOT%\lib

copy libboost_filesystem-vc100-mt-1_46_1.lib boost_filesystem.lib
copy libboost_system-vc100-mt-1_46_1.lib boost_system.lib

cd %COLLADA_ROOT% 

cmake -DBOOST_ROOT=%BOOST_ROOT%
msbuild ALL_BUILD.vcxproj /p:Configuration=Release /p:Platform=Win32
