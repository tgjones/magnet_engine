@echo off

set MSVC_VER=10.0
set BOOST_ROOT=C:\Development\boost_1_46_1
set ZLIB_ROOT=C:\Development\zlib-1.2.5
set BJAM_WITH=--with-system --with-test --with-iostreams --with-regex --with-program_options --with-filesystem

cd %BOOST_ROOT% 

if not exist bjam.exe call bootstrap.bat

bjam --prefix=%BOOST_ROOT% %BJAM_WITH% toolset=msvc-%MSVC_VER% variant=release threading=multi link=static runtime-link=shared runtime-debugging=off define=_ITERATOR_DEBUG_LEVEL=0 -sZLIB_SOURCE=%ZLIB_ROOT% install
bjam --prefix=%BOOST_ROOT% %BJAM_WITH% toolset=msvc-%MSVC_VER% variant=debug threading=multi link=static runtime-link=shared runtime-debugging=on define=_ITERATOR_DEBUG_LEVEL=0 -sZLIB_SOURCE=%ZLIB_ROOT% install

cd include
move boost-1_46_1\boost .
rmdir boost-1_46_1

cd /d %0\.. 