@echo off
call "%VS100COMNTOOLS%"\vsvars32.bat

set MSVC_VER=10.0
set PROTOBUF_ROOT=C:\Development\protobuf-2.4.0a

cd %PROTOBUF_ROOT%

devenv /Upgrade vsprojects\protobuf.sln

msbuild vsprojects\libprotobuf.vcxproj /p:Configuration=Release /p:Platform=Win32
msbuild vsprojects\libprotoc.vcxproj /p:Configuration=Release /p:Platform=Win32
msbuild vsprojects\protoc.vcxproj /p:Configuration=Release /p:Platform=Win32

mkdir lib > NUL 2>&1
mkdir bin > NUL 2>&1
mkdir include > NUL 2>&1

xcopy /Y vsprojects\Release\*.lib lib
xcopy /Y vsprojects\Release\*.exe bin
xcopy /S /Y src\*.h include