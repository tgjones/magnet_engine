Magnet Game Engine
==================

Hobby game engine project.  Much work went into this during 2010, but ultimately it proved an awesome learning 
experience but not much else.  Re-inventing the wheel doesn't begin to cover it!

When I stopped working on this, I had successfully completed the following milestones:

 - DirectX9 rendering on Windows (no textures)
 - OpenGL rendering on Windows (no textures)
 - OpenGL ES rendering on XCode iPhone Simulator (no textures)
 - Real-time rendering switching (DirectX/OpenGL) on Windows
 - XInput2 implemented and test app written that worked with XBox 360 controller
 - Simple PhysX rigid-body integration and test app written
 - Bespoke mesh format developed (simply a serialised class)
 - Used the Assimp library to write a tool that converted common mesh formats to bespoke format
