#ifndef _MAGNET_UTIL_EXCEPTIONS_H
#define _MAGNET_UTIL_EXCEPTIONS_H

#include <string>

namespace Magnet
{
    
    // Error codes in use
    enum
    {   
        ERR_CODE_UNSPECIFIED = 0,
        
        // Runtime errors
        ERR_CONFIG_FILE_UNREADABLE = 1000 // ConfigurationManager::parseConfigFile()
        
        // Logic errors
    };

    // Base-class exception
	class Exception
	{
	public:

		inline Exception(const std::string& reason) : m_reason(reason), m_code(0) {}
        
        inline Exception(const std::string& reason, int32_t code) : m_reason(reason), m_code(code) {}

		virtual inline ~Exception() {}

		inline const std::string& getReason() { return m_reason; }
        
        inline const int32_t getCode() { return m_code; }

	private:

		std::string m_reason;
        
        int32_t m_code;
	};
    
    // RuntimeError exception
	class RuntimeError : public Exception
	{
	public:

		inline RuntimeError(const std::string& reason) : Exception(reason) {}

		inline RuntimeError(const std::string& reason, int32_t code) : Exception(reason, code) {}        
        
		virtual inline ~RuntimeError() {}
    };
    
    // LogicError exception
    class LogicError : public Exception
	{
	public:

		inline LogicError(const std::string& reason) : Exception(reason) {}
        
        inline LogicError(const std::string& reason, int32_t code) : Exception(reason, code) {}

		virtual inline ~LogicError() {}
	};
}

#endif