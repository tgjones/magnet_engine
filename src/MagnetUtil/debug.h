#ifndef _MAGNET_UTIL_DEBUG_H
#define _MAGNET_UTIL_DEBUG_H

#ifdef _MAG_DEBUG

	//
	// Prerequisite headers for debug macros
	//
	
	#include <string>
	#include <sstream>
	
	#if defined(_MAG_PLATFORM_WIN32)

		#include <Windows.h>
//		#include <DXErr.h>

	#elif defined(__APPLE__)

		#import <Foundation/Foundation.h>

	#else

		#include <cstdio>

	#endif


	// 
	// Inline breakpoint
	//

	#if defined(_MAG_PLATFORM_WIN32)
		
		// Breaks nicely into the MSVC debugger
		#define _DEBUG_BREAK() DebugBreak() 

	#else

		// Cannot directly break on other platforms
		#define _DEBUG_BREAK() (void(0)) 

	#endif


	//
	// Debug output
	//

	#if defined(_MAG_PLATFORM_WIN32)

		// Outputs nicely to MSVC debugger
		#define _DEBUG_OUTPUT(msg) OutputDebugString(msg)

	#elif defined(__APPLE__)
		
		// Outputs to the iOS or OSX log
		#define _DEBUG_OUTPUT(msg) NSLog(@"%@", [NSString stringWithCString:msg \
			encoding:NSUTF8StringEncoding]) 

	#else

		// Outputs to stdout
		#define _DEBUG_OUTPUT(msg) printf(msg)

	#endif

	//
	// Format debug output
	//

	#if defined (_MSC_VER)

		// 'Double-clickable' format for MSVC debug window
		inline std::string _debugFormatMsg(const std::string& prefix, const std::string& msg, 
			const std::string& file, const int line)
		{
			std::stringstream ss;
			ss << file << "(" << line << "): " << prefix << " " << msg << std::endl;
			return ss.str();
		}

	#else

		// Shorter format for everything else
		inline std::string _debugFormatMsg(const std::string& prefix, const std::string& msg, 
			const std::string& file, const int line)
		{
			std::string fileName = file.substr(file.find_last_of('/') + 1, file.npos);
			std::stringstream ss;
			ss << prefix << " " << fileName << "(" << line << "): " << msg << std::endl;
			return ss.str();			
		}

	#endif

	//
	// Logging macros
	//

	#define LOG_INFO(msg)    _DEBUG_OUTPUT(_debugFormatMsg("[INFO]", msg, __FILE__, __LINE__).c_str())
	#define LOG_WARNING(msg) _DEBUG_OUTPUT(_debugFormatMsg("[WARN]", msg, __FILE__, __LINE__).c_str())
	#define LOG_DEBUG(msg)   _DEBUG_OUTPUT(_debugFormatMsg("[DEBG]", msg, __FILE__, __LINE__).c_str())
	#define LOG_ERROR(msg)   _DEBUG_OUTPUT(_debugFormatMsg("[ERRO]", msg, __FILE__, __LINE__).c_str())
	#define LOG_FATAL(msg)   _DEBUG_OUTPUT(_debugFormatMsg("[FATL]", msg, __FILE__, __LINE__).c_str())

	// 
	// Logging 'if' wrappers
	//

	#define LOG_INFO_IF(expr, msg)    if (expr) { LOG_INFO(msg); }
	#define LOG_WARNING_IF(expr, msg) if (expr) { LOG_WARNING(msg); }
	#define LOG_ERROR_IF(expr, msg)   if (expr) { LOG_ERROR(msg); }
	#define LOG_FATAL_IF(expr, msg)   if (expr) { LOG_FATAL(msg); }
	#define LOG_DEBUG_IF(expr, msg)   if (expr) { LOG_DEBUG(msg); }

	//
	// Assertion failed exception
	//

	namespace Magnet
	{
		class AssertionFailedError
		{
		public:
			
			inline AssertionFailedError(const std::string& expr, const std::string& file, const unsigned int line)
				: m_expr(expr), m_file(file), m_line(line) {}
			
			inline const std::string& getExpression() { return m_expr; }
			
			inline const std::string& getFile() { return m_file; }
			
			inline unsigned int getLine() { return m_line; }
			
		private:
			
			std::string m_expr;
			
			std::string m_file;
			
			unsigned int m_line;
		};
	}

	//
	// Simple assertion
	//

	#define ASSERT(expr) \
		if (expr) {} else \
		{ \
			std::stringstream ss; \
			ss << "Assertion failed on expression \"" << #expr << "\""; \
			LOG_FATAL(ss.str().c_str()); \
			_DEBUG_BREAK(); \
			throw ::Magnet::AssertionFailedError(#expr, __FILE__, __LINE__); \
		}

	//
	// Assertion with message
	//

	#define ASSERT_MSG(expr, msg) \
		if (expr) {} else \
		{ \
			std::stringstream ss; \
			ss << "Assertion failed on expression \"" << #expr << "\": " << msg; \
			LOG_FATAL(ss.str().c_str()); \
			_DEBUG_BREAK(); \
			throw ::Magnet::AssertionFailedError(#expr, __FILE__, __LINE__); \
		}

	#ifdef _MAG_PLATFORM_WIN32

		// 
		// HRESULT checker
		//

		#define HR(expr) \
			{ \
				HRESULT hr = expr; \
				if (FAILED(hr)) \
				{ \
					std::stringstream ss; \
					ss << "Error code " << DXGetErrorString(hr) << " returned from expression \"" << #expr << "\""; \
					LOG_FATAL(ss.str().c_str()); \
					_DEBUG_BREAK(); \
					throw ::Magnet::AssertionFailedError("FAILED(hr)", __FILE__, __LINE__); \
				} \
			}

	#endif

#else

	// Void out everything
	#define DEBUG_BREAK()             ((void)0)
	#define DEBUG_OUTPUT(msg)         ((void)0)

	#define LOG_INFO(msg)             ((void)0)
	#define LOG_WARNING(msg)          ((void)0)
	#define LOG_DEBUG(msg)            ((void)0)
	#define LOG_ERROR(msg)            ((void)0)
	#define LOG_FATAL(msg)            ((void)0)

	#define LOG_INFO_IF(expr, msg)    ((void)0)
	#define LOG_WARNING_IF(expr, msg) ((void)0)
	#define LOG_ERROR_IF(expr, msg)   ((void)0)
	#define LOG_FATAL_IF(expr, msg)   ((void)0)
	#define LOG_DEBUG_IF(expr, msg)   ((void)0)

	#define ASSERT(expr)              ((void)0)
	#define ASSERT_MSG(expr, msg)     ((void)0)

	// Remove HRESULT check but leave expression
	#define HR(expr) expr;

#endif
#endif