#include "external.h"
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/debug.h>
#include <MagnetUtil/global.h>

Magnet::Vector2::Vector2() : x(0.0f), y(0.0f)
{
}

Magnet::Vector2::Vector2(const Vector2& v) : x(v.x), y(v.y)
{
}

Magnet::Vector2::Vector2(const float x, const float y) : x(x), y(y)
{
}

Magnet::Vector2::Vector2(const float data[2]) : x(data[0]), y(data[1])
{
}

float& Magnet::Vector2::operator [] (const unsigned int n)
{
	ASSERT((n >= 0) && (n < 2)); 
	return *(&x + n); 
}

float* Magnet::Vector2::getDataPtr()
{
	return &x;
}

bool Magnet::Vector2::operator == (const Vector2& v) const
{
	return ((std::abs(this->x - v.x) <= EPSILON) &&
		(std::abs(this->y - v.y) <= EPSILON));
}

bool Magnet::Vector2::operator != (const Vector2& v) const
{
	return !(*this == v);
}

Magnet::Vector2 Magnet::Vector2::operator - () const
{
	return Vector2(-this->x, -this->y);
}

Magnet::Vector2 Magnet::Vector2::operator + (const Vector2& v) const
{
	return Vector2(this->x + v.x, this->y + v.y);
}

Magnet::Vector2& Magnet::Vector2::operator += (const Vector2& v)
{
	this->x += v.x;
	this->y += v.y;
	return *this;
}

Magnet::Vector2 Magnet::Vector2::operator + (const float scalar) const
{
	return Vector2(this->x + scalar, this->y + scalar);
}

Magnet::Vector2& Magnet::Vector2::operator += (const float scalar)
{
	this->x += scalar;
	this->y += scalar;
	return *this;
}

Magnet::Vector2 Magnet::Vector2::operator - (const Vector2& v) const
{
	return Vector2(this->x - v.x, this->y - v.y);
}

Magnet::Vector2& Magnet::Vector2::operator -= (const Vector2& v)
{
	this->x -= v.x;
	this->y -= v.y;
	return *this;
}

Magnet::Vector2 Magnet::Vector2::operator - (const float scalar) const
{
	return Vector2(this->x - scalar, this->y - scalar);
}

Magnet::Vector2& Magnet::Vector2::operator -= (const float scalar)
{
	this->x -= scalar;
	this->y -= scalar;
	return *this;
}

Magnet::Vector2 Magnet::Vector2::operator * (const float scalar) const
{
	return Magnet::Vector2(this->x * scalar, this->y * scalar);
}

Magnet::Vector2& Magnet::Vector2::operator *= (const float scalar)
{
	this->x *= scalar;
	this->y *= scalar;
	return *this;
}

float Magnet::Vector2::dot(const Magnet::Vector2& v) const
{
	return (this->x * v.x) + (this->y * v.y);
}

float Magnet::Vector2::operator * (const Magnet::Vector2& v) const
{
	return this->dot(v);
}

Magnet::Vector2& Magnet::Vector2::normalise()
{
	float magnitude = std::sqrt(dot(*this));
	this->x /= magnitude;
	this->y /= magnitude;
	return *this;
}

