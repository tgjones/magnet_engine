#include "external.h"
#include <MagnetUtil/Matrix4x4.h>
#include <MagnetUtil/Vector4.h>
#include <MagnetUtil/Vector3.h>
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/debug.h>
#include <MagnetUtil/global.h>

Magnet::Matrix4x4::Matrix4x4() :
	_11(0.0f), _12(0.0f), _13(0.0f), _14(0.0f),
	_21(0.0f), _22(0.0f), _23(0.0f), _24(0.0f),
	_31(0.0f), _32(0.0f), _33(0.0f), _34(0.0f),
	_41(0.0f), _42(0.0f), _43(0.0f), _44(0.0f)
{
}

Magnet::Matrix4x4::Matrix4x4(const Matrix4x4& m) :
	_11(m._11), _12(m._12), _13(m._13), _14(m._14),
	_21(m._21), _22(m._22), _23(m._23), _24(m._24),
	_31(m._31), _32(m._32), _33(m._33), _34(m._34),
	_41(m._41), _42(m._42), _43(m._43), _44(m._44) 
{
}

Magnet::Matrix4x4::Matrix4x4(
	const float _11, const float _12, const float _13, const float _14,
	const float _21, const float _22, const float _23, const float _24,
	const float _31, const float _32, const float _33, const float _34,
	const float _41, const float _42, const float _43, const float _44)	:
	_11(_11), _12(_12), _13(_13), _14(_14),
	_21(_21), _22(_22), _23(_23), _24(_24),
	_31(_31), _32(_32), _33(_33), _34(_34),
	_41(_41), _42(_42), _43(_43), _44(_44) 
{
}

Magnet::Matrix4x4::Matrix4x4(const float data[16]) :
	_11(data[0]),  _12(data[1]),  _13(data[2]),  _14(data[3]),
	_21(data[4]),  _22(data[5]),  _23(data[6]),  _24(data[7]),
	_31(data[8]),  _32(data[9]),  _33(data[10]), _34(data[11]),
	_41(data[12]), _42(data[13]), _43(data[14]), _44(data[15])
{
}

float& Magnet::Matrix4x4::operator [] (const unsigned int n)
{
	ASSERT((n >= 0) && (n < 16)); 
	return *(&_11 + n);
}

float * Magnet::Matrix4x4::getDataPtr()
{
	return &_11;
}

bool Magnet::Matrix4x4::operator == (const Matrix4x4& m) const
{
	return ((std::abs(_11 - m._11) <= EPSILON) &&
		(std::abs(_12 - m._12) <= EPSILON) &&
		(std::abs(_13 - m._13) <= EPSILON) &&
		(std::abs(_14 - m._14) <= EPSILON) &&
		(std::abs(_21 - m._21) <= EPSILON) &&
		(std::abs(_22 - m._22) <= EPSILON) &&
		(std::abs(_23 - m._23) <= EPSILON) &&
		(std::abs(_24 - m._24) <= EPSILON) &&
		(std::abs(_31 - m._31) <= EPSILON) &&
		(std::abs(_32 - m._32) <= EPSILON) &&
		(std::abs(_33 - m._33) <= EPSILON) &&
		(std::abs(_34 - m._34) <= EPSILON) &&
		(std::abs(_41 - m._41) <= EPSILON) &&
		(std::abs(_42 - m._42) <= EPSILON) &&
		(std::abs(_43 - m._43) <= EPSILON) &&
		(std::abs(_44 - m._44) <= EPSILON));
}

bool Magnet::Matrix4x4::operator != (const Matrix4x4& m) const
{
	return !(*this == m);
}

Magnet::Matrix4x4 Magnet::Matrix4x4::operator - () const
{			
	return Matrix4x4(
		-_11, -_12, -_13, -_14,
		-_21, -_22, -_23, -_24,
		-_31, -_32, -_33, -_34,
		-_41, -_42, -_43, -_44);		
}

Magnet::Matrix4x4& Magnet::Matrix4x4::identity()
{
	_11 = 1.0f; _12 = 0.0f; _13 = 0.0f; _14 = 0.0f;
	_21 = 0.0f; _22 = 1.0f; _23 = 0.0f; _24 = 0.0f;
	_31 = 0.0f; _32 = 0.0f; _33 = 1.0f; _34 = 0.0f;
	_41 = 0.0f; _42 = 0.0f; _43 = 0.0f; _44 = 1.0f;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createIdentity()
{
	return Matrix4x4().identity();
}

Magnet::Matrix4x4& Magnet::Matrix4x4::transpose()
{
	Matrix4x4 m = *this;

	/*_11 = m._11;*/ _12 = m._21; _13 = m._31; _14 = m._41;
	_21 = m._12; /*_22 = m._22;*/ _23 = m._32; _24 = m._42;
	_31 = m._13; _32 = m._23; /*_33 = m._33;*/ _34 = m._43;
	_41 = m._14; _42 = m._24; _43 = m._34; /*_44 = m._44;*/

	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::operator * (const Matrix4x4& m) const
{
	Matrix4x4 result;

	result._11 = (_11 * m._11) + (_12 * m._21) + (_13 * m._31) + (_14 * m._41);
	result._12 = (_11 * m._12) + (_12 * m._22) + (_13 * m._32) + (_14 * m._42);
	result._13 = (_11 * m._13) + (_12 * m._23) + (_13 * m._33) + (_14 * m._43);
	result._14 = (_11 * m._14) + (_12 * m._24) + (_13 * m._34) + (_14 * m._44);

	result._21 = (_21 * m._11) + (_22 * m._21) + (_23 * m._31) + (_24 * m._41);
	result._22 = (_21 * m._12) + (_22 * m._22) + (_23 * m._32) + (_24 * m._42);
	result._23 = (_21 * m._13) + (_22 * m._23) + (_23 * m._33) + (_24 * m._43);
	result._24 = (_21 * m._14) + (_22 * m._24) + (_23 * m._34) + (_24 * m._44);

	result._31 = (_31 * m._11) + (_32 * m._21) + (_33 * m._31) + (_34 * m._41);
	result._32 = (_31 * m._12) + (_32 * m._22) + (_33 * m._32) + (_34 * m._42);
	result._33 = (_31 * m._13) + (_32 * m._23) + (_33 * m._33) + (_34 * m._43);
	result._34 = (_31 * m._14) + (_32 * m._24) + (_33 * m._34) + (_34 * m._44);

	result._41 = (_41 * m._11) + (_42 * m._21) + (_43 * m._31) + (_44 * m._41);
	result._42 = (_41 * m._12) + (_42 * m._22) + (_43 * m._32) + (_44 * m._42);
	result._43 = (_41 * m._13) + (_42 * m._23) + (_43 * m._33) + (_44 * m._43);
	result._44 = (_41 * m._14) + (_42 * m._24) + (_43 * m._34) + (_44 * m._44);

	return result;
}

Magnet::Matrix4x4& Magnet::Matrix4x4::operator *= (const Matrix4x4& m)
{
	Matrix4x4 result;

	result._11 = (_11 * m._11) + (_12 * m._21) + (_13 * m._31) + (_14 * m._41);
	result._12 = (_11 * m._12) + (_12 * m._22) + (_13 * m._32) + (_14 * m._42);
	result._13 = (_11 * m._13) + (_12 * m._23) + (_13 * m._33) + (_14 * m._43);
	result._14 = (_11 * m._14) + (_12 * m._24) + (_13 * m._34) + (_14 * m._44);

	result._21 = (_21 * m._11) + (_22 * m._21) + (_23 * m._31) + (_24 * m._41);
	result._22 = (_21 * m._12) + (_22 * m._22) + (_23 * m._32) + (_24 * m._42);
	result._23 = (_21 * m._13) + (_22 * m._23) + (_23 * m._33) + (_24 * m._43);
	result._24 = (_21 * m._14) + (_22 * m._24) + (_23 * m._34) + (_24 * m._44);

	result._31 = (_31 * m._11) + (_32 * m._21) + (_33 * m._31) + (_34 * m._41);
	result._32 = (_31 * m._12) + (_32 * m._22) + (_33 * m._32) + (_34 * m._42);
	result._33 = (_31 * m._13) + (_32 * m._23) + (_33 * m._33) + (_34 * m._43);
	result._34 = (_31 * m._14) + (_32 * m._24) + (_33 * m._34) + (_34 * m._44);

	result._41 = (_41 * m._11) + (_42 * m._21) + (_43 * m._31) + (_44 * m._41);
	result._42 = (_41 * m._12) + (_42 * m._22) + (_43 * m._32) + (_44 * m._42);
	result._43 = (_41 * m._13) + (_42 * m._23) + (_43 * m._33) + (_44 * m._43);
	result._44 = (_41 * m._14) + (_42 * m._24) + (_43 * m._34) + (_44 * m._44);

	std::memcpy((void*)&_11, (void*)&result._11, 16 * sizeof(float));

	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::operator * (const float scalar) const
{
	Matrix4x4 result;

    // Parallelize using OpenMP
    #ifdef _MAG_USE_OPENMP
	#pragma omp parallel
    #endif
    
	for (int i = 0; i < 16; i++)
		*(&result._11 + i) = *(&_11 + i) * scalar;

	return result;
}

Magnet::Matrix4x4& Magnet::Matrix4x4::operator *= (const float scalar)
{
    // Parallelize using OpenMP
    #ifdef _MAG_USE_OPENMP
	#pragma omp parallel
    #endif
    
	for (int i = 0; i < 16; i++)
		*(&_11 + i) *= scalar;

	return *this;
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setRotationX(const float theta)
{
	this->identity();
	float s = std::sin(theta);
	float c = std::cos(theta);
	_22 = c;
	_23 = s;
	_32 = -s;
	_33 = c;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createRotationX(const float theta)
{
	return Matrix4x4().setRotationX(theta);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setRotationY(const float theta)
{
	this->identity();
	float s = std::sin(theta);
	float c = std::cos(theta);
	_11 = c;
	_13 = -s;
	_31 = s;
	_33 = c;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createRotationY(const float theta)
{
	return Matrix4x4().setRotationY(theta);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setRotationZ(const float theta)
{
	this->identity();
	float s = std::sin(theta);
	float c = std::cos(theta);
	_11 = c;
	_12 = s;
	_21 = -s;
	_22 = c;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createRotationZ(const float theta)
{
	return Matrix4x4().setRotationZ(theta);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setRotation(const Vector3& rotation)
{
	this->identity();
	*this *= createRotationX(rotation.x);
	*this *= createRotationY(rotation.y);
	*this *= createRotationZ(rotation.z);
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createRotation(const Vector3& rotation)
{
	return Matrix4x4().setRotation(rotation);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setRotation(const float xTheta, const float yTheta, const float zTheta)
{
	this->identity();
	*this *= createRotationX(xTheta);
	*this *= createRotationY(yTheta);
	*this *= createRotationZ(zTheta);
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createRotation(const float xTheta, const float yTheta, const float zTheta)
{
	return Matrix4x4().setRotation(xTheta, yTheta, zTheta);
}

/*
TODO: Implement these methods and add to test suite

Magnet::Matrix4x4& Magnet::Matrix4x4::setArbitraryRotation(const float theta, const Vector3 v)
{
	this->identity();
	float s = std::sin(theta);
	float c = std::cos(theta);
	_11 = c + (v.x * v.x) * (1.0f - c);
	_12 = v.x * v.y * (1.0f - c) + v.z * s;
	_13 = v.x * v.z * (1.0f - c) - v.y * s;
	_21 = v.x * v.y * (1.0f - c) - v.z * s;
	_22 = c + (v.y * v.y) * (1.0f - c);
	_23 = v.y * v.z * (1.0f - c) + v.x * s;
	_31 = v.x * v.z * (1.0f - c) + v.y * s;
	_32 = v.y * v.z * (1.0f - c) - v.x * s;
	_33 = c + (v.z * v.z) * (1.0f - c);
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createArbitraryRotation(const float theta, const Vector3 v)
{
	return Matrix4x4().setArbitraryRotation(theta, v);
}
*/

Magnet::Matrix4x4& Magnet::Matrix4x4::setScale(const Vector3& scale)
{
	this->identity();
	_11 = scale.x;
	_22 = scale.y;
	_33 = scale.z;
	_44 = 1.0f;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createScale(const Vector3& scale)
{
	return Matrix4x4().setScale(scale);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setScale(const float x, const float y, const float z)
{
	this->identity();
	_11 = x;
	_22 = y;
	_33 = z;
	_44 = 1.0f;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createScale(const float x, const float y, const float z)
{
	return Matrix4x4().setScale(x, y, z);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setInverseScale(const Vector3& inverseScale)
{
	// UNTESTED
	this->identity();
	_11 = 1.0f / inverseScale.x;
	_22 = 1.0f / inverseScale.y;
	_33 = 1.0f / inverseScale.z;
	_44 = 1.0f;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createInverseScale(const Vector3& inverseScale)
{
	return Matrix4x4().setInverseScale(inverseScale);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setInverseScale(const float x, const float y, const float z)
{
	// UNTESTED
	this->identity();
	_11 = 1.0f / x;
	_22 = 1.0f / y;
	_33 = 1.0f / z;
	_44 = 1.0f;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createInverseScale(const float x, const float y, const float z)
{
	return Matrix4x4().setInverseScale(x, y, z);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setTranslation(const Vector3& translation)
{
	this->identity();
	_41 = translation.x;
	_42 = translation.y;
	_43 = translation.z;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createTranslation(const Vector3& translation)
{
	return Matrix4x4().setTranslation(translation);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setTranslation(const float x, const float y, const float z)
{
	this->identity();
	_41 = x;
	_42 = y;
	_43 = z;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createTranslation(const float x, const float y, const float z)
{
	return Matrix4x4().setTranslation(x, y, z);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setInverseTranslation(const Vector3& inverseTranslation)
{
	// UNTESTED
	this->identity();
	_41 = -inverseTranslation.x;
	_42 = -inverseTranslation.y;
	_43 = -inverseTranslation.z;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createInverseTranslation(const Vector3& inverseTranslation)
{
	return Matrix4x4().setInverseTranslation(inverseTranslation);
}

Magnet::Matrix4x4& Magnet::Matrix4x4::setInverseTranslation(const float x, const float y, const float z)
{
	// UNTESTED
	this->identity();
	_41 = -x;
	_42 = -y;
	_43 = -z;
	return *this;
}

Magnet::Matrix4x4 Magnet::Matrix4x4::createInverseTranslation(const float x, const float y, const float z)
{
	return Matrix4x4().setInverseTranslation(x, y, z);
}