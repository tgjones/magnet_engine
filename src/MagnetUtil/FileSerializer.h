#ifndef _MAGNET_UTIL_FILE_H
#define _MAGNET_UTIL_FILE_H

#include <MagnetUtil/types.h>
#include <MagnetUtil/external.h>
#include <MagnetUtil/exceptions.h>

namespace Magnet
{
	class FileSerializer
	{
	public:
		virtual inline ~FileSerializer() {}

		virtual void read(const fs::path& filePath) = 0;
		virtual void write(const fs::path& filePath, const uint8_t compressionLevel) = 0;
	};
}

#endif