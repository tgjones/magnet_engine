#ifndef _MAGNET_CORE_COMMON_H
#define _MAGNET_CORE_COMMON_H

// common.h is a convenience header which itself includes all the headers that comprise the Magnet
// classes and other definitions. The headers are included in the correct order, which is probably
// the toughest part of all. The disadvantage of using this header instead of the specific ones your
// source depends on is slightly increased compile times, however, the benefits of using it in 
// general far out-weigh this slight inconveniance, as you can see below...!

// MagnetUtil/Core headers
#include <MagnetUtil/config.h>
#include <MagnetCore/external.h>
#include <MagnetUtil/debug.h>
#include <MagnetCore/global.h>
#include <MagnetCore/SubsystemFactory.h>
#include <MagnetCore/Engine.h>
#include <MagnetUtil/types.h>
#include <MagnetUtil/shapes.h>
#include <MagnetUtil/exceptions.h>
#include <MagnetCore/Subsystem.h>
#include <MagnetCore/Application.h>
#include <MagnetCore/ConfigurationManager.h>
#include <MagnetUtil/FileSerializer.h>
#include <MagnetUtil/ModelFileSerializer.h>
#include <MagnetUtil/TextureFileSerializer.h>
#include <MagnetUtil/Handle.h>
#include <MagnetUtil/HandleController.h>
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/Vector3.h>
#include <MagnetUtil/Vector4.h>
#include <MagnetUtil/Matrix4x4.h>
#include <MagnetUtil/Variant.h>
#include <MagnetUtil/Timer.h>
#include <MagnetCore/handles.h>
#include <MagnetCore/Vertex.h>
#include <MagnetCore/Mesh.h>
#include <MagnetCore/MediaManager.h>
#include <MagnetCore/MediaFile.h>
#include <MagnetCore/TextureFile.h>
#include <MagnetCore/ModelFile.h>
#include <MagnetCore/MediaAsset.h>
#include <MagnetCore/Entity.h>
#include <MagnetCore/GameWorld.h>
#include <MagnetCore/CameraEntity.h>
#include <MagnetCore/SpriteAnimation.h>
#include <MagnetCore/SpriteAsset.h>
#include <MagnetCore/SpriteEntity.h>
#include <MagnetCore/ActorAsset.h>
#include <MagnetCore/ActorEntity.h>
#include <MagnetCore/DirectActorEntity.h>
#include <MagnetCore/PhysicalActorEntity.h>
#include <MagnetCore/Event.h>
#include <MagnetCore/EventReceiver.h>
#include <MagnetCore/EventManager.h>
#include <MagnetCore/Plane.h>
#include <MagnetCore/RigidBody.h>
#include <MagnetCore/PhysicsEngine.h>
#include <MagnetCore/PhysXPhysicsEngine.h>
#include <MagnetCore/LightEntity.h>
#include <MagnetCore/ViewPort.h>
#include <MagnetCore/Renderer.h>
#include <MagnetCore/OpenGLRenderer.h>
#include <MagnetCore/InputEngine.h>

// Protobuf output headers (move these to external.h?)
#include <support/proto/cpp_out/simple_types.pb.h>
#include <support/proto/cpp_out/file_formats.pb.h>

#endif