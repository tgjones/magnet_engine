
#ifndef _MAGNET_UTIL_HANDLE_CONTROLLER_H
#define _MAGNET_UTIL_HANDLE_CONTROLLER_H

#include <vector>
#include <MagnetUtil/types.h>

namespace Magnet
{

	#pragma region Main non-pointer type HandleController

	template <typename T, typename H>
	class HandleController
	{
	public:

		HandleController() {}

		~HandleController() { clear(); }

		// Dispose of everything
		void clear()
		{
			m_data.clear();
			m_freeList.clear();
			m_magicNumbers.clear();
		}

		// Acquire a handle
		T* acquire(H& h)
		{
			uint32_t index;
			
			// If there are no free slots
			if (m_freeList.empty())
			{
				index = m_magicNumbers.size();
				h.attach(index);
				m_data.push_back(T());
				m_magicNumbers.push_back(h.getMagic());
			}

			// If there are free slots
			else
			{
				index = m_freeList.back();
				h.attach(index);
				m_freeList.pop_back();
				m_magicNumbers[index] = h.getMagic();
			}

			// Return pointer to data
			return &(*(m_data.begin() + index));
		}

		// Release a handle
		void release(H h)
		{
			uint32_t index = h.getIndex();

			ASSERT(index < m_data.size());

			// Stale handle check
			if (m_magicNumbers[index] != h.getMagic())
				throw StaleHandleError(h);

			m_magicNumbers[index] = 0;

			m_freeList.push_back(index);
		}

		// Dereference a handle
		T* dereference(H h)
		{
			if (h.isNull()) return nullptr;

			uint32_t index = h.getIndex();

			ASSERT(index < m_data.size());

			// Stale handle check
			if (m_magicNumbers[index] != h.getMagic())
				throw StaleHandleError(h);

			return &(*(m_data.begin() + index));
		}

		// Dereference a handle (const)
		const T* dereference(H h) const
		{
			// Just wrap non-const version
			return const_cast< HandleController<T, H> >(this)->dereference(h);
		}

		// Subscript handle operator for conveniance
		T* operator [] (H h)
		{
			return dereference(h);
		}

		// Subscript handle operator for conveniance (const)
		const T* operator [] (H h) const
		{
			return dereference(h);
		}

		// Get container size
		uint32_t getSize() 
		{ 
			return m_data.size();
		}

		// Allow direct access through subscript operator for iterating
		T* operator [] (uint32_t index)
		{
			ASSERT((index >= 0) && (index < m_data.size()));
			return &m_data[index];
		}

		// Allow direct access through subscript operator for iterating (const)
		const T* operator [] (uint32_t index) const
		{
			ASSERT((index >= 0) && (index < m_data.size()));
			return const_cast< HandleController<T, H> >(this)->m_data[index];
		}

	private:

		std::vector<T> m_data;

		std::vector<uint32_t> m_magicNumbers;

		std::vector<uint32_t> m_freeList;
	};

	#pragma endregion

	#pragma region Pointer-type partial specialisation of HandleController

	template <typename T, typename H>
	class HandleController<T*, H>
	{
	public:

		HandleController() {}

		~HandleController() { clear(); }

		// Dispose of everything
		void clear()
		{
			// Clear containers
			m_pointers.clear();
			m_freeList.clear();
			m_magicNumbers.clear();
		}

		// Aquire a handle to a user provided pointer
		template <typename U>
		U* acquire(H& h, U* ptr)
		{
			uint32_t index;
			
			// If there are no free slots
			if (m_freeList.empty())
			{
				index = m_magicNumbers.size();
				h.attach(index);
				m_pointers.push_back(ptr);
				m_magicNumbers.push_back(h.getMagic());
			}

			// If there are free slots
			else
			{
				index = m_freeList.back();
				h.attach(index);
				m_freeList.pop_back();
				m_magicNumbers[index] = h.getMagic();
			}

			// Return pointer
			return ptr;
		}

		// Release a handle (leaving data undeleted)
		void release(H h)
		{
			uint32_t index = h.getIndex();

			ASSERT(index < m_pointers.size());

			// Stale handle check
			if (m_magicNumbers[index] != h.getMagic())
				throw StaleHandleError(h);

			m_magicNumbers[index] = 0;
			m_freeList.push_back(index);
		}

		// Release handle and delete data
		void dispose(H h)
		{
			uint32_t index = h.getIndex();

			ASSERT(index < m_pointers.size());

			// Stale handle check
			if (m_magicNumbers[index] != h.getMagic())
				throw StaleHandleError(h);

			// Dispose of memory
			SAFE_DELETE(m_pointers[index]);
			
			m_magicNumbers[index] = 0;
			m_freeList.push_back(index);
		}

		// Dispose and release all data
		void disposeAll()
		{
			// Dispose of memory
			BOOST_FOREACH(T* p, m_pointers) SAFE_DELETE(p);

			// Empty lists
			this->clear();
		}

		// Dereference a handle
		T* dereference(H h)
		{
			if (h.isNull()) return nullptr;

			uint32_t index = h.getIndex();

			ASSERT(index < m_pointers.size());

			// Stale handle check
			if (m_magicNumbers[index] != h.getMagic())
				throw StaleHandleError(h);

			return *(m_pointers.begin() + index);
		}

		// Dereference a handle (const)
		const T* dereference(H h) const
		{
			// Just wrap non-const version
			return const_cast< HandleController<T, H> >(this)->dereference(h);
		}

		// Subscript handle operator for conveniance
		T* operator [] (H h)
		{
			return dereference(h);
		}

		// Subscript handle operator for conveniance (const)
		const T* operator [] (H h) const
		{
			return dereference(h);
		}

		// Get container size
		uint32_t getSize() 
		{ 
			return m_pointers.size();
		}

		// Allow direct access through subscript operator for iterating
		T* operator [] (uint32_t index)
		{
			ASSERT((index >= 0) && (index < m_pointers.size()));
			return m_pointers[index];
		}

		// Allow direct access through subscript operator for iterating (const)
		const T* operator [] (uint32_t index) const
		{
			ASSERT((index >= 0) && (index < m_pointers.size()));
			return const_cast< HandleController<T, H> >(this)->m_pointers[index];
		}

	private:

		std::vector<T*> m_pointers;

		std::vector<uint32_t> m_magicNumbers;

		std::vector<uint32_t> m_freeList;
	};

	#pragma endregion
}

#endif