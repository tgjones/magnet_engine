#ifndef _MAGNET_UTIL_VECTOR4_H
#define _MAGNET_UTIL_VECTOR4_H

namespace Magnet
{
	struct Vector2;
	struct Vector3;
	struct Matrix4x4;

	struct Vector4
	{
		// Data members
		float x, y, z, w;

		// Construction
		Vector4();
		Vector4(const Vector4& v);
		Vector4(const Vector3& v, const float w);
		Vector4(const Vector2& v, const float z, const float w);
		Vector4(const float x, const float y, const float z, const float w);
		Vector4(const float data[4]);
		
		// Data access
		float& operator [] (const unsigned int n);
		float* getDataPtr();

		// Equality
		bool operator == (const Vector4& v) const;
		bool operator != (const Vector4& v) const;

		// Unary minus
		Vector4 operator - () const;

		// Addition
		Vector4 operator + (const Vector4& v);
		Vector4& operator += (const Vector4& v);
		Vector4 operator + (const float scalar);
		Vector4& operator += (const float scalar);

		// Subtraction
		Vector4 operator - (const Vector4& v);
		Vector4& operator -= (const Vector4& v);
		Vector4 operator - (const float scalar);
		Vector4& operator -= (const float scalar);

		// Multiplication
		Vector4 operator * (const float scalar) const;
		Vector4& operator *= (const float scalar);
		Vector4 operator * (const Matrix4x4& m) const;
		Vector4& operator *= (const Matrix4x4& m);

		// Dot product
		float dot(const Vector4& v);
		float operator * (const Vector4& v);

		// Normalise
		Vector4& normalise();
	};
}

#endif