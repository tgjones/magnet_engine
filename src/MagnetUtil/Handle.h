
#ifndef _MAGNET_UTIL_HANDLE_H
#define _MAGNET_UTIL_HANDLE_H

#include <sstream>
#include <MagnetUtil/types.h>

namespace Magnet
{
	class AbstractHandle
	{
	public:
        
        inline virtual ~AbstractHandle() {}

		virtual uint32_t getIndex() = 0;

		virtual uint32_t getMagic() = 0;

		virtual uint32_t getHandle() = 0;
	};

	template <typename TAG>
	class Handle : public AbstractHandle
	{
	public:
        
		static const uint16_t MAX_INDEX = 65535;
		static const uint16_t MAX_MAGIC = 65535;

		Handle() : m_handle(0) {}

        virtual ~Handle() {}

		uint32_t getIndex() { return m_index; }

		uint32_t getMagic() { return m_magic; }

		uint32_t getHandle() { return m_handle; }

		bool isNull() { return m_handle == 0; }

		void setNull() { m_handle = 0; }

		bool operator != (const Handle<TAG> other)
		{
			return (this->getHandle() != other.getHandle());
		}

		bool operator == (const Handle<TAG> other)
		{
			return (this->getHandle() == other.getHandle());
		}
	
	protected:

		template <typename, typename> friend class HandleController;

		// 32 bits wide
		union
		{
			struct
			{
				uint16_t m_index;
				uint16_t m_magic;
			};

			uint32_t m_handle;
		};

		virtual void attach(uint32_t index)
		{
			ASSERT(isNull());
			ASSERT(index <= MAX_INDEX);

			static uint32_t magic = 0;
			if (++magic > MAX_INDEX)
				magic = 1; // 0 is "null handler"

			m_index = index;
			m_magic = magic;
		}
	};

	// Less than operator to allow Handle to be stored in sets
	template <typename TAG>
	bool operator < (Handle<TAG> first, Handle<TAG> second)
	{
		return (first.getIndex() < second.getIndex());
	}

	// Special exception for stale handles
	class StaleHandleError : public RuntimeError
	{
	public:

		inline StaleHandleError(AbstractHandle& handle) : 
			RuntimeError("Stale handle with index " + toString(handle.getIndex()) + " and magic number " + toString(handle.getMagic())) {}
	};
}

#endif