#ifndef _MAGNET_UTIL_VECTOR3_H
#define _MAGNET_UTIL_VECTOR3_H

namespace Magnet
{
	struct Vector2;

	struct Vector3
	{
		// Data members
		float x, y, z;

		// Construction
		Vector3();
		Vector3(const Vector3& v);
		Vector3(const Vector2& v, const float z);
		Vector3(const float x, const float y, const float z);
		Vector3(const float data[3]);
		
		// Data access
		float& operator[] (const unsigned int n);
		float* getDataPtr();

		// Equality
		bool operator == (const Vector3& v) const;
		bool operator != (const Vector3& v) const;

		// Unary minus
		Vector3 operator - () const;

		// Addition
		Vector3 operator + (const Vector3& v) const;
		Vector3& operator += (const Vector3& v);
		Vector3 operator + (const float scalar) const;
		Vector3& operator += (const float scalar);

		// Subtraction
		Vector3 operator - (const Vector3& v) const;
		Vector3& operator -= (const Vector3& v);
		Vector3 operator - (const float scalar) const;
		Vector3& operator -= (const float scalar);

		// Multiplication
		Vector3 operator * (const float scalar) const;
		Vector3& operator *= (const float scalar);

		// Dot product
		float dot(const Vector3& v) const;
		float operator * (const Vector3& v) const;

		// Cross product
		Vector3 cross(const Vector3& v) const;
		Vector3 operator ^ (const Vector3& v) const;
		Vector3& operator ^= (const Vector3& v);

		// Normalise
		Vector3& normalise();
	};
	
	// Compute surface normal
	Vector3 computeSurfaceNormal(Vector3 point0, Vector3 point1, Vector3 point2);
}

#endif