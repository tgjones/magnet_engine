
#ifndef _MAGNET_UTIL_TIMER_H
#define _MAGNET_UTIL_TIMER_H

#include <ctime>
#include <MagnetUtil/debug.h>

namespace Magnet
{
	class Timer
	{
	public:

		inline Timer() : m_startTime(clock()), m_elapsedTime(0.0f) {}

		inline float last() const { return m_elapsedTime; }

		inline void start() { m_startTime = clock(); m_elapsedTime = 0.0f; }

		inline float stop() { m_elapsedTime = ((float)clock() - m_startTime) / CLOCKS_PER_SEC; return m_elapsedTime; }

	private:

		clock_t m_startTime;
		
		float m_elapsedTime;
	};
}

#endif