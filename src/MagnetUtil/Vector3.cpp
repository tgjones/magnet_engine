#include "external.h"
#include <MagnetUtil/Vector3.h>
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/debug.h>
#include <MagnetUtil/global.h>

Magnet::Vector3::Vector3() : x(0.0f), y(0.0f), z(0.0f) 
{
}

Magnet::Vector3::Vector3(const Vector3& v) : x(v.x), y(v.y), z(v.z)
{
}

Magnet::Vector3::Vector3(const Vector2& v, const float z) : x(v.x), y(v.y), z(z)
{
}

Magnet::Vector3::Vector3(const float x, const float y, const float z) : x(x), y(y), z(z) 
{
}

Magnet::Vector3::Vector3(const float data[3]) : x(data[0]), y(data[1]), z(data[2]) 
{
}
		
float& Magnet::Vector3::operator [] (const unsigned int n)
{ 
	ASSERT((n >= 0) && (n < 3)); 
	return *(&x + n); 
}

float* Magnet::Vector3::getDataPtr() 
{ 
	return &x; 
}

bool Magnet::Vector3::operator == (const Vector3& v) const
{
	return ((std::abs(this->x - v.x) <= EPSILON) &&
		(std::abs(this->y - v.y) <= EPSILON) &&
		(std::abs(this->z - v.z) <= EPSILON));
}

bool Magnet::Vector3::operator != (const Vector3& v) const
{
	return !(*this == v);
}

Magnet::Vector3 Magnet::Vector3::operator - () const
{
	return Vector3(-this->x, -this->y, -this->z);
}

Magnet::Vector3 Magnet::Vector3::operator + (const Vector3& v) const
{
	return Vector3(this->x + v.x, this->y + v.y, this->z + v.z);
}

Magnet::Vector3& Magnet::Vector3::operator += (const Vector3& v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	return *this;
}

Magnet::Vector3 Magnet::Vector3::operator + (const float scalar) const
{
	return Vector3(this->x + scalar, this->y + scalar, this->z + scalar);
}

Magnet::Vector3& Magnet::Vector3::operator += (const float scalar)
{
	this->x += scalar;
	this->y += scalar;
	this->z += scalar;
	return *this;
}

Magnet::Vector3 Magnet::Vector3::operator - (const Vector3& v) const
{
	return Vector3(this->x - v.x, this->y - v.y, this->z - v.z);
}

Magnet::Vector3& Magnet::Vector3::operator -= (const Vector3& v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	return *this;
}

Magnet::Vector3 Magnet::Vector3::operator - (const float scalar) const
{
	return Vector3(this->x - scalar, this->y - scalar, this->z - scalar);
}

Magnet::Vector3& Magnet::Vector3::operator -= (const float scalar)
{
	this->x -= scalar;
	this->y -= scalar;
	this->z -= scalar;
	return *this;
}
				
Magnet::Vector3 Magnet::Vector3::operator * (const float scalar) const
{
	return Vector3(this->x * scalar, this->y * scalar, this->z * scalar);
}

Magnet::Vector3& Magnet::Vector3::operator *= (const float scalar)
{
	this->x *= scalar;
	this->y *= scalar;
	this->z *= scalar;
	return *this;
}

float Magnet::Vector3::dot(const Vector3& v) const
{
	return (this->x * v.x) + (this->y * v.y) + (this->z * v.z);
}

Magnet::Vector3 Magnet::Vector3::cross(const Vector3& v) const
{
	return Vector3(
		(this->y * v.z) - (v.y * this->z),
		(this->z * v.x) - (v.z * this->x),
		(this->x * v.y) - (v.x * this->y));
}

Magnet::Vector3 Magnet::Vector3::operator ^ (const Vector3& v) const
{
	return this->cross(v);
}

Magnet::Vector3& Magnet::Vector3::operator ^= (const Vector3& v)
{
	Vector3 result = this->cross(v);
	this->x = result.x;
	this->y = result.y;
	this->z = result.z;
	return *this; 
}

Magnet::Vector3& Magnet::Vector3::normalise()
{
	float magnitude = std::sqrt(dot(*this));
	ASSERT(magnitude != 0.0f);
	this->x /= magnitude;
	this->y /= magnitude;
	this->z /= magnitude;
	return *this;
}

Magnet::Vector3 Magnet::computeSurfaceNormal(Vector3 point0, Vector3 point1, Vector3 point2)
{
	Vector3 u = point1 - point0;
	Vector3 v = point2 - point0;
	return u.cross(v).normalise();
}