#ifndef _MAGNET_UTIL_GLOBAL_H
#define _MAGNET_UTIL_GLOBAL_H

// Stringify a macro argument
#define _STRINGIFY(x) #x
#define STRINGIFY(x) _STRINGIFY(x)

// Safe delete macro
#define SAFE_DELETE(ptr) { if (ptr) { delete ptr; (ptr) = nullptr; } }

// COM (DirectX) release macro
#ifdef _MAG_PLATFORM_WIN32
#define SAFE_COM_RELEASE(obj) { if (obj) { (obj)->Release(); obj = nullptr; } }
#endif

// Define PI
const float PI = 3.1415926f;

// Some pre-calculated PI related numbers
const float _PI_OVER_180 = 0.0174532f;
const float _180_OVER_PI = 57.295779f;
const float _PI_OVER_360 = 0.0087266f;
const float _360_OVER_PI = 114.59155f;

// EPSILON is used to test float equality
const float EPSILON = 0.00001f;

#include <string>
#include <sstream>
#include <cctype>
#include <cmath>

namespace Magnet 
{
	// Float/double equality using epsilon
	template <typename T>
	bool isNearlyEqual(T a, T b, const float epsilon = EPSILON)
	{
		return (std::abs(a - b) <= epsilon);
	}

	// Check if a number is a power of two
	template <typename T>
	bool isPowerOfTwo (T x)
	{
		while (((x % 2) == 0) && x > 1)	x /= 2;
		return (x == 1);
	}

	// Exploit sstream to convert anything to a string
	template <typename T>
	std::string toString(const T var)
	{
		std::ostringstream ss;
		ss << var;
		return ss.str();
	}

	// Convert a string to lowercase
	inline std::string toLower(const std::string& str)
	{
		std::string lower;
		for (size_t i = 0; i < str.size(); i++)
			lower.push_back(std::tolower(str[i]));
		return lower;
	}

	// Convert a string to uppercase
	inline std::string toUpper(const std::string& str)
	{
		std::string upper;
		for (size_t i = 0; i < str.size(); i++)
			upper.push_back(std::toupper(str[i]));
		return upper;
	}
}

#endif