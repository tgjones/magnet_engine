#ifndef _MAGNET_UTIL_MAIN_H
#define _MAGNET_UTIL_MAIN_H

// main.h is a convenience header which supplies a Magnet program with the appropriate 'bootstrap' code for the
// current platform. The main() method for instance. This header should be included in a single source file in
// the program with the _MAG_APP_CLASS macro defined as the fully qualified name of program's Magnet::Application 
// decendant above the include.

// Check an application class has been specified
#ifndef _MAG_APP_CLASS
#error A Magnet application class name must be specified with the _MAG_APP_CLASS macro before including main.h
#endif

// config.h detects platform
#include <MagnetUtil/config.h>

// Magnet for Win32
#ifdef _MAG_PLATFORM_WIN32

	// Include Win32 config, classes, macros etc
	#include <MagnetWin32/common.h>

	// Win32 WinMain wrapper
	int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
	{
		try 
		{
			LOG_INFO("Welcome to Magnet Engine for Win32");

			// Register app subsystem class
			REGISTER_SUBSYSTEM(Magnet::ST_APPLICATION, _MAG_APP_CLASS);

			// Down-cast g_pApplication and double check valid inheritance
			Magnet::Win32Application *pWin32Application = dynamic_cast<Magnet::Win32Application*>(g_pApplication);
			ASSERT(pWin32Application);

			// Set Win32 instance handle
			pWin32Application->m_hInstance = hInstance;
			
			// Startup engine
			Magnet::Engine::startup();

			// Run main loop
			int result = pWin32Application->run();

			// Shutdown engine
			Magnet::Engine::shutdown();

			// Return result code
			return result;
		}
		catch (Magnet::Exception &e) 
		{
			// Get exception class name from RTTI and truncate 'class '
			std::string name = typeid(e).name();
			name = name.substr(name.find_last_of(" ") + 1, name.npos);

			// Display an error dialog box
			std::stringstream ss;
			ss << "An exception occured and the program had to be halted: " << std::endl << std::endl << name << ": " << e.getReason();
			MessageBox(NULL, ss.str().c_str(), "Magnet Exception", MB_ICONERROR | MB_OK);

			// Shutdown any Magnet subsystems that are open gracefully
			Magnet::Engine::shutdown();

			// Return error
			return -1;
		}
	}

// Magnet for iOS
#elif defined(_MAG_PLATFORM_IOS)

	// Import iOS config, classes, macros etc
	#import <MagnetiOS/common.h>

	// Derive a local app delegate class
	@interface MyMagnetiOSAppDelegate : MagnetiOSAppDelegate @end
	@implementation MyMagnetiOSAppDelegate @end

	// iOS main method wrapper
	int main(int argc, char* argv[]) 
	{
		// Auto-release pool
		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
			
		// Welcome message
		LOG_INFO("Welcome to Magnet Engine for iOS");

		// Register application class
		REGISTER_SUBSYSTEM(Magnet::ST_APPLICATION, _MAG_APP_CLASS);

		// Run UIKit application entry point
		int result = UIApplicationMain(argc, argv, nil, @"MyMagnetiOSAppDelegate");
			
		// Release auto-release pool
		[pool release];
			
		// Return result code
		return result;
	}

#endif

#endif