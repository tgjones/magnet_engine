#ifndef _MAGNET_UTIL_PRECOMPILE_H
#define _MAGNET_UTIL_PRECOMPILE_H

#include <MagnetUtil/config.h>

// C standard library
#include <cmath>

// C++ Standard Library
#include <vector>
#include <string>
#include <sstream>

// TR1 (via Boost headers)
#include <boost/tr1/regex.hpp>
#include <boost/tr1/unordered_map.hpp>

// Boost IOStreams
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>

// Other Boost
#include <boost/version.hpp>
#include <boost/cstdint.hpp>
#include <boost/foreach.hpp>

// Assimp
#if !defined(_MAG_NO_IMPORT)
	#include <assimp.hpp>
	#include <aiScene.h>
	#include <aiPostProcess.h>
#endif

// Collada DOM (for COLLADA Physics only)
#if !defined(_MAG_NO_PHYSICS)

	#ifdef _MSC_VER // TODO: More specific compiler check
	#pragma warning(push)
	#pragma warning(disable : 4355) // 'this' : used in base member initializer list
	#endif

	#define NO_BOOST
	#define NO_ZAE
	#define DOM_DYNAMIC

	#include <dae.h>
	#include <dom/domCOLLADA.h>
	#include <dom/domConstants.h>

	#undef NO_BOOST
	#undef NO_ZAE

	#ifdef _MSC_VER
	#pragma warning(pop)
	#endif

#endif

// libpng
#if !defined(_MAG_NO_IMPORT)
#include <png.h>
#endif

#endif