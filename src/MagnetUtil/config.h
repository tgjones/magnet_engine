#ifndef _MAGNET_UTIL_CONFIG_H
#define _MAGNET_UTIL_CONFIG_H

// config.h must be included first be any header or source in the Magnet source tree, however, common.h
// does this so it is not neccessary if you include common.h first instead. This header sets up a number
// of important macros which are used to configure the source built, including platform detection
// manifested as the _MAG_PLATORM_* macros and some configuration macros for 3rd party libraries.

// Check we are using a C++ compiler
#ifndef __cplusplus
#error C++ compiler not detected, perhaps you are trying to use a C compiler
#endif

// Detect platform
#if defined(_WIN32)
	#define _MAG_PLATFORM_WIN32
#elif defined(__APPLE__)
	#import <TargetConditionals.h>
    #if (TARGET_OS_MAC == 1) && (TARGET_OS_IPHONE == 0)
        #define _MAG_PLATFORM_MAC_OSX
	#elif (TARGET_OS_IPHONE == 1)
		#define _MAG_PLATFORM_IOS
	#else
		#define _MAG_PLATFORM_UNSUPPORTED
	#endif
#else
	#define _MAG_PLATFORM_UNSUPPORTED
#endif

// Report compile-time error for unsupported platforms
#ifdef _MAG_PLATFORM_UNSUPPORTED
	#error Magnet does not support this platform
#endif

/*
// **** TEMPORARY ****
// Disable Physics code for now, remove this in future
#ifndef _MAG_NO_PHYSICS
	#define _MAG_NO_PHYSICS
#endif
// **** TEMPORARY ****
*/

// Define Magnet version strings
#define _MAG_VER_STR "0.5 alpha"

// Win32 Magnet compile flags
#if defined(_MAG_PLATFORM_WIN32)

	// External libraries to use
	#define _MAG_USE_D3D
	#define _MAG_USE_GL
	#define _MAG_USE_GLEW
    #define _MAG_USE_OPENMP
	#ifndef _MAG_NO_PHYSICS
	#define _MAG_USE_PHYSX
	#endif

	// OpenGL version 4.1 supported
	#define _MAG_GL_VER 41

// iOS Magnet compile flags
#elif defined(_MAG_PLATFORM_IOS)

	// External libraries to use
	#define _MAG_USE_GL
    #define _MAG_USE_OPENMP

	// OpenGL ES 2.0 supported
	#define _MAG_GL_ES_VER 20

// Mac OS X Magnet compile flags
#elif defined(_MAG_PLATFORM_MAC_OSX)

    // External libraries to use
    #define _MAG_USE_GL
    #define _MAG_USE_OPENMP

    // OpenGL version 4.1 supported
    #define _MAG_GL_VER 41

#endif

// iOS specific settings
#ifdef _MAG_PLATFORM_IOS

	// Check we are using an Objective-C++ compiler
	#if !defined(__OBJC__)
	#error Objective-C compiler not detected, perhaps you forgot to enable Objective-C++ mode in your compiler
	#endif

#endif

// Turn on Boost diagnostic information
#ifdef _MAG_VERBOSE
	#define BOOST_LIB_DIAGNOSTIC
#endif

// Use version 3 of the Boost Filesystem library and strip deprecated functionality
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED

// Define nullptr keyword for unsupported compilers
#ifndef _MSC_VER
	#define nullptr 0
#endif

#endif
