#ifndef _MAGNET_UTIL_MATRIX4X4_H
#define _MAGNET_UTIL_MATRIX4X4_H

namespace Magnet 
{
	struct Vector2;
	struct Vector3;
	struct Vector4;

	struct Matrix4x4
	{
		// Data members
		float _11, _12, _13, _14;
		float _21, _22, _23, _24;
		float _31, _32, _33, _34;
		float _41, _42, _43, _44;

		// Construction
		Matrix4x4();
		Matrix4x4(const Matrix4x4& m);
		Matrix4x4(
			const float _11, const float _12, const float _13, const float _14,
			const float _21, const float _22, const float _23, const float _24,
			const float _31, const float _32, const float _33, const float _34,
			const float _41, const float _42, const float _43, const float _44);
		Matrix4x4(const float data[16]);

		// Data access
		float& operator [] (const unsigned int n);
		float* getDataPtr();

		// Equality
		bool operator == (const Matrix4x4& m) const;
		bool operator != (const Matrix4x4& m) const;

		// Unary minus
		Matrix4x4 operator - () const;

		// Identity matrix
		Matrix4x4& identity();
		static Matrix4x4 createIdentity();

		// Transpose matrix
		Matrix4x4& transpose();
		
		// Multiplication
		Matrix4x4 operator * (const Matrix4x4& m) const;
		Matrix4x4& operator *= (const Matrix4x4& m);

		Matrix4x4 operator * (const float scalar) const;
		Matrix4x4& operator *= (const float scalar);
	
		// Rotation
		Matrix4x4& setRotationX(const float theta);
		static Matrix4x4 createRotationX(const float theta);

		Matrix4x4& setRotationY(const float theta);
		static Matrix4x4 createRotationY(const float theta);
		
		Matrix4x4& setRotationZ(const float theta);
		static Matrix4x4 createRotationZ(const float theta);
		
		Matrix4x4& setRotation(const Vector3& rotation);
		static Matrix4x4 createRotation(const Vector3& rotation);
		
		Matrix4x4& setRotation(const float xTheta, const float yTheta, const float zTheta);
		static Matrix4x4 createRotation(const float xTheta, const float yTheta, const float zTheta);

		Matrix4x4& setArbitraryRotation(const float theta, const Vector3 v);
		static Matrix4x4 createArbitraryRotation(const float theta, const Vector3 v);

		// Scale
		Matrix4x4& setScale(const Vector3& scale);
		static Matrix4x4 createScale(const Vector3& scale);

		Matrix4x4& setScale(const float x, const float y, const float z);
		static Matrix4x4 createScale(const float x, const float y, const float z);

		Matrix4x4& setInverseScale(const Vector3& inverseScale);
		static Matrix4x4 createInverseScale(const Vector3& inverseScale);

		Matrix4x4& setInverseScale(const float x, const float y, const float z);
		static Matrix4x4 createInverseScale(const float x, const float y, const float z);

		// Translation
		Matrix4x4& setTranslation(const Vector3& translation);
		static Matrix4x4 createTranslation(const Vector3& translation);

		Matrix4x4& setTranslation(const float x, const float y, const float z);
		static Matrix4x4 createTranslation(const float x, const float y, const float z);

		Matrix4x4& setInverseTranslation(const Vector3& inverseTranslation);
		static Matrix4x4 createInverseTranslation(const Vector3& inverseTranslation);

		Matrix4x4& setInverseTranslation(const float x, const float y, const float z);
		static Matrix4x4 createInverseTranslation(const float x, const float y, const float z);
	};
}

#endif