#ifndef _MAGNET_UTIL_VECTOR2_H
#define _MAGNET_UTIL_VECTOR2_H

namespace Magnet
{
	struct Vector2
	{
		// Data members
		float x, y;

		// Construction
		Vector2();
		Vector2(const Vector2& v);
		Vector2(const float x, const float y);
		Vector2(const float data[2]);

		// Data access
		float& operator [] (const unsigned int n);
		float* getDataPtr();

		// Equality
		bool operator == (const Vector2& v) const;
		bool operator != (const Vector2& v) const;

		// Unary minus
		Vector2 operator - () const;

		// Addition
		Vector2 operator + (const Vector2& v) const;
		Vector2& operator += (const Vector2& v);
		Vector2 operator + (const float scalar) const;
		Vector2& operator += (const float scalar);

		// Subtraction
		Vector2 operator - (const Vector2& v) const;
		Vector2& operator -= (const Vector2& v);
		Vector2 operator - (const float scalar) const;
		Vector2& operator -= (const float scalar);

		// Multiplication
		Vector2 operator * (const float scalar) const;
		Vector2& operator *= (const float scalar);

		// Dot product
		float dot(const Vector2& v) const;
		float operator * (const Vector2& v) const;

		// Normalise
		Vector2& normalise();
	};

}

#endif