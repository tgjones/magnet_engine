
#ifndef _MAGNET_UTIL_TYPES_H
#define _MAGNET_UTIL_TYPES_H

// Boost int typedefs (uint8_t etc.)
#include <boost/cstdint.hpp>

// Memory size typedefs
typedef uint8_t  byte_t;
typedef uint16_t word_t;
typedef uint32_t dword_t;
typedef uint64_t qword_t;

// Generic function pointer typedef
typedef void (*void_func_t)(void);

namespace Magnet 
{
	// 32-bit color struct
	struct Color
	{
		unsigned char r, g, b, a;

		inline Color() : r(255), g(255), b(255), a(255) {}

		inline Color(unsigned char r, unsigned char g, unsigned char b) : r(r), g(g), b(b), a(255) {}

		inline Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a) : r(r), g(g), b(b), a(a) {}
        
        inline Color(uint32_t color) : r(*((byte_t*)&color)), g(*((byte_t*)&color+1)), b(*((byte_t*)&color+2)), a(*((byte_t*)&color+3)) {}

		Color(const char* hex);

		inline operator unsigned char* () { return &r; }
        
        inline operator uint32_t () { return *(uint32_t*)&r; }
	};

	// 128-bit floating point color struct
	struct ColorValue
	{
		// Red value first
		float r, g, b, a;
		
		inline ColorValue() : r(1.0f), g(1.0f), b(1.0f), a(1.0f) {}
		
		inline ColorValue(float r, float g, float b) : r(r), g(g), b(b), a(1.0f) {}

		inline ColorValue(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

		inline operator float* () { return &r; }
	};
}

#endif