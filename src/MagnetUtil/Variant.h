
#ifndef _MAGNET_UTIL_VARIANT_H
#define _MAGNET_UTIL_VARIANT_H

namespace Magnet 
{

	
	class Variant
	{
	private:
		
		enum VariantType
		{
			VT_INTEGER,
			VT_FLOAT,
			VT_BOOL,
		};
		
		// Type member
		VariantType m_type;

		// Data values
		union
		{
			int   m_intValue;
			float m_floatValue;
			bool  m_boolValue;
		};

	public:

		// Default constructor (default is integer of 0)
		Variant();

		// Custom constructors
		Variant(int intValue);
		Variant(float floatValue);
		Variant(bool boolValue);

		// Conversion operators
		inline operator int () const { return this->asInt(); }
		inline operator float () const { return this->asFloat(); }
		inline operator bool () const { return this->asBool(); }

		// Equality operators
		bool operator == (const Variant& other) const;
		bool operator != (const Variant& other) const;

		// Type getters
		inline bool isInt() const { return m_type == VT_INTEGER; }
		inline bool isFloat() const { return m_type == VT_FLOAT; }
		inline bool isBool() const { return m_type == VT_BOOL; }

		// Value getters
		int asInt() const;
		float asFloat() const;
		bool asBool() const;

		// Value setters
		void set(int intValue);
		void set(float floatValue);
		void set(bool boolValue);
	};
}

#endif