#include "external.h"
#include <MagnetUtil/Vector4.h>
#include <MagnetUtil/Vector3.h>
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/Matrix4x4.h>
#include <MagnetUtil/debug.h>
#include <MagnetUtil/global.h>

Magnet::Vector4::Vector4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) 
{
}

Magnet::Vector4::Vector4(const Vector4& v) : x(v.x), y(v.y), z(v.z), w(v.w) 
{
}

Magnet::Vector4::Vector4(const Vector3& v, const float w) : x(v.x), y(v.y), z(v.z), w(w) 
{
}

Magnet::Vector4::Vector4(const Vector2& v, const float z, const float w) : x(v.x), y(v.y), z(z), w(w) 
{
}

Magnet::Vector4::Vector4(const float x, const float y, const float z, const float w) : x(x), y(y), z(z), w(w) 
{
}

Magnet::Vector4::Vector4(const float data[4]) : x(data[0]), y(data[1]), z(data[2]), w(data[3]) 
{
}
		
float& Magnet::Vector4::operator [] (const unsigned int n) 
{	
	ASSERT((n >= 0) && (n < 4)); 
	return *(&x + n); 
}

float* Magnet::Vector4::getDataPtr() 
{ 
	return &x; 
}

bool Magnet::Vector4::operator == (const Vector4& v) const
{
	return ((std::abs(this->x - v.x) <= EPSILON) &&
		(std::abs(this->y - v.y) <= EPSILON) &&
		(std::abs(this->z - v.z) <= EPSILON) &&
		(std::abs(this->w - v.w) <= EPSILON));
}

bool Magnet::Vector4::operator != (const Vector4& v) const
{
	return !(*this == v);
}

Magnet::Vector4 Magnet::Vector4::operator - () const
{
	return Vector4(-this->x, -this->y, -this->z, -this->w);
}

Magnet::Vector4 Magnet::Vector4::operator + (const Vector4& v)
{
	return Vector4(this->x + v.x, this->y + v.y, this->z + v.z, this->w + v.w);
}

Magnet::Vector4& Magnet::Vector4::operator += (const Vector4& v)
{
	this->x += v.x;
	this->y += v.y;
	this->z += v.z;
	this->w += v.w;
	return *this;
}

Magnet::Vector4 Magnet::Vector4::operator + (const float scalar)
{
	return Vector4(this->x + scalar, this->y + scalar, this->z + scalar, this->w + scalar);
}

Magnet::Vector4& Magnet::Vector4::operator += (const float scalar)
{
	this->x += scalar;
	this->y += scalar;
	this->z += scalar;
	this->w += scalar;
	return *this;
}

Magnet::Vector4 Magnet::Vector4::operator - (const Vector4& v)
{
	return Vector4(this->x - v.x, this->y - v.y, this->z - v.z, this->w - v.w);
}

Magnet::Vector4& Magnet::Vector4::operator -= (const Vector4& v)
{
	this->x -= v.x;
	this->y -= v.y;
	this->z -= v.z;
	this->w -= v.w;
	return *this;
}

Magnet::Vector4 Magnet::Vector4::operator - (const float scalar)
{
	return Vector4(this->x - scalar, this->y - scalar, this->z - scalar, this->w - scalar);
}

Magnet::Vector4& Magnet::Vector4::operator -= (const float scalar)
{
	this->x -= scalar;
	this->y -= scalar;
	this->z -= scalar;
	this->w -= scalar;
	return *this;
}

Magnet::Vector4 Magnet::Vector4::operator * (const float scalar) const
{
	return Vector4(this->x * scalar, this->y * scalar, this->z * scalar, this->w * scalar);
}

Magnet::Vector4& Magnet::Vector4::operator *= (const float scalar)
{
	this->x *= scalar;
	this->y *= scalar;
	this->z *= scalar;
	this->w *= scalar;
	return *this;
}

Magnet::Vector4 Magnet::Vector4::operator * (const Matrix4x4& m) const
{
	Vector4 result;
	result.x = (m._11 * this->x) + (m._21 * this->y) + (m._31 * this->z) + (m._41 * this->w);
	result.y = (m._12 * this->x) + (m._22 * this->y) + (m._32 * this->z) + (m._42 * this->w);
	result.z = (m._13 * this->x) + (m._23 * this->y) + (m._33 * this->z) + (m._43 * this->w);
	result.w = (m._14 * this->x) + (m._24 * this->y) + (m._34 * this->z) + (m._44 * this->w);
	return result;
}

Magnet::Vector4& Magnet::Vector4::operator *= (const Matrix4x4& m)
{
	Vector4 result;
	result.x = (m._11 * this->x) + (m._21 * this->y) + (m._31 * this->z) + (m._41 * this->w);
	result.y = (m._12 * this->x) + (m._22 * this->y) + (m._32 * this->z) + (m._42 * this->w);
	result.z = (m._13 * this->x) + (m._23 * this->y) + (m._33 * this->z) + (m._43 * this->w);
	result.w = (m._14 * this->x) + (m._24 * this->y) + (m._34 * this->z) + (m._44 * this->w);
	std::memcpy((void*)&x, (void*)&result.x, 4 * sizeof(float));
	return *this;
}

float Magnet::Vector4::dot(const Vector4& v)
{
	return (this->x * v.x) + (this->y * v.y) + (this->z * v.z) + (this->w * v.w);
}

Magnet::Vector4& Magnet::Vector4::normalise()
{
	float magnitude = std::sqrt(dot(*this));
	ASSERT(magnitude != 0.0f);
	this->x /= magnitude;
	this->y /= magnitude;
	this->z /= magnitude;
	this->w /= magnitude;
	return *this;
}