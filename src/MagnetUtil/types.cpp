#include "external.h"
#include <MagnetUtil/types.h>
#include <MagnetUtil/debug.h>

Magnet::Color::Color(const char* hex)
{
	ASSERT(std::strlen(hex) == 6);

	char* strData = new char[2];
	byte_t channel[3];

	for (int i = 0, j = 0; i < 3; i++, j += 2)
	{
		std::strncpy(strData, hex + j, 2);
		channel[i] = (byte_t)std::strtoul(strData, 0, 16);
	}

	r = channel[0];
	g = channel[1];
	b = channel[2];
}