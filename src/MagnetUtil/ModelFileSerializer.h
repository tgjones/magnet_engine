
#ifndef _MAGNET_UTIL_MODEL_DATA_H
#define _MAGNET_UTIL_MODEL_DATA_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class AbstractMesh;
	class RigidBody;

	#if !defined(_MAG_NO_IMPORT)
	
	// Import flags
	enum ModelImportFlags
	{
		MIF_FACE_NORMALS     = 0x1, // Compute face (sharp) normals
		MIF_SMOOTH_NORMALS   = 0x2, // Compute smooth normals (nice)
		MIF_OVERRIDE_NORMALS = 0x4, // Override any normals in the imported file
		MIF_BOUNDING_BOXES   = 0x8, // Compute a simple bounding boxes
	};

	// Import options
	struct ModelImportOptions
	{
		uint32_t flags;
		float hardEdgeThresh; // For MIF_SMOOTH_NORMALS, this is the max smooth angle in degrees
		inline ModelImportOptions() : flags(0), hardEdgeThresh(80.0f) {}
	};

	#endif

	// ModelFile struct
	class ModelFileSerializer : public FileSerializer
	{
	public:

		static const std::string FILE_EXT;

		~ModelFileSerializer();

		inline bool hasPhysicalProperties() { return !m_rigidBodies.empty(); }

		inline std::vector<AbstractMesh*>& getMeshes() { return m_meshes; }

		inline std::vector<RigidBody>& getRigidBodies() { return m_rigidBodies; }

		void read(const fs::path& filePath);

		void write(const fs::path& filePath, const uint8_t compressionLevel = 9);

		#if !defined(_MAG_NO_IMPORT)
		void import(const fs::path& filePath, const ModelImportOptions options = ModelImportOptions());
		#endif

	private:

		std::vector<Magnet::AbstractMesh*> m_meshes;

		std::vector<Magnet::RigidBody> m_rigidBodies;
	};
}

#endif