#include "external.h"
#include <MagnetUtil/common.h>

const std::string Magnet::ModelFileSerializer::FILE_EXT = ".mmf";

Magnet::ModelFileSerializer::~ModelFileSerializer()
{
	// Delete and clear meshes
	BOOST_FOREACH(AbstractMesh* p, m_meshes) SAFE_DELETE(p);
	m_meshes.clear();
}

void Magnet::ModelFileSerializer::read(const fs::path& filePath)
{	
	ASSERT(m_meshes.empty());
	ASSERT(toLower(filePath.extension().string()) == FILE_EXT);
    
    // Load protobuf file format class
    Proto::ModelFileFormat proto;
    
    // Setup file stream buffer with ZLib decompressor
    std::ifstream ifs(filePath.string().c_str(), std::ios::in | std::ios::binary);
    boost::iostreams::filtering_streambuf<boost::iostreams::input> ifsb;
    ifsb.push(boost::iostreams::zlib_decompressor());
    ifsb.push(ifs);
    
    // De-serialize from disk
    std::istream is(&ifsb);
    proto.ParseFromIstream(&is);
    
    // VU_POSITION_ONLY meshes (UNTESTED!)
    for (int i = 0; i < proto.position_only_mesh_size(); i++)
    {
        // Create memory for mesh
        GenericMesh<VU_POSITION_ONLY>* pGenericMesh = new GenericMesh<VU_POSITION_ONLY>();
        
        // Get a pointer to the mesh data
        const Proto::ModelFileFormat_PositionOnlyMesh* pProtoMesh = &proto.position_only_mesh(i);
        
        // Index data
        for (int j = 0; j < pProtoMesh->index_size(); j++)
            pGenericMesh->getIndices().push_back(pProtoMesh->index(j));
        
        // Vertex data
        for (int j = 0; j < pProtoMesh->position_size(); j++)
        {
            GenericMesh<VU_POSITION_ONLY>::Vertex vert;

            // Position
            const Proto::Vector3* pProtoPosition = &pProtoMesh->position(j);
            vert.position.x = pProtoPosition->x();
            vert.position.y = pProtoPosition->y();
            vert.position.z = pProtoPosition->z();
            
            pGenericMesh->getVertices().push_back(vert);
        }
        
        // Add mesh
        m_meshes.push_back(static_cast<AbstractMesh*>(pGenericMesh));        
    }
    
    // VU_UNTEXTURED_PLAIN meshes
    for (int i = 0; i < proto.untextured_plain_mesh_size(); i++)
    {
        // Create memory for mesh
        GenericMesh<VU_UNTEXTURED_PLAIN>* pGenericMesh = new GenericMesh<VU_UNTEXTURED_PLAIN>();
        
        // Get a pointer to the mesh data
        const Proto::ModelFileFormat_UntexturedPlainMesh* pProtoMesh = &proto.untextured_plain_mesh(i);
        
        // Index data
        for (int j = 0; j < pProtoMesh->index_size(); j++)
            pGenericMesh->getIndices().push_back(pProtoMesh->index(j));
        
        // Vertex data
        for (int j = 0; j < pProtoMesh->position_size(); j++)
        {            
            GenericMesh<VU_UNTEXTURED_PLAIN>::Vertex vert;
            
            // Position
            const Proto::Vector3* pProtoPosition = &pProtoMesh->position(j);
            vert.position.x = pProtoPosition->x();
            vert.position.y = pProtoPosition->y();
            vert.position.z = pProtoPosition->z();
            
            // Normal
            const Proto::Vector3* pProtoNormal = &pProtoMesh->normal(j);
            vert.normal.x = pProtoNormal->x();
            vert.normal.y = pProtoNormal->y();
            vert.normal.z = pProtoNormal->z();
            
            pGenericMesh->getVertices().push_back(vert);
        }       

        // Add mesh
        m_meshes.push_back(static_cast<AbstractMesh*>(pGenericMesh));        
    }
}

void Magnet::ModelFileSerializer::write(const fs::path& filePath, const uint8_t compressionLevel)
{
	ASSERT(!m_meshes.empty());
	ASSERT(toLower(filePath.extension().string()) == FILE_EXT);
	ASSERT(compressionLevel <= 9);
    
    // Load protobuf file format class
    Proto::ModelFileFormat proto;
    
    // Mesh data
    BOOST_FOREACH(AbstractMesh* pAbstractMesh, m_meshes)
    {
        // VU_POSITION_ONLY
        if (pAbstractMesh->getVertexUsage() == VU_POSITION_ONLY)
        {
            // Down-cast mesh
            GenericMesh<VU_POSITION_ONLY>* pGenericMesh = 
                dynamic_cast<GenericMesh<VU_POSITION_ONLY>*>(pAbstractMesh);
            ASSERT(pGenericMesh);
            
            // Add mesh to protobuf file format class
            Proto::ModelFileFormat_PositionOnlyMesh* pProtoMesh = proto.add_position_only_mesh();
            
            // Index data
            BOOST_FOREACH(uint16_t index, pGenericMesh->getIndices())
                pProtoMesh->add_index(index);
            
            // Vertex data
            BOOST_FOREACH(GenericMesh<VU_POSITION_ONLY>::Vertex vert, pGenericMesh->getVertices())
            {
                // Position
                Proto::Vector3* pPosition = pProtoMesh->add_position();
                pPosition->set_x(vert.position.x);
                pPosition->set_y(vert.position.y);
                pPosition->set_z(vert.position.z);
            }
        }
        
        // VU_UNTEXTURED_PLAIN
        else if (pAbstractMesh->getVertexUsage() == VU_UNTEXTURED_PLAIN)
        {
            // Down-cast mesh
            GenericMesh<VU_UNTEXTURED_PLAIN>* pGenericMesh = 
                dynamic_cast<GenericMesh<VU_UNTEXTURED_PLAIN>*>(pAbstractMesh);
            ASSERT(pGenericMesh);
            
            // Load protobuf mesh class
            Proto::ModelFileFormat_UntexturedPlainMesh* pProtoMesh = proto.add_untextured_plain_mesh();
            
            // Index data
            BOOST_FOREACH(uint16_t index, pGenericMesh->getIndices())
                pProtoMesh->add_index(index);
            
            // Vertex data
            BOOST_FOREACH(GenericMesh<VU_UNTEXTURED_PLAIN>::Vertex vert, pGenericMesh->getVertices())
            {
                // Position
                Proto::Vector3* pPosition = pProtoMesh->add_position();
                pPosition->set_x(vert.position.x);
                pPosition->set_y(vert.position.y);
                pPosition->set_z(vert.position.z);
                
                // Normal
                Proto::Vector3* pNormal = pProtoMesh->add_normal();
                pNormal->set_x(vert.normal.x);
                pNormal->set_y(vert.normal.y);
                pNormal->set_z(vert.normal.z);
            }
        }
        
        // Unrecognised vertex usage
        else throw RuntimeError("Unrecognised vertex usage");
    }
    
    // Setup file stream buffer with ZLib compressor
    std::ofstream ofs(filePath.string().c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
    boost::iostreams::filtering_streambuf<boost::iostreams::output> ofsb;
    ofsb.push(boost::iostreams::zlib_compressor(compressionLevel));
    ofsb.push(ofs);
   
    // Serialize to disk
    std::ostream os(&ofsb);
    proto.SerializeToOstream(&os);
}

#if !defined(_MAG_NO_IMPORT)
void Magnet::ModelFileSerializer::import(const fs::path& filePath, const ModelImportOptions options)
{
	ASSERT(m_meshes.empty());
	ASSERT(!((options.flags & MIF_FACE_NORMALS) && (options.flags & MIF_SMOOTH_NORMALS)));

	#pragma region Import visual geometry data with Assimp (any format)

	// Create assimp aiImporter
	Assimp::Importer aiImporter;

	// Check extension is supported by Assimp
	if (!aiImporter.IsExtensionSupported(filePath.extension().string()))
		throw RuntimeError("Cannot import model: '" + toLower(filePath.extension().string()) + "' is not a supported format by Assimp");

	#ifndef _MAG_NO_IMPORT
	// Enable extra post-processing check
	aiImporter.SetExtraVerbose(true);
	#endif

	// Concatenate required Assimp options
	uint32_t aiProcessFlags = 
		aiProcess_MakeLeftHanded   |  
		aiProcess_FlipWindingOrder |
		aiProcess_RemoveComponent  |
		aiProcess_OptimizeMeshes   |
		aiProcess_OptimizeGraph    |
		aiProcess_Triangulate      |
		aiProcess_GenUVCoords      |
		aiProcess_JoinIdenticalVertices;

	// Are we to compute face normals?
	if (options.flags & MIF_FACE_NORMALS)
		aiProcessFlags |= aiProcess_GenNormals;

	// Or smooth normals
	else if (options.flags & MIF_SMOOTH_NORMALS)
	{
		aiProcessFlags |= aiProcess_GenSmoothNormals;
		aiImporter.SetPropertyFloat(AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, options.hardEdgeThresh);
	}

	// Tell Assimp to ignore certain info (vert colors, cameras and lights)
	uint32_t aiRemoveFlags = 
		aiComponent_COLORS  |
		aiComponent_CAMERAS |
		aiComponent_LIGHTS;

	// Tell Assimp to ignore normals if we have specified to force Assimp to calculate them
	if (options.flags & MIF_OVERRIDE_NORMALS)
		aiRemoveFlags |= aiComponent_NORMALS; 

	// Setup some aiImporter properties
	aiImporter.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiRemoveFlags);

	// Import file and apply post-processing (Assimp I love you)
	const aiScene* pAiScene = aiImporter.ReadFile(filePath.string(), aiProcessFlags);

	// If the import failed, report it
	if (pAiScene == nullptr)
		throw RuntimeError("Assimp import failed: " + std::string(aiImporter.GetErrorString()));

	// Check we even have meshes
	ASSERT(pAiScene->HasMeshes());

	// For each mesh
	for (uint32_t i = 0; i < pAiScene->mNumMeshes; i++)
	{
		// Dereference mesh from array
		aiMesh* pAiMesh = pAiScene->mMeshes[i];

		ASSERT(pAiMesh->HasPositions());
		ASSERT(pAiMesh->HasFaces());

		// Abstract mesh pointer
		AbstractMesh* pAbstractMesh = nullptr;

		// Decide on vertex format based on what data Assimp has imported, just use index 0 to check for
		// vertex colors and texture coordinates, this makes obvious assumptions about the whole vertex
		// dataset containing the same data as the first vertex.

		// VU_POSITION_ONLY
		if (!pAiMesh->HasNormals() && !pAiMesh->HasVertexColors(0) && !pAiMesh->HasTextureCoords(0))
		{
			// Create mesh with correct vertex format
			pAbstractMesh = new Magnet::GenericMesh<Magnet::VU_POSITION_ONLY>();
			
			// Down-cast for use in this block
			Magnet::GenericMesh<Magnet::VU_POSITION_ONLY>* pMesh = 
				static_cast<Magnet::GenericMesh<Magnet::VU_POSITION_ONLY>*>(pAbstractMesh);

			// Create a temporary vertex to build
			Magnet::GenericMesh<Magnet::VU_POSITION_ONLY>::Vertex vert;

			// For each vertex (in the same order as Assimp for indexing)
			for (uint32_t i = 0; i < pAiMesh->mNumVertices; i++)
			{
				// Just copy position and add to mesh
				vert.position = *(reinterpret_cast<Vector3*>(&pAiMesh->mVertices[i]));
				pMesh->addVertex(vert);
			}
		}

		// VU_UNTEXTURED_PLAIN
		else if (pAiMesh->HasNormals() && !pAiMesh->HasVertexColors(0) && !pAiMesh->HasTextureCoords(0))
		{
			// Create mesh with correct vertex format
			pAbstractMesh = new Magnet::GenericMesh<Magnet::VU_UNTEXTURED_PLAIN>();

			// Down-cast for use in this block
			Magnet::GenericMesh<Magnet::VU_UNTEXTURED_PLAIN>* pMesh = 
				static_cast<Magnet::GenericMesh<Magnet::VU_UNTEXTURED_PLAIN>*>(pAbstractMesh);

			// Create a temporary vertex to build
			Magnet::GenericMesh<Magnet::VU_UNTEXTURED_PLAIN>::Vertex vert;

			// For each vertex (in the same order as Assimp for indexing)
			for (uint32_t i = 0; i < pAiMesh->mNumVertices; i++)
			{
				// Copy position
				vert.position = *(reinterpret_cast<Vector3*>(&pAiMesh->mVertices[i]));

				// Copy normal
				vert.normal = *(reinterpret_cast<Vector3*>(&pAiMesh->mNormals[i]));

				// Add vert to mesh
				pMesh->addVertex(vert);
			}
		}

		// Unsupported vertex data
		else throw RuntimeError("Assimp imported unsupported properties imported from ModelFile file");

		// Add faces (index buffer) which we can do without knowing the vertex usage
		for (uint32_t i = 0; i < pAiMesh->mNumFaces; i++)
		{
			// Dereference face
			aiFace face = pAiMesh->mFaces[i];

			// Triangles only
			ASSERT(face.mNumIndices == 3);

			// Add indices (this assumes verts are in the same order still)
			pAbstractMesh->addTriangle(face.mIndices[0], face.mIndices[1], face.mIndices[2]);
		}

		// Add default material
		// TODO: Support actual materials!
		pAbstractMesh->addMaterial(Magnet::Material());

		// Generate a simple bounding box rigid body if requested by flags
		if (options.flags & MIF_BOUNDING_BOXES)
		{
			// Spin through all the vertices and get min/max position
			Vector3 min, max;
			for (uint32_t i = 0; i < pAiMesh->mNumVertices; i++)
			{
				// Dereference vert
				aiVector3D aiVert = pAiMesh->mVertices[i];

				// Save any new maximums
				if (aiVert.x > max.x) max.x = aiVert.x;
				if (aiVert.y > max.y) max.y = aiVert.y;
				if (aiVert.z > max.z) max.z = aiVert.z;

				// Save any new minimums
				if (aiVert.x < min.x) min.x = aiVert.x;
				if (aiVert.y < min.y) min.y = aiVert.y;
				if (aiVert.z < min.z) min.z = aiVert.z;
			}

			// Generate bounding box
			Vector3 box;
			box.x = (max.x - min.x) / 2.0f;
			box.y = (max.y - min.y) / 2.0f;
			box.z = (max.z - min.z) / 2.0f;

			// Create bounding box rigid body
			RigidBody boundingBox;
			boundingBox.addBox(box);

			// Add bounding box to model
			m_rigidBodies.push_back(boundingBox);
		}

		// Add mesh to model
		m_meshes.push_back(pAbstractMesh);
	}

	// Free Assimp's memory as we'eve finished with it now
	aiImporter.FreeScene();

	// COLLADA Physics 
    #ifndef _MAG_NO_PHYSICS
	if (toLower(filePath.extension().string()) == ".dae")
	{
		DAE dae;

		// Double check version of DOM in use (*not* the COLLADA DOM library version)
		ASSERT((std::string(dae.getDomVersion()) == "1.4") || (std::string(dae.getDomVersion()) == "1.4.1"));

		// Attempt to open the DAE file
		if (!dae.open(filePath.string())) 
			throw RuntimeError("Collada DOM import failed on '" + filePath.string() + "'");

		// Get all the rigid bodies
		std::vector<domRigid_body*> daeRigidBodies = dae.getDatabase()->typeLookup<domRigid_body>();

		// For each rigid body
		BOOST_FOREACH(domRigid_body* pDaeRigidBody, daeRigidBodies)
		{
			// Get shapes
			domRigid_body::domTechnique_common::domShape_Array shapes = pDaeRigidBody->getTechnique_common()->getShape_array();

			// For each shape
			for (uint32_t i = 0; i < shapes.getCount(); i ++)
			{
				// If the shape is a box
				if (domBox* pDaeBox = shapes[i]->getBox())
				{
					// Generate bounding box
					Vector3 box;
					box.x = boost::numeric_cast<float, daeDouble>(pDaeBox->getHalf_extents()->getValue()[0]);
					box.y = boost::numeric_cast<float, daeDouble>(pDaeBox->getHalf_extents()->getValue()[1]);
					box.z = boost::numeric_cast<float, daeDouble>(pDaeBox->getHalf_extents()->getValue()[2]);

					// Create bounding box rigid body
					RigidBody boundingBox;
					boundingBox.addBox(box);

					// Add bounding box to model
					m_rigidBodies.push_back(boundingBox);
				}

				// TODO: Other shapes and stuff like mass etc
			}
		}
	}
    #endif
}
#endif