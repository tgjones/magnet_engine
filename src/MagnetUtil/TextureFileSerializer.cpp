#include "external.h"
#include <MagnetUtil/common.h>

const std::string Magnet::TextureFileSerializer::FILE_EXT = ".mtf";

void Magnet::TextureFileSerializer::read(const fs::path& filePath)
{
	ASSERT(m_pPixelData == nullptr);
	ASSERT(toLower(filePath.extension().string()) == FILE_EXT);
    
    // Load protobuf file format class
    Proto::TextureFileFormat proto;

    // Setup file stream buffer with ZLib decompressor
    std::ifstream ifs(filePath.string().c_str(), std::ios::in | std::ios::binary);
    boost::iostreams::filtering_streambuf<boost::iostreams::input> ifsb;
    ifsb.push(boost::iostreams::zlib_decompressor());
    ifsb.push(ifs);
    
    // De-serialize from disk
    std::istream is(&ifsb);
    proto.ParseFromIstream(&is);
 
    // Dimension data
    m_width      = proto.width();
    m_height     = proto.height();
    m_pixelWidth = proto.pixel_width();
    
    // Pixel bytes
    ASSERT(proto.pixel_data().size() == m_width * m_height * m_pixelWidth);
    m_pPixelData = new byte_t[proto.pixel_data().size()];
    std::memcpy((void*)m_pPixelData, (void*)&proto.pixel_data()[0], proto.pixel_data().size());
    
    // Usage (optional)
    if (proto.has_usage())
        m_usage = (TextureUsage)proto.usage();
    
    // Colorkey (optional, default = FFFFFF00)
    m_colorKey = Color(proto.color_key());
}

void Magnet::TextureFileSerializer::write(const fs::path& filePath, const uint8_t compressionLevel)
{
	ASSERT(m_pPixelData != nullptr);
	ASSERT(toLower(filePath.extension().string()) == FILE_EXT);
	ASSERT(compressionLevel <= 9);

    // Load protobuf file format class
    Proto::TextureFileFormat proto;
    
    // Dimension data
    proto.set_width(m_width);
    proto.set_height(m_height);
    proto.set_pixel_width(m_pixelWidth);
    
    // Pixel bytes
    proto.set_pixel_data(m_pPixelData, m_width * m_height * m_pixelWidth);
    
    // Usage
    proto.set_usage(static_cast<uint32_t>(m_usage));
    
    // Colorkey
    proto.set_color_key((uint32_t)m_colorKey);
    
    // Setup file stream buffer with ZLib compressor
    std::ofstream ofs(filePath.string().c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
    boost::iostreams::filtering_streambuf<boost::iostreams::output> ofsb;
    ofsb.push(boost::iostreams::zlib_compressor(compressionLevel));
    ofsb.push(ofs);

    // Serilize to disk
    std::ostream os(&ofsb);
    proto.SerializeToOstream(&os);
}

#if !defined(_MAG_NO_IMPORT)
void Magnet::TextureFileSerializer::importPng(const fs::path& filePath, const TextureUsage usage,
	const TextureImportOptions& options)
{
	ASSERT(m_pPixelData == nullptr);
	ASSERT(filePath.extension().string() == ".png");
	ASSERT(usage != Magnet::TU_UNSPECIFIED);

	// Open file
	FILE* pFile = std::fopen(filePath.string().c_str(), "rb");
	if (pFile == nullptr)
		throw RuntimeError("Cannot open file '" + filePath.string() + "'");

	// Check PNG sig
	png_byte sig[8];
	std::fread((void*)sig, 1, 8, pFile);
	if (png_sig_cmp(sig, 0, 8) != 0)
		throw RuntimeError("PNG sig bytes are not valid");

	// Setup PNG struct
	png_structp pPngStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
	if (pPngStruct == nullptr)
	{
		png_destroy_read_struct(&pPngStruct, nullptr, nullptr);
		throw RuntimeError("Out of memory");
	}

	// Setup PNG info
	png_infop pPngInfo = png_create_info_struct(pPngStruct);
	if (pPngInfo == nullptr)
	{
		png_destroy_read_struct(nullptr, &pPngInfo, nullptr);
		throw RuntimeError("Out of memory");
	}

	// Weird PNG error handler block (run by libpng on an error)
	if (setjmp(pPngStruct->jmpbuf)) 
	{
		png_destroy_read_struct(&pPngStruct, &pPngInfo, nullptr);
		throw RuntimeError("Internal libpng error");
	}

	// Tell libpng about the file
	png_init_io(pPngStruct, pFile);

	// Tell libpng to skip sig bytes as they've already been checked
	png_set_sig_bytes(pPngStruct, 8);

	// Read PNG file header
	png_read_info(pPngStruct, pPngInfo);

	// Set texture width and height in pixels
	m_width  = pPngInfo->width;
	m_height = pPngInfo->height;
	
	// Warn if width and height arn't power of equal
	LOG_WARNING_IF(m_width != m_height, "TextureFile width and height are not equal");

	// Warn if width or height arn't power of two
	LOG_WARNING_IF((!Magnet::isPowerOfTwo(m_width)) || (!Magnet::isPowerOfTwo(m_height)), 
		"TextureFile dimensions are not a power of two");

	// Array of pointers used in image read
	png_bytep* pRowPtrs = new png_bytep[m_height];

	// Per-usage format checking etc.
	if (usage == Magnet::TU_DIFFUSE_MAP)
	{
		// Setup some transformations for 32-bit RGBA requirement
		switch (pPngStruct->color_type)
		{
		case PNG_COLOR_TYPE_PALETTE:
			png_set_palette_to_rgb(pPngStruct); // Convert to RGB
			png_set_filler(pPngStruct, 0xFF, PNG_FILLER_AFTER); // Add opaque alpha channel
			break;

		case PNG_COLOR_TYPE_RGB:
			png_set_filler(pPngStruct, 0xFF, PNG_FILLER_AFTER); // Add opaque alpha channel
			break;
		}

		// Expand 1, 2 and 4 bit pixel depths to 8 bit
		if (pPngInfo->bit_depth < 8)
			png_set_packing(pPngStruct);

		// Convert transparency information to full alpha channel
		if (png_get_valid(pPngStruct, pPngInfo, PNG_INFO_tRNS)) 
			png_set_tRNS_to_alpha(pPngStruct);

		// Allocate buffer
		m_pPixelData = new byte_t[m_width * m_height * 4];

		// Organise pixel pointers
		for (uint32_t i = 0; i < m_height; i++)
			pRowPtrs[i] = (png_bytep)m_pPixelData + (m_width * i * 4);
	}
	else throw LogicError("Unreachable code");
			
	// Do actual image read
	png_read_image(pPngStruct, pRowPtrs);
	png_read_end(pPngStruct, nullptr);

	// Some more per-usage stuff
	if (usage == Magnet::TU_DIFFUSE_MAP)
	{
		// Set pixel width to 4 bytes
		m_pixelWidth = 4;

		// If using a colorkey set colorkey pixels to transparent black
		if (options.flags & TIF_USE_COLOUR_KEY)
		{
			for (uint32_t i = 0; i < m_width * m_height; i++)
			{
				dword_t* pPixel = (dword_t*)m_pPixelData + i;
				if (*((byte_t*)pPixel + 0) == options.colorKey.r &&
					*((byte_t*)pPixel + 1) == options.colorKey.g &&
					*((byte_t*)pPixel + 2) == options.colorKey.b)
					*pPixel = 0x00000000;
			}
		}
	}
	else throw LogicError("Unreachable code");

	// Cleanup row pointers
	delete [] pRowPtrs;
}
#endif

#if !defined(_MAG_NO_IMPORT)
void Magnet::TextureFileSerializer::import(const fs::path& filePath, const TextureUsage usage, 
	const TextureImportOptions options)
{
	ASSERT(m_pPixelData == nullptr);
	ASSERT(usage != Magnet::TU_UNSPECIFIED);

	// Save usage
	m_usage = usage;

	// PNG
	if (filePath.extension().string() == ".png")
		this->importPng(filePath, usage, options);

	// Unsupported format
	else throw RuntimeError("'" + filePath.extension().string() + "' is not a supported texture extension");
}
#endif