
#ifndef _MAGNET_UTIL_SHAPES_H
#define _MAGNET_UTIL_SHAPES_H

#include <MagnetUtil/external.h>

namespace Magnet
{
	template <uint8_t NDIMS>
	struct Shape {};

	typedef Shape<2> Shape2D;

	typedef Shape<3> Shape3D;

	struct Rectangle : public Shape2D
	{
		unsigned top, bottom, left, right;

		inline Rectangle() : top(0), bottom(0), left(0), right(0) {}

		inline Rectangle(unsigned top, unsigned bottom, unsigned left, unsigned right) {}
	};
}

#endif