#ifndef _MAGNET_UTIL_TEXTURE_DATA_H
#define _MAGNET_UTIL_TEXTURE_DATA_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	// TextureUsage enumeration
	enum TextureUsage
	{
		TU_UNSPECIFIED,
		TU_DIFFUSE_MAP,
		// TU_HEIGHT_MAP,
	};

	#if !defined(_MAG_NO_IMPORT)

	enum TextureImportFlags
	{
		TIF_USE_COLOUR_KEY = 0x01,
	};

	struct TextureImportOptions
	{
		uint32_t flags;
		Color colorKey;
		inline TextureImportOptions() : flags(0), colorKey(Color(255,255,255,255)) {}
	};

	#endif

	// TextureFileSerializer struct
	class TextureFileSerializer : public FileSerializer
	{
	public:

		static const std::string FILE_EXT;

		inline TextureFileSerializer() : m_width(0), m_height(0), m_pixelWidth(0),
			m_pPixelData(nullptr), m_usage(TU_UNSPECIFIED), m_colorKey(255, 255, 255, 255) {}

		inline ~TextureFileSerializer() { SAFE_DELETE(m_pPixelData); }

		inline uint32_t getWidth() const { ASSERT(m_pPixelData); return m_width; }

		inline uint32_t getHeight() const { ASSERT(m_pPixelData); return m_height; }

		inline uint8_t getPixelWidth() const { ASSERT(m_pPixelData); return m_pixelWidth; }

		inline byte_t* getPixelData() const { ASSERT(m_pPixelData); return m_pPixelData; }

		inline TextureUsage getUsage() const { ASSERT(m_pPixelData); return m_usage; }

		inline Color getColorKey() const { ASSERT(m_pPixelData); return m_colorKey; }

		void read(const fs::path& filePath);

		void write(const fs::path& filePath, const uint8_t compressionLevel = 9);

		#if !defined(_MAG_NO_IMPORT)		
		void import(const fs::path& filePath, const TextureUsage usage, 
			const TextureImportOptions options = TextureImportOptions());
		#endif

	protected:
		
		#if !defined(_MAG_NO_IMPORT)
		void importPng(const fs::path& filePath, const TextureUsage usage,
			const TextureImportOptions& options);
		#endif

	private:

		uint32_t m_width;

		uint32_t m_height;

		uint8_t m_pixelWidth;

		byte_t* m_pPixelData;

		TextureUsage m_usage;

		Color m_colorKey;
	};
}

#endif