#include "external.h"
#include <MagnetUtil/Variant.h>
#include <MagnetUtil/debug.h>

Magnet::Variant::Variant() :
	m_intValue(0), m_type(VT_INTEGER)
{
}

Magnet::Variant::Variant(int intValue) : 
	m_intValue(intValue), m_type(VT_INTEGER)
{
}

Magnet::Variant::Variant(float floatValue) : 
	m_floatValue(floatValue), m_type(VT_FLOAT)
{
}

Magnet::Variant::Variant(bool boolValue) : 
	m_boolValue(boolValue), m_type(VT_BOOL)
{
}

bool Magnet::Variant::operator == (const Variant& other) const
{
	if (m_type == other.m_type)
	{
		switch (m_type)
		{
		case VT_INTEGER: return m_intValue == other.m_intValue;
		case VT_FLOAT:   return m_floatValue == other.m_floatValue;
		case VT_BOOL:    return m_boolValue == other.m_boolValue;
		}
		return true; // Never reached
	}
	else return false;
}

bool Magnet::Variant::operator != (const Variant& other) const
{
	if (m_type == other.m_type)
	{
		switch (m_type)
		{
		case VT_INTEGER: return m_intValue != other.m_intValue;
		case VT_FLOAT:   return m_floatValue != other.m_floatValue;
		case VT_BOOL:    return m_boolValue != other.m_boolValue;
		}
		return false; // Never reached
	}
	else return true;
}

int Magnet::Variant::asInt() const
{
	ASSERT(m_type == VT_INTEGER);
	return m_intValue;
}

float Magnet::Variant::asFloat() const
{
	ASSERT(m_type == VT_FLOAT);
	return m_floatValue;
}

bool Magnet::Variant::asBool() const
{
	ASSERT(m_type == VT_BOOL);
	return m_boolValue;
}

void Magnet::Variant::set(int intValue)
{
	m_intValue = intValue;
	m_type = VT_INTEGER;
}

void Magnet::Variant::set(float floatValue)
{
	m_floatValue = floatValue;
	m_type = VT_FLOAT;
}

void Magnet::Variant::set(bool boolValue)
{
	m_boolValue = boolValue;
	m_type = VT_BOOL;
}
