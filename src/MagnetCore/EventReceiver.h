#ifndef _MAGNET_CORE_EVENT_RECEIVER_H
#define _MAGNET_CORE_EVENT_RECEIVER_H

namespace Magnet 
{
	class Event;

	class EventReceiver
	{
	public:

		inline virtual ~EventReceiver() {}

		virtual void onEvent(Event& event) = 0;
	};
}

#endif