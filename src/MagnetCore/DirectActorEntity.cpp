#include "external.h"
#include <MagnetUtil/common.h>

Magnet::DirectActorEntity::DirectActorEntity()
{
	m_offset.identity();
	m_position.identity();
	m_orientation.identity();
}

Magnet::Matrix4x4 Magnet::DirectActorEntity::getWorldTransform()
{
	return this->getAsset()->getOffset() * m_offset * m_orientation * m_position;
}