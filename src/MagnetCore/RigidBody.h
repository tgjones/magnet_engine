
#ifndef _MAGNET_CORE_RIGID_BODY_H
#define _MAGNET_CORE_RIGID_BODY_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class RigidBody
	{
	public:

		inline RigidBody() {}

		inline std::vector<Vector3>& getBoxes() { ASSERT(!m_boxes.empty()); return m_boxes; }

		inline void addBox(const Vector3& box) { m_boxes.push_back(box); }

	private:

		std::vector<Vector3> m_boxes;
	};
}

#endif