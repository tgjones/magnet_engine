#include "external.h"
#include <MagnetUtil/common.h>

Magnet::SpriteEntity::SpriteEntity(HMediaAsset hSprite) : Entity(GO_SPRITE),
		m_hSprite(hSprite), m_pSprite(nullptr), m_pPlayingAnimation(nullptr)
{
	// Lock sprite
	m_pSprite = static_cast<SpriteAsset*>(g_pMediaManager->lockAsset(hSprite));

	// Resolve default frame rectangle
	//Rectangle* pDefaultFrame = &m_pSprite->getDefaultFrame();

	// Set the default size (which also sets the centroid)
	//m_size.x = (float)(pDefaultFrame->right - pDefaultFrame->left);
	//m_size.y = (float)(pDefaultFrame->bottom - pDefaultFrame->top);

	// Set the default position
	m_position.x = 0;
	m_position.y = 0;
}

Magnet::SpriteEntity::~SpriteEntity()
{
	// Unlock sprite
	g_pMediaManager->unlockAsset(m_hSprite);
}

//! @todo Throw exception if animation is not found, currently silent
void Magnet::SpriteEntity::startAnimation(const std::string& name, bool loop)
{
	m_pPlayingAnimation = &m_pSprite->getAnimation(name);
	m_animationLooping = loop;
	m_animationStartTime = g_pApplication->getAbsoluteTime() / 1000.0f;
}

Magnet::Rectangle& Magnet::SpriteEntity::getCurrentFrame()
{
	// If animation is playing
	if (m_pPlayingAnimation != nullptr)
	{
		// Get current time
		double drawTime = g_pApplication->getAbsoluteTime() / 1000.0f;

		// Get current frame from animation, or nullptr if the animation is finished
		Rectangle* pFrame = m_pPlayingAnimation->getFrame(drawTime - m_animationStartTime);

		// If there is no frame for the specified time
		if (!pFrame)
		{
			// Start animation again
			if (m_animationLooping)
				m_animationStartTime = drawTime;

			// Finish animation
			else
				m_pPlayingAnimation = nullptr;

			// Return the default frame
			return m_pSprite->getDefaultFrame();
		}

		// Return the current frame
		return *pFrame;
	}

	// No animation playing so return default fram
	else return m_pSprite->getDefaultFrame();
}
