#ifndef _MAGNET_CORE_RENDERER_H
#define _MAGNET_CORE_RENDERER_H

#include <MagnetUtil/common.h>

namespace Magnet {

	class Renderer : public Subsystem
	{
	public:

		inline virtual ~Renderer() {}

		virtual void startup();

		virtual void render() = 0;

		virtual void reshape() = 0;

		virtual void reset() = 0;

		virtual void createDiffuseMap(const std::string& id, const TextureFileSerializer& data) = 0;

		virtual void releaseDiffuseMap(const std::string& id) = 0;

		// virtual void createHeightMap(const std::string& id, byte_t* pData, uint16_t width, uint16_t height) = 0;

		// virtual void releaseHeightMap(const std::string& id) = 0;

		ViewPort* getScreen() { ASSERT(!m_hScreen.isNull()); return m_viewPorts[m_hScreen]; }

		HViewPort addViewPort(const float top, const float left, const float height, const float width);

		inline ViewPort* getViewPort(const HViewPort hViewPort) { return m_viewPorts[hViewPort]; }

		inline HandleController<ViewPort, HViewPort>& getAllViewPorts() { return m_viewPorts; }

	protected:

		friend class SubsystemFactory;

		inline Renderer() : m_shaderPath("") {}

		HViewPort m_hScreen;

		HandleController<ViewPort, HViewPort> m_viewPorts;

		std::string m_shaderPath;
	};
}

#endif