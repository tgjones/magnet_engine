
#ifndef _MAGNET_CORE_ACTOR_H
#define _MAGNET_CORE_ACTOR_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class ActorAsset : public MediaAsset
	{
	public:

		inline ActorAsset() { m_offset.identity(); }

		void setModel(HMediaFile hModel);

		ModelFile* getModel();

		void setOffset(const Matrix4x4& offset) 
		{ 
			m_offset = offset; 
		}

		Matrix4x4& getOffset() 
		{ 
			return m_offset; 
		}

	private:

		HMediaFile m_hModel;

		Matrix4x4 m_offset;

	};
}

#endif