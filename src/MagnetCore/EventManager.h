#ifndef _MAGNET_CORE_EVENT_MANAGER_H
#define _MAGNET_CORE_EVENT_MANAGER_H

#include <map>
#include <set>
#include <MagnetUtil/common.h>

namespace Magnet {

	class SubsystemFactory;

	/// Event priority enumerations.
	enum EventPriority
	{
		EVENT_PRIORITY_LOW,
		EVENT_PRIORITY_MEDIUM,
		EVENT_PRIORITY_HIGH,
	};

	/// Event schedule mode enumerations.
	enum EventScheduleMode
	{
		EVENT_SCHEDULE_FRAMES,
		EVENT_SCHEDULE_TICKS
	};
		
	class EventManager : public Subsystem
	{
	public:

		/// Virtual destructor.
		inline virtual ~EventManager() {}

		/// Startup the event subsytem.
		inline void startup() {}

		/// Shutdown the event subsystem.
		inline void shutdown() {}

		virtual void registerReceiver(const std::string& directive, EventReceiver& receiver);

		virtual void deregisterReceiver(const std::string& directive, EventReceiver& receiver);

		virtual void sendEvent(Event& event);

		virtual void enqueueEvent(Event& event, EventPriority priority);

		virtual void scheduleEvent(Event& event, EventPriority priority, EventScheduleMode mode, uint32_t delay);

		// Called at start of frame
		virtual void dispatchScheduledEvents();

		// Called just before render()
		virtual void dispatchQueuedEvents();

	protected:

		inline EventManager() {}

	private:

		/// EventManager is a singleton and Engine needs access to the protected constructor.
		friend class SubsystemFactory;
		
		typedef std::tr1::unordered_map<std::string, std::set<EventReceiver*> > Receivers;
		Receivers m_receivers;

		// Queues
		typedef std::queue<Event> PriorityQueue;
		PriorityQueue m_highPriorityQueue;
		PriorityQueue m_mediumPriorityQueue;
		PriorityQueue m_lowPriorityQueue;

		// Schedules
		typedef std::map<EventScheduleMode, std::map<uint32_t, Event> > PrioritySchedule;
		PrioritySchedule m_highPrioritySchedule;
		PrioritySchedule m_mediumPrioritySchedule;
		PrioritySchedule m_lowPrioritySchedule;

		// Dispatch helper function
		void dispatchScheduledEventsHelper(PrioritySchedule::iterator& itr);
	};
}

#endif