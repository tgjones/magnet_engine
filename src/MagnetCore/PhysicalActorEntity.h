#ifndef _MAGNET_CORE_PHYSICAL_ACTOR_INSTANCE_H
#define _MAGNET_CORE_PHYSICAL_ACTOR_INSTANCE_H

#ifndef _MAG_NO_PHYSICS

#include <MagnetUtil/common.h>

namespace Magnet
{
	class PhysicalActorEntity : public ActorEntity
	{
	public:

		PhysicalActorEntity();

		inline virtual ~PhysicalActorEntity() {}

		Matrix4x4 getWorldTransform();

		inline void setVisible(bool visible) { m_visible = visible; }

		inline bool isVisible() { return m_visible; }

		inline void setDensity(float density) { m_density = density; }

		inline float getDensity() { return m_density; }

		inline void setOffset(const Matrix4x4& offset) { m_offset = offset; }

		inline Matrix4x4& getOffset() { return m_offset; }

		inline void setInitPose(const Matrix4x4& initPose) { m_initPose = initPose; }

		inline Matrix4x4& getInitPose() { return m_initPose; }

	protected:

		// Override GameWorld spawn handler
		void onSpawn();

		// Override GameWorld kill handler
		void onKill();

	private:

		// For onSpawn() and onKill() handlers
		friend class GameWorld;

		bool m_visible;

		HPhysicalActorInstance m_hPhysicsHandle;

		float m_density;

		Matrix4x4 m_offset;

		Matrix4x4 m_initPose;
	};
}

#endif
#endif