#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::Entity::setParent(HEntity hParent)
{
	// Orphan the object first
	this->orphan();

	// Set the new parent handle
	m_hParent = hParent;

	// Dereference handle to new parent
	Entity* pParent = g_pGameWorld->getEntity(m_hParent);

	// Add new child relationship
	pParent->m_children.insert(m_hSelf);
}

Magnet::Entity* Magnet::Entity::getParent()
{
	ASSERT(!m_hParent.isNull());
	return g_pGameWorld->getEntity(m_hParent);
}

void Magnet::Entity::orphan()
{
	if (!m_hParent.isNull())
	{
		// Dereference handle to old parent
		Entity* pOldParent = g_pGameWorld->getEntity(m_hParent);

		// Find iterator to child handle
		std::set<HEntity>::iterator itr = pOldParent->m_children.find(m_hSelf);

		// Check child relationship is there
		ASSERT(itr != pOldParent->m_children.end());

		// Delete child relationship
		pOldParent->m_children.erase(itr);

		// Nullify handle
		m_hParent.setNull();
	}
}
