#ifndef _MAGNET_CORE_ACTOR_INSTANCE_H
#define _MAGNET_CORE_ACTOR_INSTANCE_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class ActorEntity : public Entity
	{
	public:

		inline ActorEntity() : Entity(GO_ACTOR) {}

		virtual ~ActorEntity();

		void setAsset(const std::string& id);

		void setAsset(const HMediaAsset hActor);

		ActorAsset* getAsset();

		inline HMediaAsset getAssetHandle() { return m_hActor; }

		virtual void setVisible(bool visible) = 0;

		virtual bool isVisible() = 0;

		virtual Matrix4x4 getWorldTransform() = 0;

		virtual void setOffset(const Matrix4x4& offset) = 0;

		virtual Matrix4x4& getOffset() = 0;

	private:

		HMediaAsset m_hActor;
	};
}

#endif