#include "external.h"

#if !defined(_MAG_NO_PHYSICS) && defined(_MAG_USE_PHYSX)

#include <MagnetUtil/common.h>

// Set time-stepping to one 60th of a second
const double Magnet::PhysXPhysicsEngine::TIME_STEP = 1.0f / 60.0f;

#ifdef _MAG_DEBUG

namespace
{
	// PhysX visual debugger listener functionality
	class PVDHandler : public NxRemoteDebuggerEventListener
	{
	public:

		PVDHandler(NxPhysicsSDK* pNxPhysicsSDK) : m_pNxPhysicsSDK(pNxPhysicsSDK) {}

		void updateCameras()
		{
			// Update viewport cameras in visual debugger
			if (m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->isConnected())
			{
				for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
				{
					// Dereference viewport
					Magnet::ViewPort* pViewPort = g_pRenderer->getAllViewPorts()[i];

					// New camera position (reverse Z axis)
					m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->writeParameter(NxVec3(pViewPort->getCamera()->getPosition().x,
						pViewPort->getCamera()->getPosition().y, -pViewPort->getCamera()->getPosition().z), (void*)pViewPort, false, "Origin", NX_DBG_EVENTMASK_EVERYTHING);

					// New camera target (reverse Z axis)
					m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->writeParameter(NxVec3(pViewPort->getCamera()->getTarget().x,
						pViewPort->getCamera()->getTarget().y, -pViewPort->getCamera()->getTarget().z), (void*)pViewPort, false, "Target", NX_DBG_EVENTMASK_EVERYTHING);
				}
			}
		}

		virtual void onConnect()
		{
			// Setup in-game viewport cameras
			for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
			{
				// Dereference viewport
				Magnet::ViewPort* pViewPort = g_pRenderer->getAllViewPorts()[i];

				// Create view port camera
				m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->createObject(pViewPort, NX_DBG_OBJECTTYPE_CAMERA, pViewPort->getName().c_str(), NX_DBG_EVENTMASK_EVERYTHING);

				// CameraEntity position (reverse Z axis)
				m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->writeParameter(NxVec3(pViewPort->getCamera()->getPosition().x,
						pViewPort->getCamera()->getPosition().y, -pViewPort->getCamera()->getPosition().z), (void*)pViewPort, true, "Origin", NX_DBG_EVENTMASK_EVERYTHING);

				// CameraEntity target (reverse Z axis)
				m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->writeParameter(NxVec3(pViewPort->getCamera()->getPosition().x,
						pViewPort->getCamera()->getPosition().y, -pViewPort->getCamera()->getPosition().z), (void*)pViewPort, true, "Target", NX_DBG_EVENTMASK_EVERYTHING);

				// CameraEntity up (reverse Z axis)
				m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->writeParameter(NxVec3(pViewPort->getCamera()->getPosition().x, 
					pViewPort->getCamera()->getPosition().y, pViewPort->getCamera()->getPosition().z), (void*)pViewPort, true, "Up", NX_DBG_EVENTMASK_EVERYTHING);
			}
		}

		virtual void onDisconnect()
		{
			// Remove viewport cameras
			if (m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->isConnected())
				for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
					m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->removeObject((void*)g_pRenderer->getAllViewPorts()[i], NX_DBG_EVENTMASK_EVERYTHING);
		}

	private:

		NxPhysicsSDK* m_pNxPhysicsSDK;
	};

	static PVDHandler* g_pPVDHandler;
}

#endif

void Magnet::PhysXPhysicsEngine::startup()
{
	// Initialise PhysX
	m_pNxPhysicsSDK = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION);
	if (m_pNxPhysicsSDK == nullptr)
		throw RuntimeError("Unable to initialise the PhysX SDK");

	LOG_DEBUG("PhysX SDK successfully created");

	#ifdef _MAG_DEBUG

	// Create event listener
	g_pPVDHandler = new PVDHandler(m_pNxPhysicsSDK);

	// Register event listener
	m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->registerEventListener(g_pPVDHandler);

	// Get remote debugger settings from configuration file
	std::string debugHost = g_pConfigurationManager->getParameterAsString("remoteDebugHost", "localhost");
	uint32_t debugPort = (uint32_t)g_pConfigurationManager->getParameterAsLong("remoteDebugPort", 5425);

	// Attempt to connect to remote debugger
	m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->connect(debugHost.c_str(), debugPort);

	// Display log message
	LOG_DEBUG_IF(m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->isConnected(), "Connected to PhysX remote debugger at '" 
		+ debugHost + ":" + toString(debugPort) + "'");

	#endif

	// Create scene with default gravity
	NxSceneDesc sceneDesc;
	sceneDesc.gravity = NxVec3(0.0f, -9.81f, 0.0f);
	m_pNxScene = m_pNxPhysicsSDK->createScene(sceneDesc);
	if (m_pNxScene == nullptr)
		throw RuntimeError("Unable to create PhysX scene");

	// Setup the timing
	m_pNxScene->setTiming((NxReal)(TIME_STEP / 4.0f), 4);

	// Set the defailt material
	NxMaterial* defaultMaterial = m_pNxScene->getMaterialFromIndex(0);
	defaultMaterial->setRestitution(0.0f);
	defaultMaterial->setStaticFriction(0.5f);
	defaultMaterial->setDynamicFriction(0.5f);

}

void Magnet::PhysXPhysicsEngine::shutdown()
{
	if (m_pNxPhysicsSDK != nullptr)
	{
		// Shutdown API threahds
		m_pNxScene->shutdownWorkerThreads();

		// Delete scene
		if (m_pNxScene != nullptr)
			m_pNxPhysicsSDK->releaseScene(*m_pNxScene);
		m_pNxScene = nullptr;

		#ifdef _MAG_DEBUG

		// Disconnect from remote debugger
		if (m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->isConnected())
			m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->disconnect();

		// Deregister event listender
		m_pNxPhysicsSDK->getFoundationSDK().getRemoteDebugger()->unregisterEventListener(g_pPVDHandler);

		// Delete event listener
		SAFE_DELETE(g_pPVDHandler);

		#endif

		// Deinitialise PhysX
		NxReleasePhysicsSDK(m_pNxPhysicsSDK);
		m_pNxPhysicsSDK = nullptr;

		LOG_DEBUG("PhysX SDK released");
	}
}

void Magnet::PhysXPhysicsEngine::update(double lastFrameTime)
{
	// Timing
	static double elapsed = 0.0f;
	elapsed += lastFrameTime;

	if (elapsed >= TIME_STEP)
	{
		// Reset timing counter
		elapsed = 0.0f;

		// Simulate and flush
		m_pNxScene->simulate((NxReal)TIME_STEP);
		m_pNxScene->flushStream();

		// Block until PhysX thread is finished
		while(!m_pNxScene->fetchResults(NX_RIGID_BODY_FINISHED, true))
		{
			// TODO: Do something useful while we're waiting for PhysX thread to finish
		}
	}

	// Update cameras in PVD
	#ifdef _MAG_DEBUG
	g_pPVDHandler->updateCameras();
	#endif
}

Magnet::Handle<Magnet::Plane> Magnet::PhysXPhysicsEngine::createPlane(Plane& plane)
{
	// Plane descriptor
	NxPlaneShapeDesc nxPlaneShapeDesc;
	nxPlaneShapeDesc.d = plane.getDistance();
	nxPlaneShapeDesc.normal = *((NxVec3*)&plane.getNormal());

	// Create actor descriptor
	NxActorDesc nxActorDesc;
	nxActorDesc.shapes.pushBack(&nxPlaneShapeDesc);

	// Create a handle
	Handle<Plane> handle;
	
	// Add entity to PhysX scene and acquire handle
	m_planes.acquire(handle, m_pNxScene->createActor(nxActorDesc));

	// Return handle
	return handle;
}

void Magnet::PhysXPhysicsEngine::releasePlane(const Magnet::Handle<Magnet::Plane> hPlane)
{
	// Dereference rigid body
	NxActor* pNxActor = m_planes.dereference(hPlane);

	// Release from PhysX scene
	m_pNxScene->releaseActor(*pNxActor);

	// Release handle
	m_planes.release(hPlane);
}

Magnet::HPhysicalActorInstance Magnet::PhysXPhysicsEngine::createActorInstance(
	PhysicalActorEntity& instance, Matrix4x4& initTransform)
{
	// Dereference model
	ModelFile* pModel = instance.getAsset()->getModel();

	// Create body descriptor
	NxBodyDesc nxBodyDesc;
	//nxBodyDesc.angularDamping = 0.5f;

	// Create actor descriptor
	NxActorDesc nxActorDesc;
	nxActorDesc.density = instance.getDensity();
	ASSERT(nxBodyDesc.isValid());
	nxActorDesc.body = &nxBodyDesc;

	// Point PhysX to the asset ID
	#ifdef _MAG_DEBUG
	nxActorDesc.name = &pModel->getId()[0];
	#endif

	// For each rigid body
	BOOST_FOREACH(RigidBody body, pModel->getRigidBodies())
	{
		// Add boxes
		BOOST_FOREACH(Vector3 box, body.getBoxes())
		{
			NxBoxShapeDesc nxBox;

			// Transform rigid body dimensions to match initial transform
			nxBox.dimensions = *((NxVec3*)&(Vector4(box, 0.f) * initTransform));

			ASSERT(nxBox.isValid());
			nxActorDesc.shapes.pushBack(&nxBox);
		}
	}

	// Set initial pose - use setColumnMajor44() as PhysX expects a transpose of the row-major Matrix4x4
	// format (like OpenGL) for its (column-vector based) calulations.
	nxActorDesc.globalPose.setColumnMajor44(initTransform.getDataPtr());
	
	// Create a handle
	Handle<PhysicalActorEntity> handle;
	
	// Add entity to PhysX scene and acquire handle
	ASSERT(nxActorDesc.isValid());
	m_actorInstances.acquire(handle, m_pNxScene->createActor(nxActorDesc));

	// Return handle
	return handle;
}

void Magnet::PhysXPhysicsEngine::releaseActorInstance(const HPhysicalActorInstance hActorInstance)
{
	// Dereference actor instance
	NxActor* pNxActor = m_actorInstances.dereference(hActorInstance);

	// Release from PhysX scene
	m_pNxScene->releaseActor(*pNxActor);

	// Release handle
	m_actorInstances.release(hActorInstance);
}

Magnet::Matrix4x4 Magnet::PhysXPhysicsEngine::getActorInstanceTransform(const HPhysicalActorInstance hActorInstance)
{
	// Dereference actor instance
	NxActor* pNxActor = m_actorInstances.dereference(hActorInstance);

	// Create matrix
	Matrix4x4 transform;

	// Get pose - use getColumnMajor44() as PhysX uses a transpose of the row-major Matrix4x4
	// format (like OpenGL) for its (column-vector based) calulations.
	pNxActor->getGlobalPose().getColumnMajor44(transform.getDataPtr());

	// Return pose
	return transform;
}

void Magnet::PhysXPhysicsEngine::setGravity(Vector3 gravity)
{
	ASSERT(m_pNxScene != nullptr);
	m_pNxScene->setGravity(*reinterpret_cast<NxVec3*>(&gravity));
}

Magnet::Vector3 Magnet::PhysXPhysicsEngine::getGravity()
{
	ASSERT(m_pNxScene != nullptr);
	Vector3 gravity;
	m_pNxScene->getGravity(*reinterpret_cast<NxVec3*>(&gravity));
	return gravity;
}

#endif
