#ifndef _MAGNET_CORE_MEDIA_MANAGER_H
#define _MAGNET_CORE_MEDIA_MANAGER_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class MediaManager : public Subsystem
	{
	public:

		void startup();

		void shutdown();

		inline virtual ~MediaManager() {}

		HMediaAsset getAssetHandleById(const std::string& id);

		MediaAsset* getAsset(HMediaAsset handle);

		MediaAsset* getAsset(const std::string& id);

		MediaAsset* lockAsset(HMediaAsset handle);

		MediaAsset* lockAsset(const std::string& id);

		void unlockAsset(HMediaAsset handle);

		void unlockAsset(const std::string& id);

		HMediaFile getItemHandleById(const std::string& id);

		MediaFile* getFile(HMediaFile handle);

		MediaFile* getFile(const std::string& id);

		MediaFile* lockFile(HMediaFile handle);

		MediaFile* lockFile(const std::string& id);

		void unlockFile(HMediaFile handle);

		void unlockFile(const std::string& id);

	protected:

		void parseManifest(const fs::path& filePath);

		inline MediaManager() : m_mediaRoot("") {}

	private:

		friend class SubsystemFactory;

		fs::path m_mediaRoot;

		HandleController<MediaFile*, HMediaFile> m_files;
		
		HandleController<MediaAsset*, HMediaAsset> m_assets;

		typedef std::tr1::unordered_map<std::string, HMediaFile> FileHandlesById;
		FileHandlesById m_fileHandlesById;
		
		typedef std::tr1::unordered_map<std::string, HMediaAsset> AssetHandlesById;
		AssetHandlesById m_assetHandlesById;
	};
}

#endif