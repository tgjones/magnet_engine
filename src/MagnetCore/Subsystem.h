#ifndef _MAGNET_CORE_SUBSYSTEM_H
#define _MAGNET_CORE_SUBSYSTEM_H

#include <boost/noncopyable.hpp>

namespace Magnet 
{
	class Subsystem : public boost::noncopyable
	{
	public:
		inline virtual ~Subsystem() {}

		inline virtual void startup() {}
		inline virtual void shutdown() {}
	};
}

#endif