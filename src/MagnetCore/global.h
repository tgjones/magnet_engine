#ifndef _MAGNET_CORE_GLOBAL_H
#define _MAGNET_CORE_GLOBAL_H

// Include util macros and stuff
#include <MagnetUtil/global.h>

// Global subsystem variables
#define g_pApplication			::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_APPLICATION>()
#define g_pRenderer				::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_RENDERER>()
#define g_pPhysicsEngine		::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_PHYSICS_ENGINE>()
#define g_pEventManager			::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_EVENT_MANAGER>()
#define g_pMediaManager			::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_MEDIA_MANAGER>()
#define g_pConfigurationManager ::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_CONFIGURATION_MANAGER>()
#define g_pInputEngine			::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_INPUT_ENGINE>()
#define g_pGameWorld			::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_GAME_WORLD>()
#define g_pGame					::Magnet::SubsystemFactory::getSubsystem<Magnet::ST_GAME>()

// Universal Magnet::SubsystemFactory commands
#define REGISTER_SUBSYSTEM(ST_ENUM, T) ::Magnet::SubsystemFactory::registerType<ST_ENUM, T>()
#define RELEASE_SUBSYSTEM(ST_ENUM)     ::Magnet::SubsystemFactory::getSubsystem<ST_ENUM>(true)

#endif