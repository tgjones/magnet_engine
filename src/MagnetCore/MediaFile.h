
#ifndef _MAGNET_CORE_MEDIA_ITEM_H
#define _MAGNET_CORE_MEDIA_ITEM_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class MediaFile
	{
	public:

		inline MediaFile() : m_lockCount(0), m_id(""), m_filePath("") {}

		inline virtual ~MediaFile() {}

		inline void setId(const std::string& id) { m_id = id; }

		inline const std::string& getId() { ASSERT(m_id != ""); return m_id; }

		inline void setFilePath(const fs::path& filePath) { m_filePath = filePath; }

		inline const fs::path& getFilePath() { ASSERT(!m_filePath.empty()); return m_filePath; }

		inline void lock() { m_lockCount++; if (!isLoaded()) this->load(); }

		inline void unlock() { ASSERT(m_lockCount > 0); m_lockCount--; }

		inline bool isLocked() { return m_lockCount > 0; }

		virtual void load() = 0;

		virtual void unload() = 0;

		virtual bool isLoaded() = 0;

	protected:

		std::string m_id;

		fs::path m_filePath;

	private:

		uint32_t m_lockCount;
	};
}

#endif