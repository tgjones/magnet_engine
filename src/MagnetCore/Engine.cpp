#include "external.h"
#include <MagnetUtil/common.h>		

void Magnet::Engine::startup(const fs::path& configFile)
{	
	// Ensure application class has already been registered
	ASSERT(SubsystemFactory::isRegistered(ST_APPLICATION));

	// Call pre-startup event
	g_pApplication->onPreEngineStartup();

	// Version message
	std::stringstream ss;
	ss << "Magnet Engine version " << _MAG_VER_STR << " starting up...";
	LOG_INFO(ss.str().c_str());
	
	// Register platform/API agnostic default types
	REGISTER_SUBSYSTEM(ST_GAME_WORLD, GameWorld);
	REGISTER_SUBSYSTEM(ST_EVENT_MANAGER, EventManager);
	REGISTER_SUBSYSTEM(ST_MEDIA_MANAGER, MediaManager);
	REGISTER_SUBSYSTEM(ST_CONFIGURATION_MANAGER, ConfigurationManager);
	
	// Start configuration manager and parse default and custom config files
	g_pConfigurationManager->startup();
    
    // Optionally load default configuration file
    try 
    {
        g_pConfigurationManager->parseConfigFile("default.cfg");
    } 
    catch (RuntimeError& e) 
    {
        
        // Warn if the configuration file was not found (error code -1)
        if (e.getCode() == ERR_CONFIG_FILE_UNREADABLE)
            LOG_WARNING("Configuration file 'default.cfg' not read");
        
        // Rethrow otherwise
        else throw e;
    }
    
    // Optionally load platform specific configuration file
    fs::path platformConfigFile = "";
    #if defined(_MAG_PLATFORM_WIN32)
    platformConfigFile = "default-win32.cfg";
    #elif defined(_MAG_PLATFORM_IOS)
    platformConfigFile = "default-ios.cfg";
    #elif defined(_MAG_PLATFORM_MACOSX)
    platformConfigFile = "default-macosx.cfg";
    #endif
    try 
    {
        if (platformConfigFile != "") g_pConfigurationManager->parseConfigFile(platformConfigFile);
    } 
    catch (RuntimeError& e) 
    {
        
        // Warn if the configuration file was not found
        if (e.getCode() == ERR_CONFIG_FILE_UNREADABLE)
            LOG_WARNING(std::string("Configuration file '" + platformConfigFile.string() + "' not read").c_str());
        
        // Rethrow otherwise
        else throw e;
    }
    
    // Load custom configuration file if one is specified
	if (configFile != "") g_pConfigurationManager->parseConfigFile(configFile);
		
	// Startup application which must register platform/API specific subsystem types
	g_pApplication->onPreStartup();
	g_pApplication->startup();
	g_pApplication->onPostStartup();

	// Confirm platform/API specific subsystems types were registered by g_pApplication->startup();
	ASSERT(SubsystemFactory::isRegistered(ST_RENDERER));
	ASSERT(SubsystemFactory::isRegistered(ST_INPUT_ENGINE));
	#ifndef _MAG_NO_PHYSICS
	ASSERT(SubsystemFactory::isRegistered(ST_PHYSICS_ENGINE));
	#endif

	// Startup game world first
	g_pGameWorld->startup();	

	// Startup remaining subsystems
	g_pRenderer->startup();
	g_pEventManager->startup();
	g_pMediaManager->startup();
	g_pInputEngine->startup();
	#ifndef _MAG_NO_PHYSICS
	g_pPhysicsEngine->startup();
	#endif

	// Call post-startup event
	g_pApplication->onPostEngineStartup();
}

void Magnet::Engine::shutdown()
{
	// Ensure application class has already been registered
	ASSERT(SubsystemFactory::isRegistered(ST_APPLICATION));
	
	// Call pre-shutdown event
	g_pApplication->onPreEngineShutdown();
	
	// Shutdown and dispose of g_pPhysicsEngine
	#ifndef _MAG_NO_PHYSICS
	if (SubsystemFactory::isRegistered(ST_PHYSICS_ENGINE))
	{
		g_pPhysicsEngine->shutdown();
		RELEASE_SUBSYSTEM(ST_PHYSICS_ENGINE);
	}
	#endif
	
	// Shutdown and dispose of g_pInputEngine
	if (SubsystemFactory::isRegistered(ST_INPUT_ENGINE))
	{
		g_pInputEngine->shutdown();
		RELEASE_SUBSYSTEM(ST_INPUT_ENGINE);
	}
	
	// Shutdown and dispose of g_pRenderer
	if (SubsystemFactory::isRegistered(ST_RENDERER))
	{
		g_pRenderer->shutdown();
		RELEASE_SUBSYSTEM(ST_RENDERER);
	}

	// Shutdown and dispose of g_pEventManager
	if (SubsystemFactory::isRegistered(ST_EVENT_MANAGER))
	{
		g_pEventManager->shutdown();
		RELEASE_SUBSYSTEM(ST_EVENT_MANAGER);
	}
	
	// Shutdown and dispose of g_pGameWorld
	if (SubsystemFactory::isRegistered(ST_GAME_WORLD))
	{
		g_pGameWorld->shutdown();
		RELEASE_SUBSYSTEM(ST_GAME_WORLD);
	}

	
	// Shutdown and dispose of g_pMediaManager
	if (SubsystemFactory::isRegistered(ST_MEDIA_MANAGER))
	{
		g_pMediaManager->shutdown();
		RELEASE_SUBSYSTEM(ST_MEDIA_MANAGER);
	}
	
	// Shutdown and dispose of g_pConfigurationManager
	if (SubsystemFactory::isRegistered(ST_CONFIGURATION_MANAGER))
	{
		g_pConfigurationManager->shutdown();
		RELEASE_SUBSYSTEM(ST_CONFIGURATION_MANAGER);
	}
	
	// Shutdown and dispose of g_pApplication
	g_pApplication->onPreShutdown();
	g_pApplication->shutdown();
	g_pApplication->onPostShutdown();
	RELEASE_SUBSYSTEM(ST_APPLICATION);

	// Call post-shutdown event
	g_pApplication->onPostEngineShutdown();
}