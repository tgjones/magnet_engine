
#ifndef _MAGNET_CORE_VERTEX_H
#define _MAGNET_CORE_VERTEX_H

#include <MagnetUtil/common.h>

// Based on Jason Hise's very enlightening article:
// http://www.entropygames.net/index.php?option=com_content&view=article&id=51:generic-vertices&catid=37:articles&Itemid=56

namespace Magnet 
{
	enum VertexAttributeEnum
	{
		VA_POSITION,
		VA_NORMAL,
		VA_DIFFUSE,
		VA_SPECULAR
	};

	struct AbstractVertex {};

	// Vertex Attribute root template
	template <VertexAttributeEnum> struct VertexAttribute : public AbstractVertex {};

	// VA_POSITION Attribute
	template <> struct VertexAttribute<VA_POSITION> { Vector3 position; };

	// VA_NORMAL Attribute
	template <> struct VertexAttribute<VA_NORMAL> {	Vector3 normal;	};

 	struct VertexNodeDescriptor
	{
		VertexAttributeEnum attribute;
		size_t offset;
	};

	struct VertexFormat
    {
        std::vector<VertexNodeDescriptor> nodes;
        size_t stride;
    };

	template <typename USAGE>
    struct VertexBuffer
    {
	public:

        typedef typename USAGE::Vertex Vertex;
        std::vector<Vertex> data;
    };

	template <VertexAttributeEnum VA, typename NEXT = void>
	class VertexNode
	{
	public:

		struct Vertex : public VertexAttribute<VA>, public NEXT::Vertex {};

		static void buildFormat(VertexFormat& fmt)
        {
            Vertex v;
            VertexNodeDescriptor n;
            n.attribute = VA;
            n.offset = offset;
            fmt.nodes.push_back(n);
			NEXT::offset = offset + reinterpret_cast<byte_t*>(static_cast<typename NEXT::Vertex*>(&v)) - reinterpret_cast<byte_t*>(&v);
			NEXT::buildFormat(fmt);
        }
    
	private:

		template <VertexAttributeEnum, typename> friend class VertexNode;

		static size_t offset;
	};

	template <VertexAttributeEnum VA>
	class VertexNode<VA, void>
	{
	public:

		struct Vertex : public VertexAttribute<VA> {};

		static void buildFormat(VertexFormat& fmt)
		{
            VertexNodeDescriptor n;
            n.attribute = VA;
			n.offset = offset;
			if (offset == 0) fmt.nodes.clear(); // Allows format to be rebuilt
            fmt.nodes.push_back(n);
			fmt.stride = offset + sizeof(Vertex);
		};

	private:

		template <VertexAttributeEnum, typename> friend class VertexNode;

		static size_t offset;
	};

	// Initialise offset member
	template <VertexAttributeEnum VA, typename NEXT>
	size_t VertexNode<VA, NEXT>::offset = 0;

	// Initialise offset member
	template <VertexAttributeEnum VA>
	size_t VertexNode<VA, void>::offset = 0;

	enum VertexUsageEnum
	{
		VU_POSITION_ONLY,
		VU_UNTEXTURED_PLAIN,
		VU_COUNT // Number of vertex usages
	};

	/// Vertex usage root template
	template <VertexUsageEnum> struct VertexUsage {};

	/// VU_SIMPLE usage
	template <> struct VertexUsage<VU_POSITION_ONLY>
	{
		typedef VertexNode<VA_POSITION> Usage; // 12 bytes
	};

	/// VU_UNTEXTURED_PLAIN usage
	template <> struct VertexUsage<VU_UNTEXTURED_PLAIN>
	{
		typedef 
			VertexNode<VA_POSITION,
			VertexNode<VA_NORMAL> > Usage; // 24 bytes
	};

	/// Format builder helper function
	VertexFormat buildVertexFormat(const VertexUsageEnum& vertexUsage);
}

#endif