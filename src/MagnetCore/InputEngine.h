#ifndef _MAGNET_CORE_INPUT_ENGINE_H 
#define _MAGNET_CORE_INPUT_ENGINE_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	enum InputMode
	{
		IM_DIGITAL,
		IM_PLANE,
		IM_AXIS,
		IM_COUNT // Number of input modes
	};

	class InputEngine : public Subsystem
	{
	public:

		inline virtual ~InputEngine() {}

		virtual void startup() = 0;

		virtual void shutdown() = 0;

		virtual void update() = 0;
	};
}

#endif