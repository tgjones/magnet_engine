
#ifndef _MAGNET_CORE_DIRECT_ACTOR_INSTANCE_H
#define _MAGNET_CORE_DIRECT_ACTOR_INSTANCE_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class DirectActorEntity : public ActorEntity
	{
	public:

		DirectActorEntity();

		inline virtual ~DirectActorEntity() {}

		inline void setVisible(bool visible) { m_visible = visible; }

		inline bool isVisible() { return m_visible; }

		Matrix4x4 getWorldTransform();

		inline void setOffset(const Matrix4x4& offset) { m_offset = offset; }

		inline Matrix4x4& getOffset() { return m_offset; }

		inline void setPosition(const Matrix4x4& position) { m_position = position; }

		inline Matrix4x4& getPosition() { return m_position; }

		inline void setOrientation(const Matrix4x4& orientation) { m_orientation = orientation; }

		inline Matrix4x4& getOrientation() { return m_orientation; }

	private:

		bool m_visible;

		Matrix4x4 m_offset;

		Matrix4x4 m_position;

		Matrix4x4 m_orientation;
	};
}

#endif