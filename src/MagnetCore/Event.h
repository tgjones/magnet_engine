
#ifndef _MAGNET_CORE_EVENT_H
#define _MAGNET_CORE_EVENT_H

#include <MagnetUtil/common.h>

namespace Magnet 
{

	class Event
	{
	private:

		std::string m_directive;
		
		typedef std::tr1::unordered_map<std::string, Variant> Args;

		Args m_args;

	public:

		inline Event(const std::string& directive) : m_directive(directive) {}

		inline ~Event() {}

		inline std::string getDirective() { return m_directive; }

		void setArgument(const std::string& name, const Variant& value);

		Variant& getArgument(const std::string& name);

		Variant& operator [] (const std::string& name);
	};
}

#endif