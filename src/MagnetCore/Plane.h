
#ifndef _MAGNET_CORE_PLANE_H
#define _MAGNET_CORE_PLANE_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class Plane
	{
	public:
		
		// Default to horizontal 'ground'
		inline Plane() : m_normal(0.0f, 1.0f, 0.0f), m_distance(0.0f) {}

		inline void setNormal(const Vector3& normal) { m_normal = normal; }

		inline Vector3& getNormal() { return m_normal; }

		inline void setDistance(const float distance) { m_distance = distance; }

		inline float getDistance() { return m_distance; }

	private:

		Vector3 m_normal;

		float m_distance;
	};
}

#endif