#ifndef _MAGNET_CORE_PHYSICS_WORLD_H
#define _MAGNET_CORE_PHYSICS_WORLD_H

#ifndef _MAG_NO_PHYSICS

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class PhysicsEngine : public Subsystem
	{
	public:

		inline virtual ~PhysicsEngine() {}

		virtual void update(double lastFrameTime) = 0;


		virtual Handle<Plane> createPlane(Plane& plane) = 0;

		virtual void releasePlane(const Handle<Plane> hPlane) = 0;


		virtual HPhysicalActorInstance createActorInstance(PhysicalActorEntity& instance, Matrix4x4& initTransform) = 0;

		virtual void releaseActorInstance(const HPhysicalActorInstance hPhysicalActorInstance) = 0;

		virtual Matrix4x4 getActorInstanceTransform(const HPhysicalActorInstance hPhysicalActorInstance) = 0;


		virtual void setGravity(Vector3 gravity) = 0;

		virtual Vector3 getGravity() = 0;
	};
}

#endif
#endif