#ifndef _MAGNET_CORE_SPRITE_H
#define _MAGNET_CORE_SPRITE_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class SpriteAsset : public MediaAsset
	{
	public:

		inline void setDiffuseMap(HMediaFile hDiffuseMap) { m_hDiffuseMap = hDiffuseMap; }

		//inline HMediaFile getDiffuseMap() { ASSERT(!m_hDiffuseMap.isNull()); return m_hDiffuseMap; }

		TextureFile* getDiffuseMap();

		inline void setDefaultFrame(const Rectangle& defaultFrame) { m_defaultFrame = defaultFrame; }

		inline Rectangle& getDefaultFrame() { return m_defaultFrame; }

		void addAnimation(const std::string& id, const SpriteAnimation& animation);

		inline bool hasAnimations() { return (!m_animations.empty()); }

		SpriteAnimation& getAnimation(const std::string& id);

	private:

		HMediaFile m_hDiffuseMap;

		Rectangle m_defaultFrame;
		
		typedef std::tr1::unordered_map<std::string, SpriteAnimation> Animations;
		Animations m_animations;
	};
}

#endif