#ifndef _MAGNET_CORE_APPLICATION_H
#define _MAGNET_CORE_APPLICATION_H

#include <MagnetUtil/common.h>

namespace Magnet 
{

	class Application : public Subsystem
	{
	public:

		virtual inline ~Application() {}

		virtual inline void startup() {}

		virtual inline void shutdown() {}

		virtual inline void onPreStartup() {}

		virtual inline void onPostStartup() {}

		virtual inline void onPreShutdown() {}

		virtual inline void onPostShutdown() {}

		virtual inline void onPreEngineStartup() {}

		virtual inline void onPostEngineStartup() {}

		virtual inline void onPreEngineShutdown() {}

		virtual inline void onPostEngineShutdown() {}
		
		virtual fs::path getResourceRoot() = 0;

		virtual double getAbsoluteTime() = 0;
	
		virtual uint16_t getPixelWidth() = 0;

		virtual uint16_t getPixelHeight() = 0;
	};
}

#endif