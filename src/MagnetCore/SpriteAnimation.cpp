#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::SpriteAnimation::addFrame(const Rectangle& rect, float ticks)
{
	ASSERT(ticks > 0.0f);
	m_frames.push_back(rect);
	m_timing.push_back(ticks);
}

Magnet::Rectangle* Magnet::SpriteAnimation::getFrame(double atTime)
{
	// Use time to return desired frame index
	unsigned idx = 0;
	double cumulativeTime = 0;
	for (std::vector<double>::iterator itr = m_timing.begin(); 
		itr != m_timing.end(); ++itr)
	{
		cumulativeTime += (*itr);
		if (cumulativeTime >= atTime)
			break;
		idx++;
	}

	// Return frame or nullptr to signify animation has finished
	if (m_frames.size() > idx)
		return &m_frames[idx];
	else
		return nullptr;
}