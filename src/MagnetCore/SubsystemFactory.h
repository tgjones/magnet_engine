#ifndef _MAGNET_CORE_SUBSYSTEM_FACTORY_H
#define _MAGNET_CORE_SUBSYSTEM_FACTORY_H

#include <boost/tr1/unordered_map.hpp>
#include <MagnetUtil/types.h>

namespace Magnet 
{
	class Engine;
	class Application;
	class Renderer;
	class InputEngine;
	class GameWorld;
	class EventManager;
	class MediaManager;
	class ConfigurationManager;
	#ifndef _MAG_NO_PHYSICS
	class PhysicsEngine;
	#endif

	enum SubsystemType
	{
		ST_APPLICATION,
		ST_RENDERER,
		ST_INPUT_ENGINE,
		ST_GAME_WORLD,
		ST_EVENT_MANAGER,
		ST_MEDIA_MANAGER,
		ST_CONFIGURATION_MANAGER,
		#ifndef _MAG_NO_PHYSICS
		ST_PHYSICS_ENGINE,
		#endif
	};
	
	namespace
	{		
		template <SubsystemType> struct SubsystemTraits {};
		template <> struct SubsystemTraits<ST_APPLICATION> { typedef Application BaseType; };
		template <>	struct SubsystemTraits<ST_RENDERER> { typedef Renderer BaseType;	};
		template <>	struct SubsystemTraits<ST_INPUT_ENGINE> { typedef InputEngine BaseType; };
		template <>	struct SubsystemTraits<ST_GAME_WORLD> { typedef GameWorld BaseType; };
		template <>	struct SubsystemTraits<ST_EVENT_MANAGER> { typedef EventManager BaseType; };		
		template <>	struct SubsystemTraits<ST_MEDIA_MANAGER> { typedef MediaManager BaseType; };
		template <>	struct SubsystemTraits<ST_CONFIGURATION_MANAGER> { typedef ConfigurationManager BaseType; };
		#ifndef _MAG_NO_PHYSICS
		template <>	struct SubsystemTraits<ST_PHYSICS_ENGINE> { typedef PhysicsEngine BaseType; };
		#endif
	}

	class SubsystemFactory
	{
	public:

		// Runtime check for registred subsystem type
		static bool isRegistered(const SubsystemType type);

		// Subsystem type registration, this is wrapped in global.h by the REGISTER_SUBSYSTEM macro
		template <SubsystemType ENUM_TYPE, typename DERIVED_TYPE>
		static void registerType()
		{
			typedef typename SubsystemTraits<ENUM_TYPE>::BaseType* (*func_t)(bool);
			
			// Re-registering a subsystem type so destroy old instance
			if (m_subsystemFuncs.find(ENUM_TYPE) != m_subsystemFuncs.end())
			{
				func_t func = reinterpret_cast<func_t>(m_subsystemFuncs[ENUM_TYPE]);
				func(true);
			}

			// Register new subsystem function, the extra c-style 'func_t' cast for the function pointer is required to 
			// get around a GCC bug (see: http://gcc.gnu.org/ml/gcc-bugs/2000-09/msg00537.html which is similar).
			m_subsystemFuncs[ENUM_TYPE] = reinterpret_cast<void_func_t>((func_t)&subsystemFunc<ENUM_TYPE, DERIVED_TYPE>);
		}
	
		// Subsystem getter wrapper, this is wrapped in global.h by the g_pSusbsytem* global variable look-a-like macros
		template <SubsystemType ENUM_TYPE>
		static typename SubsystemTraits<ENUM_TYPE>::BaseType* getSubsystem(bool dispose = false)
		{
			typedef typename SubsystemTraits<ENUM_TYPE>::BaseType* (*func_t)(bool);

			// Find and dereference subsystem function
			SubsystemFuncs::iterator itr = m_subsystemFuncs.find(ENUM_TYPE);
			ASSERT(itr != m_subsystemFuncs.end());
			func_t func = reinterpret_cast<func_t>(itr->second);

			// Return result of subsystem function
			return func(dispose);
		}
		
	private:

		// Map which holds function pointers to subsystem getters
		typedef std::tr1::unordered_map<uint16_t, void_func_t> SubsystemFuncs;
		static SubsystemFuncs m_subsystemFuncs;	

		// Prevent direct instantiation with private constructor
		inline SubsystemFactory() {}

		// Template subsystem accessor method, designed to be instantiated for a specific type of subsystem 
		// and then a pointer to the function instance stored in m_subsystemFuncs
		template <SubsystemType ENUM_TYPE, typename DERIVED_TYPE>
		static typename SubsystemTraits<ENUM_TYPE>::BaseType* subsystemFunc(bool release = false)
		{
			static DERIVED_TYPE* pInst = nullptr;
			
			// Construction
			if (pInst == nullptr)
				pInst = new DERIVED_TYPE();
			
			// Explicit destruction
			else if (release)
				SAFE_DELETE(pInst);
			
			return pInst;
		}
	};
}

#endif
