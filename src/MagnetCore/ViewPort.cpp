#include "external.h"
#include <MagnetUtil/common.h>

Magnet::ViewPort::ViewPort() : m_name("View Port"), m_visible(true), m_top(0.f), m_left(0.f), 
	m_height(1.f), m_width(1.f), m_zNear(1.f), m_zFar(5000.f), m_zoom(1.f)
{
	// Compute aspect ratio
	this->updateAspectRatio();

	// Get FOV from config file
	g_pConfigurationManager->setSection("renderer");
	m_fov = g_pConfigurationManager->getParameterAsFloat("fov", 70.f);
}

Magnet::CameraEntity* Magnet::ViewPort::getCamera()
{
	// Dereference camera handle and return pointer
	ASSERT(!m_hCamera.isNull());
	return static_cast<CameraEntity*>(g_pGameWorld->getEntity(m_hCamera));
}

void Magnet::ViewPort::setWidth(float width)
{
	// Update width
	m_width = width;

	// Re-compute aspect ratio
	this->updateAspectRatio();
}

void Magnet::ViewPort::setHeight(float height)
{
	// Update width
	m_height = height;

	// Re-compute aspect ratio
	this->updateAspectRatio();
}

Magnet::Matrix4x4 Magnet::ViewPort::getProjectionTransform(const ProjectionType type)
{
	ASSERT(m_visible);

	// Direct3D compatible projection matrix
	if (type == PT_PERSPECTIVE_DIRECT3D)
	{
		// Compute scales
		float yScale = 1/std::tan(((m_fov/m_zoom)/_180_OVER_PI)/2);
		float xScale = yScale/m_aspectRatio;

		// Construct matrix and return
		// Ref: MSDN D3DXMatrixPerspectiveFovLH
		return Matrix4x4(
			xScale, 0.f, 0.f, 0.f,
			0.f, yScale, 0.f, 0.f,
			0.f, 0.f, m_zFar/(m_zFar/m_zNear), 1.f,
			0.f, 0.f, -m_zNear*m_zFar/(m_zFar-m_zNear), 0.f);
	}

	// OpenGL compatible projection matrix
	else if (type == PT_PERSPECTIVE_OPENGL)
	{
		// Compute frustum boundaries
		float yMax = m_zNear * std::tan((m_fov/m_zoom)*_PI_OVER_360);
		float yMin = -yMax;
		float xMax = yMax * m_aspectRatio;
		float xMin = yMin * m_aspectRatio;

		// Construct matrix and return
		// Ref: p.807 The Red Book
		return Matrix4x4(
			2.f*m_zNear/xMax-xMin, 0.f, xMax+xMin/xMax-xMin, 0.f,
			0.f, 2.f*m_zNear/yMax-yMin, yMax+yMin/yMax-yMin, 0.f,
			0.f, 0.f, -(m_zFar+m_zNear)/m_zFar-m_zNear, -2.f*m_zFar*m_zNear/m_zFar-m_zNear,
			0.f, 0.f, -1.f, 0.f);
	}

	else throw RuntimeError("Unrecognised projection type");
}

void Magnet::ViewPort::updateAspectRatio()
{
    // Calculate aspect ration based on application dimensions and normalised viewport size
	m_aspectRatio = (m_width * boost::numeric_cast<float>(g_pApplication->getPixelWidth()) / 
		 (m_height * boost::numeric_cast<float>(g_pApplication->getPixelHeight())));
}