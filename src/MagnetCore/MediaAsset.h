
#ifndef _MAGNET_CORE_MEDIA_ASSET_H
#define _MAGNET_CORE_MEDIA_ASSET_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	enum MediaAssetPriority
	{
		MA_PRIORITY_LOW,
		MA_PRIORITY_MEDIUM,
		MA_PRIORITY_HIGH
	};

	class MediaAsset
	{
	public:

		inline MediaAsset() : m_lockCount(0), m_id(""), m_priority(MA_PRIORITY_MEDIUM) { m_lastAccess = time(nullptr); }

		inline MediaAsset(MediaAssetPriority priority) : m_priority(priority) { m_lastAccess = time(nullptr); }

		inline virtual ~MediaAsset() {}

		inline void setId(const std::string& id) { m_id = id; }

		inline const std::string& getId() { ASSERT(m_id != ""); return m_id; }

		inline void setPriority(MediaAssetPriority priority) { m_priority = priority; }

		inline MediaAssetPriority getPriority() { return m_priority; }

		void lock();

		void unlock();

		inline bool isLocked() { return m_lockCount > 0; }

		inline void addFile(HMediaFile hFile) { m_files.insert(hFile); }

	protected:

		std::string m_id;

		std::set< HMediaFile > m_files;

	private:

		uint32_t m_lockCount;

		MediaAssetPriority m_priority;

		time_t m_lastAccess;
	};
}

#endif