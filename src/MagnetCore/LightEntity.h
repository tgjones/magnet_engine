
#ifndef _MAGNET_CORE_LIGHT_H
#define _MAGNET_CORE_LIGHT_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	struct LightEntity
	{
		bool on;

		inline void toggle() { on = !on; }	
		
	protected:
		inline LightEntity() : on(true) {}
	};

	struct AmbientLightEntity : public LightEntity, public Entity
	{		
		ColorValue color;
		inline AmbientLightEntity() : LightEntity(), Entity(GO_AMBIENT_LIGHT), color(1.0f, 1.0f, 1.0f) {}
	};

	struct DirectionalLightEntity : public LightEntity, public Entity
	{
		Vector3 direction;
		ColorValue diffuse;
		ColorValue specular;
		inline DirectionalLightEntity() : LightEntity(), Entity(GO_DIRECTIONAL_LIGHT), direction(0.0f, 0.0f, -1.0f), 
			diffuse(1.0f, 1.0f, 1.0f), specular(1.0f, 1.0f, 1.0f) {}
	};

	// TODO: pointlight and spotlight

}

#endif