
#ifndef _MAGNET_CORE_MODEL_H
#define _MAGNET_CORE_MODEL_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class ModelFile : public MediaFile
	{
	public:

		inline ModelFile() {}

		inline virtual ~ModelFile() { unload(); }

		const std::vector<AbstractMesh*>& getMeshes();

		const std::vector<RigidBody>& getRigidBodies();

		#if !defined(_MAG_NO_IMPORT)
		void setImportOptions(const ModelImportOptions& importOptions);
		#endif

		#if !defined(_MAG_NO_IMPORT)
		ModelImportOptions& getImportOptions();
		#endif

		virtual void load();

		virtual void unload();

		virtual bool isLoaded();

	protected:

		#if !defined(_MAG_NO_IMPORT)
		ModelImportOptions m_importOptions;
		#endif

		ModelFileSerializer m_file;
	};
}

#endif