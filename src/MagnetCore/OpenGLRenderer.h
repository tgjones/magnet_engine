#ifndef _MAGNET_CORE_OPEN_GL_RENDERER_H
#define _MAGNET_CORE_OPEN_GL_RENDERER_H
#ifdef _MAG_USE_GL

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class OpenGLRenderer : public Renderer
	{
	public:

		inline virtual ~OpenGLRenderer() {}

		virtual void startup();

		virtual void render();

		virtual void reshape();

		virtual inline void reset() {}

		inline virtual void createDiffuseMap(const std::string& id, const TextureFileSerializer& data) { /* TODO */ }

		inline virtual void releaseDiffuseMap(const std::string& id) { /* TODO */ }

		inline void setRenderNormals(bool renderNormals) { m_renderNormals = renderNormals; }

		inline bool getRenderNormals() { return m_renderNormals; }

	protected:

		inline OpenGLRenderer() : m_renderNormals(false) {}

	private:

		friend class SubsystemFactory;

		std::tr1::unordered_map<uint16_t, VertexFormat> m_vertexFormats;

		bool m_renderNormals;
	};
}

#endif
#endif