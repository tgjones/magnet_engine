#ifndef _MAGNET_CORE_PHYS_X_PHYSICS_WORLD_H
#define _MAGNET_CORE_PHYS_X_PHYSICS_WORLD_H

#if !defined(_MAG_NO_PHYSICS) && defined(_MAG_USE_PHYSX)

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class PhysXPhysicsEngine : public PhysicsEngine
	{
	public:

		void startup();

		void shutdown();

		void update(double lastFrameTime);


		Handle<Plane> createPlane(Plane& plane);

		void releasePlane(const Handle<Plane> hPlane);


		HPhysicalActorInstance createActorInstance(PhysicalActorEntity& instance, Matrix4x4& initTransform);

		void releaseActorInstance(const HPhysicalActorInstance hActorInstance);

		Matrix4x4 getActorInstanceTransform(const HPhysicalActorInstance hActorInstance);


		void setGravity(Vector3 gravity);

		Vector3 getGravity();

	protected:

		PhysXPhysicsEngine() : m_pNxPhysicsSDK(nullptr), m_pNxScene(nullptr) {}

	private:

		friend class SubsystemFactory;
		
		static const double TIME_STEP;

		NxPhysicsSDK* m_pNxPhysicsSDK;

		NxScene* m_pNxScene;

		HandleController<NxActor*, Handle<Plane>> m_planes;

		HandleController<NxActor*, Handle<PhysicalActorEntity>> m_actorInstances;
	};
}

#endif
#endif