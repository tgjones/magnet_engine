#include "external.h"

#ifdef _MAG_USE_GL

#include <MagnetUtil/common.h>

void Magnet::OpenGLRenderer::startup()
{
	// Run base-class code
	Renderer::startup();
	
	// Get some config
	g_pConfigurationManager->setSection("renderer");
	m_renderNormals = g_pConfigurationManager->getParameterAsBool("showNormals", false);

	// Initialise GLEW if neccessary
	#ifdef _MAG_USE_GLEW
	GLenum err = glewInit();
	if (err != GLEW_OK) throw RuntimeError("An error occured initialising GLEW");
	#endif

	// Set the clear color
	glClearColor(0.5f, 0.0f, 0.0f, 0.0f);
	
	// Switch winding to clockwise as used by Magnet
	glFrontFace(GL_CW);

	// Enable backface culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	// Enable depth sorting
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	
	// Set shade model
	glShadeModel(GL_SMOOTH);
	
	// Pre-compute vertex formats
	for (int i = 0; i < VU_COUNT; i++)
	{
		VertexFormat fmt = buildVertexFormat((VertexUsageEnum)i);
		m_vertexFormats.insert(std::pair<VertexUsageEnum, VertexFormat>((VertexUsageEnum)i, fmt));
	}
}

void Magnet::OpenGLRenderer::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLfloat white_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat ambient[] = { 0.1f, 0.1f, 0.1f, 0.1f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white_light);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	
	// Temporary lighting code
	GLfloat light_position[] = { 1.0f, 2.0f, -4.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	// For each viewport
	for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
	{
		// Dereference viewport
		Magnet::ViewPort* pViewPort = g_pRenderer->getAllViewPorts()[i];

		// Apply projection matrix
		glMatrixMode(GL_PROJECTION);
		Matrix4x4 projection = pViewPort->getProjectionTransform(PT_PERSPECTIVE_OPENGL);
		glLoadMatrixf((GLfloat*)&projection);

		// Setup view matrix and switch to LH coordinates
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glScalef(1.0f, 1.0f, -1.0f);
		
		// Apply the view matrix
		Matrix4x4 view = pViewPort->getCamera()->getViewTransform();
		glMultMatrixf((GLfloat*)&view); 

		// For each actor instance
		BOOST_FOREACH(Entity* pEntity, g_pGameWorld->getEntitiesOfType(GO_ACTOR))
		{
			// Down-cast instance
			ActorEntity* pActorInstance = static_cast<ActorEntity*>(pEntity);

			// Get actor and model
			ActorAsset* pActor = pActorInstance->getAsset();
			ModelFile* pModel = pActor->getModel();
			
			// For each mesh in the model
			BOOST_FOREACH(AbstractMesh* pMesh, pModel->getMeshes())
			{			
				// Get vertex usage
				VertexFormat usage = m_vertexFormats[pMesh->getVertexUsage()];
				
				// Set up vertex buffer
				size_t vertexBufferSize = pMesh->getNumVertices() * usage.stride;
				GLuint vertexBufferObject;
				glGenBuffers(1, &vertexBufferObject);
				glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
				glBufferData(GL_ARRAY_BUFFER, vertexBufferSize, (GLvoid*)pMesh->getVerticesPtr(), GL_STATIC_DRAW);
				
				// Offset variables used by normal rendering code below
				#if !defined(_MAG_NO_IMPORT) && !defined(_MAG_GL_ES_VER)
				int posOffset, nmlOffset;
				#endif
				
				// For each node in the mesh vertex usage
				BOOST_FOREACH(VertexNodeDescriptor node, usage.nodes)
				{
					switch (node.attribute)
					{
						case VA_POSITION:
							#if !defined(_MAG_NO_IMPORT) && !defined(_MAG_GL_ES_VER)
							posOffset = node.offset;
							#endif
							glVertexPointer(3, GL_FLOAT, usage.stride, (GLvoid*)node.offset);
							glEnableClientState(GL_VERTEX_ARRAY);
							break;
							
						case VA_NORMAL:
							#if !defined(_MAG_NO_IMPORT) && !defined(_MAG_GL_ES_VER)
							nmlOffset = node.offset;
							#endif
							glNormalPointer(GL_FLOAT, usage.stride, (GLvoid*)node.offset);
							glEnableClientState(GL_NORMAL_ARRAY);
							break;
							
						case VA_DIFFUSE:
						case VA_SPECULAR:
							// TODO: Color stored in ARGB order, but I think OGL reads it in RGBA order
							//glColorPointer(4, GL_UNSIGNED_BYTE, usage.stride, reinterpret_cast<GLvoid*>(pVertices[usageItr->offset]));
							throw RuntimeError("Color vertex information is not yet supported in the OpenGL renderer");
							break;
							
						default:
							throw RuntimeError("Unsupported vertex component");
					}
				}
				
				// Setup index buffer
				size_t indexBufferSize = pMesh->getNumIndices() * sizeof(uint16_t);
				GLuint indexBufferObject;
				glGenBuffers(1, &indexBufferObject);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize, (GLvoid*)pMesh->getIndicesPtr(), GL_STATIC_DRAW); 

				// Push matrix stack
				glPushMatrix();

				// Concatenate world matrix
				Matrix4x4 world = pActorInstance->getWorldTransform();
				glMultMatrixf((GLfloat*)&world);

				// Draw primitives
				glDrawElements(GL_TRIANGLES, pMesh->getNumIndices(), GL_UNSIGNED_SHORT, 0);

				// Clear buffers
				glDeleteBuffers(1, &vertexBufferObject);
				glDeleteBuffers(1, &indexBufferObject);
				
				// Draw vertex normals
				#if defined(_MAG_DEBUG) && !defined(_MAG_GL_ES_VER)
				if (glIsEnabled(GL_VERTEX_ARRAY) && glIsEnabled(GL_NORMAL_ARRAY))
				{
					for (int i = 0; i < pMesh->getNumVertices(); i++)
					{
						// Get position and normal data
						Vector3* pPosition = (Vector3*)((byte_t*)pMesh->getVerticesPtr() + (i * usage.stride) + posOffset);
						Vector3* pNormal = (Vector3*)((byte_t*)pMesh->getVerticesPtr() + (i * usage.stride) + nmlOffset);
						
						// Draw line
						glDisable(GL_LIGHTING);
						glColor3f(0.0f, 1.0f, 0.0f);
						glBegin(GL_LINES);
						glVertex3fv((GLfloat*)pPosition);
						glVertex3fv((GLfloat*)&(*pPosition + (*pNormal * 0.1f)));
						glEnd();
						glEnable(GL_LIGHTING);
					}
				}
				#endif
								
				// Pop matrix stack
				glPopMatrix();
			}
		}
/*
		// Setup othogonal projection matrix
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0f, (GLdouble)g_pApplication->getWidth(), 0.0f, (GLdouble)g_pApplication->getHeight(), -1.0f, 1.0f);

		// Set modelview mode for drawing sprites
		glMatrixMode(GL_MODELVIEW);

		// Start drawing quads
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);

		// Get world objects that have sprite instances associated with them
		std::list<Entity*> spriteObjects = g_pGameWorld->getObjects(GOC_SPRITE_INSTANCE);
	
		// For each world object
		for (auto objectItr = spriteObjects.begin(); objectItr != spriteObjects.end(); ++objectItr)
		{
			// Get sprite instances
			std::vector<EntityComponent*> spriteInstances = (*objectItr)->getComponents(GOC_SPRITE_INSTANCE);

			// For each sprite instance
			for (auto spriteInstanceItr = spriteInstances.begin(); spriteInstanceItr != spriteInstances.end(); ++spriteInstanceItr)
			{
				SpriteEntity* spriteInstance = static_cast<SpriteEntity*>(*spriteInstanceItr);

				if (spriteInstance->isVisible())
				{
					OpenGLTexture* pTexture = static_cast<OpenGLTexture*>(spriteInstance->getSprite()->getTexture()); // Cast not checked!
					Rectangle* pRect = spriteInstance->getCurrentFrame();
					Vector2& position = spriteInstance->getPosition();
					Vector2 size((GLfloat)pRect->right - (GLfloat)pRect->left, (GLfloat)pRect->bottom - (GLfloat)pRect->top);

					// Bind the srite's texture to which subsequent calls refer to
					glBindTexture(GL_TEXTURE_2D, pTexture->getNativeTexture());

					// Bottom left
					glTexCoord2f((GLfloat)pRect->left / (GLfloat)pTexture->getWidth(), 1.0f - (GLfloat)pRect->bottom / (GLfloat)pTexture->getHeight());
					glVertex3f(position.x, g_pApplication->getHeight() - position.y, 0.0f);

					// Bottom right
					glTexCoord2f((GLfloat)pRect->right / (GLfloat)pTexture->getWidth(), 1.0f - (GLfloat)pRect->bottom / (GLfloat)pTexture->getHeight());
					glVertex3f(position.x + size.x, g_pApplication->getHeight() - position.y, 0.0f);

					// Top right
					glTexCoord2f((GLfloat)pRect->right / (GLfloat)pTexture->getWidth(), 1.0f - (GLfloat)pRect->top / (GLfloat)pTexture->getHeight());
					glVertex3f(position.x + size.x, g_pApplication->getHeight() - position.y - size.y, 0.0f);

					// Top left
					glTexCoord2f((GLfloat)pRect->left / (GLfloat)pTexture->getWidth(), 1.0f - (GLfloat)pRect->top / (GLfloat)pTexture->getHeight());
					glVertex3f(position.x, g_pApplication->getHeight() - position.y - size.y, 0.0f);
				}
			}
		}

		// End drawing quads
		glEnd();

		glDisable(GL_TEXTURE_2D);

		// Return matrices to pre-sprite-drawing states
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
*/

	}

	glFlush();
}

void Magnet::OpenGLRenderer::reshape()
{
	// Get app dimensions
	uint16_t width = g_pApplication->getPixelWidth();
	uint16_t height = g_pApplication->getPixelHeight();
	
	// Reset the current viewport
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	// Recompute aspect ratios for all viewports
	for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
		g_pRenderer->getAllViewPorts()[i]->updateAspectRatio();
}

#endif