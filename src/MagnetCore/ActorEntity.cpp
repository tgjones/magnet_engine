#include "external.h"
#include <MagnetUtil/common.h>

Magnet::ActorEntity::~ActorEntity()
{
	// Unlock old actor if there is one
	if (!m_hActor.isNull())
		g_pMediaManager->unlockAsset(m_hActor);
}

void Magnet::ActorEntity::setAsset(const std::string& id)
{
	// Unlock old actor if there is one
	if (!m_hActor.isNull())
		g_pMediaManager->unlockAsset(m_hActor);

	// Save handle
	m_hActor = g_pMediaManager->getAssetHandleById(id);

	// Lock asset
	g_pMediaManager->lockAsset(m_hActor);
}

void Magnet::ActorEntity::setAsset(const HMediaAsset hActor)
{
	// Unlock old actor if there is one
	if (!m_hActor.isNull())
		g_pMediaManager->unlockAsset(m_hActor);

	// Save handle
	m_hActor = hActor;

	// Lock asset
	g_pMediaManager->lockAsset(m_hActor);
}

Magnet::ActorAsset* Magnet::ActorEntity::getAsset()
{
	ASSERT(!m_hActor.isNull());
	return static_cast<ActorAsset*>(g_pMediaManager->getAsset(m_hActor));
}



