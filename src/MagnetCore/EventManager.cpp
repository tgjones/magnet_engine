#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::EventManager::registerReceiver(const std::string& directive, EventReceiver& receiver)
{
	// Insert label entry (or not, if it already exists)
	bool inserted = m_receivers.insert(std::make_pair(directive, std::set<EventReceiver*>()))
		.first->second.insert(&receiver).second;

	LOG_WARNING_IF(!inserted, "Receiver already registered with Event Manager");
}

void Magnet::EventManager::deregisterReceiver(const std::string& directive, EventReceiver& receiver)
{
	// Search for label entry
	Receivers::iterator itr = m_receivers.find(directive);

	if (itr != m_receivers.end())
	{
		// Erase receiver (or not if it is not found)
		bool erased = itr->second.erase(&receiver) > 0;

		LOG_WARNING_IF(!erased, "Receiver has not been registered, cannot deregister");

		// Remove label entry if all receivers have been removed
		if (itr->second.size() == 0)
			m_receivers.erase(directive);
	}
	else LOG_WARNING("Directive '" + directive + "' not found by Event Manager, cannot deregister");
}

void Magnet::EventManager::sendEvent(Event& event)
{
	Receivers::iterator directiveItr = m_receivers.find(event.getDirective());

	if (directiveItr != m_receivers.end())
	{
		BOOST_FOREACH(EventReceiver* pReceiver, directiveItr->second)
			pReceiver->onEvent(event);
		return;
	}
}

void Magnet::EventManager::enqueueEvent(Event& event, EventPriority priority)
{
	switch (priority)
	{
	case EVENT_PRIORITY_HIGH:
		m_highPriorityQueue.push(event);
		break;

	case EVENT_PRIORITY_MEDIUM:
		m_mediumPriorityQueue.push(event);
		break;

	case EVENT_PRIORITY_LOW:
		m_lowPriorityQueue.push(event);
		break;
	}
}

void Magnet::EventManager::scheduleEvent(Event& event, EventPriority priority, EventScheduleMode mode, uint32_t delay)
{
	switch (priority)
	{
	case EVENT_PRIORITY_HIGH:
//		m_highPrioritySchedule.push_back(boost::make_tuple(mode, delay, event));
		break;

	case EVENT_PRIORITY_MEDIUM:
//		m_mediumPrioritySchedule.push_back(boost::make_tuple(mode, delay, event));
		break;

	case EVENT_PRIORITY_LOW:
//		m_lowPrioritySchedule.push_back(boost::make_tuple(mode, delay, event));
		break;
	}
}

void Magnet::EventManager::dispatchScheduledEvents()
{
	// Dispatched high priority scheduled events
	for (PrioritySchedule::iterator itr = m_highPrioritySchedule.begin(); itr != m_highPrioritySchedule.end(); itr++)
	{
		this->dispatchScheduledEventsHelper(itr);
		m_highPrioritySchedule.erase(itr);
	}

	// Dispatched medium priority scheduled events
	for (PrioritySchedule::iterator itr = m_mediumPrioritySchedule.begin(); itr != m_mediumPrioritySchedule.end(); itr++)
	{
		this->dispatchScheduledEventsHelper(itr);
		m_mediumPrioritySchedule.erase(itr);
	}

	// Dispatched high priority scheduled events
	for (PrioritySchedule::iterator itr = m_lowPrioritySchedule.begin(); itr != m_lowPrioritySchedule.end(); itr++)
	{
		this->dispatchScheduledEventsHelper(itr);
		m_lowPrioritySchedule.erase(itr);
	}
}

void Magnet::EventManager::dispatchScheduledEventsHelper(PrioritySchedule::iterator& itr)
{
	/*

			int tmp = (uint32_t)g_pKernel->getLastFrameTime();
		float tmp2 =  (uint32_t)g_pKernel->getLastFrameTime();

	switch (itr->first)
	{
	case EVENT_SCHEDULE_FRAMES:

		std::map<uint32_t, Event>

		if (--itr->second.
		{
			Event* pEvent = &itr->get<2>();

			auto directiveItr = m_handlers.find(pEvent->getDirective());

			if (directiveItr != m_handlers.end())
			{
				for (auto receiverItr = directiveItr->second.begin(); receiverItr != directiveItr->second.end(); ++receiverItr)
					(*receiverItr)->onEvent(*pEvent);
				return;
			}
		}

		break;

	case EVENT_SCHEDULE_TICKS:

		itr->get<1>() -= (uint32_t)g_pKernel->getLastFrameTime();

		if (itr->get<1>() <= 0)
		{
			Event* pEvent = &itr->get<2>();

			auto directiveItr = m_handlers.find(pEvent->getDirective());

			if (directiveItr != m_handlers.end())
			{
				for (auto receiverItr = directiveItr->second.begin(); receiverItr != directiveItr->second.end(); ++receiverItr)
					(*receiverItr)->onEvent(*pEvent);
				return;
			}
		}

		break;

	default:

		LOG_ERROR("Unknowne event schedule mode");
	}
	*/
}

void Magnet::EventManager::dispatchQueuedEvents()
{
	// Dispatched high priority queued events
	for (size_t i = 0; i < m_highPriorityQueue.size(); i++)
	{
		Event* pEvent = &m_highPriorityQueue.front();

		Receivers::iterator directiveItr = m_receivers.find(pEvent->getDirective());

		if (directiveItr != m_receivers.end())
		{
			BOOST_FOREACH(EventReceiver* pReceiver, directiveItr->second)
				pReceiver->onEvent(*pEvent);
			return;
		}

		m_highPriorityQueue.pop();
	}

	// Dispatched medium priority queued events
	for (size_t i = 0; i < m_mediumPriorityQueue.size(); i++)
	{
		Event* pEvent = &m_mediumPriorityQueue.front();

		Receivers::iterator directiveItr = m_receivers.find(pEvent->getDirective());

		if (directiveItr != m_receivers.end())
		{			
			BOOST_FOREACH(EventReceiver* pReceiver, directiveItr->second)
				pReceiver->onEvent(*pEvent);
			return;
		}

		m_mediumPriorityQueue.pop();
	}

	// Dispatched high priority queued events
	for (size_t i = 0; i < m_lowPriorityQueue.size(); i++)
	{
		Event* pEvent = &m_lowPriorityQueue.front();

		Receivers::iterator directiveItr = m_receivers.find(pEvent->getDirective());

		if (directiveItr != m_receivers.end())
		{
			BOOST_FOREACH(EventReceiver* pReceiver, directiveItr->second)
				pReceiver->onEvent(*pEvent);
			return;
		}

		m_lowPriorityQueue.pop();
	}
}