#include "external.h"
#include <MagnetUtil/common.h>		

// Static initialisers for function pointer collection
std::tr1::unordered_map<uint16_t, void_func_t> Magnet::SubsystemFactory::m_subsystemFuncs;

bool Magnet::SubsystemFactory::isRegistered(const SubsystemType type)
{
	return (m_subsystemFuncs.find(type) != m_subsystemFuncs.end());
}