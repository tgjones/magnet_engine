#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::MediaManager::startup()
{
	g_pConfigurationManager->setSection("media");

	// Set media root directory (default to media subdir off getResourceRoot())
	m_mediaRoot = g_pConfigurationManager->getParameterAsString("mediaRoot", 
		fs::path(g_pApplication->getResourceRoot() / "media").string());

	// Parse custom resources manifest if there is one, otherwise try and use "media.xml"
	this->parseManifest(g_pConfigurationManager->getParameterAsString("manifest", "media.xml"));
}

void Magnet::MediaManager::shutdown()
{
	m_files.disposeAll();
	m_assets.disposeAll();
}

void Magnet::MediaManager::parseManifest(const fs::path& filePath)
{
	ASSERT(!m_mediaRoot.empty());

	// Open manifest file
    TiXmlDocument doc;
	if (filePath.is_relative())
		doc.LoadFile((g_pApplication->getResourceRoot() / filePath).string().c_str());
	else
		doc.LoadFile(filePath.string().c_str());    
    
    // Continue if the file was loaded correctly
	if (!doc.Error())
	{
		LOG_DEBUG("Parsing media manifest file '" + filePath.string() + "'");
		
		TiXmlElement *pRoot = doc.FirstChildElement("magnet:media");
		if (!pRoot)	throw LogicError("Cannot find root element 'magnet:media' in '" + filePath.string() + "'");

		// Get handle to root element
		TiXmlHandle rootHandle(pRoot);

		//
		// Parse media files first
		//

		// Textures
		for (TiXmlElement* pTextureElement = rootHandle.FirstChild("files").FirstChild("texture").ToElement();
			pTextureElement != nullptr; pTextureElement = pTextureElement->NextSiblingElement("texture"))
		{
			// Get item ID 
			std::string id = pTextureElement->Attribute("id");

			// Check ID is not already in use
			if (m_fileHandlesById.find(id) != m_fileHandlesById.end())
				throw LogicError("Attempted to use the file ID '" + id + "' more than once in '" + filePath.string() + "'");

			// Get file path
			TiXmlElement* pTexturePathElement = pTextureElement->FirstChildElement("path");
			if (pTexturePathElement == nullptr)
				throw LogicError("Cannot find texture 'path' element");
			fs::path path = pTexturePathElement->GetText();
			ASSERT(path.has_filename());
			path.make_preferred();

			// If path is relative then prepend m_mediaRoot
			if (path.is_relative()) 
				path = m_mediaRoot / path;

			// Item handle and open pointer
			HMediaFile handle;
			TextureFile* pTexture = m_files.acquire(handle, new TextureFile());

			// Set ID
			pTexture->setId(id);

			// Set filename
			pTexture->setFilePath(path);

			// Get texture usage
			TiXmlElement* pTextureUsageElement = pTextureElement->FirstChildElement("usage");
			if (pTextureUsageElement == nullptr)
				throw LogicError("Cannot find texture 'usage' element");

			// Set usage
			TextureUsage usage = TU_UNSPECIFIED;
			std::string usageStr = pTextureUsageElement->GetText();
			if (usageStr == "DIFFUSE_MAP")
				usage = TU_DIFFUSE_MAP;
			pTexture->setUsage(usage); 

			// Import options
			TiXmlElement *pImportOptionsElement = pTextureElement->FirstChildElement("importOptions");
			if (pImportOptionsElement && (toLower(path.extension().string()) != Magnet::TextureFileSerializer::FILE_EXT))
			{
				#if !defined(_MAG_NO_IMPORT)
				// Color key
				TiXmlElement* pColorKeyElement = pTextureElement->FirstChildElement("colorKey");
				if (pColorKeyElement != nullptr)
				{
					// Make sure usage allows color key
					if (usage == TU_DIFFUSE_MAP)
					{
						pTexture->getImportOptions().flags |= TIF_USE_COLOUR_KEY;
						pTexture->getImportOptions().colorKey = pColorKeyElement->GetText();
						pTexture->getImportOptions().colorKey.a = 0x00;
					}

					// Color key not supported by usage
					else throw LogicError("TextureFile usage does not support a color key");
				}
				#else
				throw LogicError("This build does not support non-native media file types");
				#endif
			}
			else if (pImportOptionsElement)
				throw LogicError("Import options are not supported for Magnet native file formats");

			// Insert ID/handle mapping
			m_fileHandlesById.insert(std::make_pair(id, handle));
		}

		// Models
		for (TiXmlElement* pModelElement = rootHandle.FirstChild("files").FirstChild("model").ToElement();
			pModelElement != nullptr; pModelElement = pModelElement->NextSiblingElement("model"))
		{
			// Get file ID
			std::string id = pModelElement->Attribute("id");

			// Check ID is not already in use
			if (m_fileHandlesById.find(id) != m_fileHandlesById.end())
				throw LogicError("Attempted to use the file ID '" + id + "' more than once");

			// Get file path
			TiXmlElement* pModelPathElement = pModelElement->FirstChildElement("path");
			if (pModelPathElement == nullptr)
				throw LogicError("Cannot find model 'path' element");
			fs::path path = pModelPathElement->GetText();
			ASSERT(path.has_filename());
			path.make_preferred();

			// If path is relative then prepend m_mediaRoot
			if (path.is_relative())
				path = m_mediaRoot / path;

			// Item handle and open pointer
			HMediaFile handle;
			ModelFile* pModel = m_files.acquire(handle, new ModelFile());

			// Set ID
			pModel->setId(id);

			// Set filename
			pModel->setFilePath(path);

			// Import options
			TiXmlElement *pImportOptionsElement = pModelElement->FirstChildElement("importOptions");
			if (pImportOptionsElement && (path.extension().string() != ModelFileSerializer::FILE_EXT))
			{
				#if !defined(_MAG_NO_IMPORT)
				// Generate normals
				TiXmlElement *pGenerateNormalsElement = pImportOptionsElement->FirstChildElement("generateNormals");
				if (pGenerateNormalsElement)
					if (std::string(pGenerateNormalsElement->GetText()) == "FACE_NORMALS")
						pModel->getImportOptions().flags |= MIF_FACE_NORMALS;
					else if (std::string(pGenerateNormalsElement->GetText()) == "SMOOTH_NORMALS")
						pModel->getImportOptions().flags |= MIF_SMOOTH_NORMALS;
					else
						throw LogicError("Unsupported generate normals enumeration '" + 
							std::string(pGenerateNormalsElement->GetText()) + "'");

				// Override normals
				TiXmlElement *pOverrideNormalsElement = pImportOptionsElement->FirstChildElement("overrideNormals");
				if (pOverrideNormalsElement)
				{
					std::string overrideNormals = toLower(std::string(pOverrideNormalsElement->GetText()));
					if ((overrideNormals == "true") || (overrideNormals == "yes") || (overrideNormals == "1"))
						pModel->getImportOptions().flags |= MIF_OVERRIDE_NORMALS;
					else if ((overrideNormals != "false") && (overrideNormals != "no") && (overrideNormals != "0"))
						throw LogicError("Problem reading override normals switch, please specify either 'true' or 'false'");
				}

				// Hard Edge Threshold
				TiXmlElement *pHardEdgeThreshElement = pImportOptionsElement->FirstChildElement("hardEdgeThresh");
				if (pHardEdgeThreshElement)
					try
					{
						pModel->getImportOptions().hardEdgeThresh = boost::lexical_cast<float, const char*>(pHardEdgeThreshElement->GetText());
					}
					catch (boost::bad_lexical_cast&)
					{
						throw LogicError("Problem reading hard edge threshold, please specify a valid floating point number");
					}
				
				// Generate bounding boxes
				TiXmlElement *pGenerateBoundingBoxes = pImportOptionsElement->FirstChildElement("generateBoundingBoxes");
				if (pGenerateBoundingBoxes)
				{
					std::string generateBoundingBoxes = toLower(std::string(pGenerateBoundingBoxes->GetText()));
					if ((generateBoundingBoxes == "true") || (generateBoundingBoxes == "yes") || (generateBoundingBoxes == "1"))
						pModel->getImportOptions().flags |= MIF_BOUNDING_BOXES;
					else if ((generateBoundingBoxes != "false") && (generateBoundingBoxes != "no") && (generateBoundingBoxes != "0"))
						throw LogicError("Problem reading generate bounding boxes switch, please specify either 'true' or 'false'");
				}
				#else
				throw LogicError("This build does not support non-native media file types");
				#endif
			}
			else if (pImportOptionsElement)
				throw Magnet::LogicError("Import options are not supported for Magnet native file formats");

			// Insert ID/handle mapping
			m_fileHandlesById.insert(std::make_pair(id, handle));
		}

		//
		// Parse assets second (they depend on items)
		//

		// Parse sprites
		for (TiXmlElement* pSpriteElement = rootHandle.FirstChild("assets").FirstChild("sprite").ToElement();
			pSpriteElement != nullptr; pSpriteElement = pSpriteElement->NextSiblingElement("sprite"))
		{
			// Get asset ID
			std::string id = pSpriteElement->Attribute("id");

			// Check ID is not already is use
			if (m_assetHandlesById.find(id) != m_assetHandlesById.end())
				throw LogicError("Attempted to use asset ID '" + id + "' more than once");
			
			// Get sprite handle and pointer
			HMediaAsset handle;
			SpriteAsset* pSprite = m_assets.acquire(handle, new SpriteAsset());

			// Set ID
			pSprite->setId(id);
			
			// Get diffuse map ID
			TiXmlElement* pDiffuseMapIdElement = pSpriteElement->FirstChildElement("diffuseMapId");
			if (!pDiffuseMapIdElement) throw LogicError("Cannot find sprite diffuse map ID");
			std::string diffuseMapId = pDiffuseMapIdElement->GetText();

			// Resolve diffuse map handle
			FileHandlesById::iterator itr = m_fileHandlesById.find(diffuseMapId);
			if (itr == m_fileHandlesById.end())
				throw LogicError("Diffuse map ID '" + diffuseMapId + "' does not reference a file");

			// Set diffuse map handle
			pSprite->setDiffuseMap(itr->second);

			// Get X offset
			TiXmlElement* pXOffsetElement = pSpriteElement->FirstChildElement("xOffset");
			if (!pXOffsetElement) throw LogicError("Cannot find sprite default frame X offset");

			// Get Y offset
			TiXmlElement* pYOffsetElement = pSpriteElement->FirstChildElement("yOffset");
			if (!pYOffsetElement) throw LogicError("Cannot find sprite default frame Y offset");

			// Get width
			TiXmlElement* pWidthElement = pSpriteElement->FirstChildElement("width");
			if (!pWidthElement) throw LogicError("Cannot find sprite default frame width");

			// Get height
			TiXmlElement* pHeightElement = pSpriteElement->FirstChildElement("height");
			if (!pHeightElement) throw LogicError("Cannot find sprite default frame height");

			// Set default frame information
			Rectangle rect;
			rect.top    = boost::lexical_cast<uint16_t, std::string>(pYOffsetElement->GetText()); 
			rect.left   = boost::lexical_cast<uint16_t, std::string>(pXOffsetElement->GetText());
			rect.bottom = rect.top  + boost::lexical_cast<uint16_t, std::string>(pHeightElement->GetText());
			rect.right  = rect.left + boost::lexical_cast<uint16_t, std::string>(pWidthElement->GetText());
			pSprite->setDefaultFrame(rect);

			// Parse sprite animations
			for (TiXmlElement* pAnimationElement = pSpriteElement->FirstChildElement("animation");
				pAnimationElement != nullptr; pAnimationElement = pAnimationElement->NextSiblingElement("animation"))
			{
				SpriteAnimation animation;
				
				// Add animation frames
				for (TiXmlElement* pFrameElement = pAnimationElement->FirstChildElement("frame");
					pFrameElement != nullptr; pFrameElement = pFrameElement->NextSiblingElement("frame"))
				{
					// Get X offset
					TiXmlElement* pXOffsetElement = pFrameElement->FirstChildElement("xOffset");
					if (!pXOffsetElement) throw LogicError("Cannot find sprite animation frame X offset");

					// Get Y offset
					TiXmlElement* pYOffsetElement = pFrameElement->FirstChildElement("yOffset");
					if (!pYOffsetElement) throw LogicError("Cannot find sprite animation frame Y offset");

					// Get width
					TiXmlElement* pWidthElement = pFrameElement->FirstChildElement("width");
					if (!pWidthElement)	throw LogicError("Cannot find sprite animation frame width");

					// Get height
					TiXmlElement* pHeightElement = pFrameElement->FirstChildElement("height");
					if (!pHeightElement) throw LogicError("Cannot find sprite animation frame height");

					// Get ticks
					TiXmlElement* pTicksElement = pFrameElement->FirstChildElement("ticks");
					if (!pTicksElement)	throw LogicError("Cannot find sprite animation frame timing (ticks)");

					// Set animation frame information
					Rectangle rect;
					rect.top    = boost::lexical_cast<uint16_t, std::string>(pYOffsetElement->GetText()); 
					rect.left   = boost::lexical_cast<uint16_t, std::string>(pXOffsetElement->GetText());
					rect.bottom = rect.top  + boost::lexical_cast<uint16_t, std::string>(pHeightElement->GetText());
					rect.right  = rect.left + boost::lexical_cast<uint16_t, std::string>(pWidthElement->GetText());

					// Add frame to animation
					animation.addFrame(rect, boost::lexical_cast<float, std::string>(pTicksElement->GetText()));
				}

				// Add animation to sprite
				pSprite->addAnimation(pAnimationElement->Attribute("id"), animation);
			}

			// Insert ID/handle mapping
			m_assetHandlesById.insert(std::make_pair(id, handle));
		
		}

		// Parse actors
		for (TiXmlElement* pActorElement = rootHandle.FirstChild("assets").FirstChild("actor").ToElement();
			pActorElement != nullptr; pActorElement = pActorElement->NextSiblingElement("actor"))
		{
			// Get asset ID
			std::string id = pActorElement->Attribute("id");

			// Check ID is not already is use
			if (m_assetHandlesById.find(id) != m_assetHandlesById.end())
				throw LogicError("Attempted to use asset ID '" + id + "' more than once");
			
			// Get actor handle and pointer
			HMediaAsset handle;
			ActorAsset* pActor = m_assets.acquire(handle, new ActorAsset());

			// Set ID
			pActor->setId(id);
			
			// Get model ID
			TiXmlElement* pModelIdElement = pActorElement->FirstChildElement("modelId");
			if (!pModelIdElement) throw LogicError("Cannot find actor model ID");
			std::string modelId = pModelIdElement->GetText();

			// Resolve model handle
			FileHandlesById::iterator itr = m_fileHandlesById.find(modelId);
			if (itr == m_fileHandlesById.end())
				throw LogicError("Model ID '" + modelId + "' does not reference a file");

			// Set model handle
			pActor->setModel(itr->second);

			// Initial offset data
			TiXmlElement* pInitOffsetElement = pActorElement->FirstChildElement("initOffset");
			if (pInitOffsetElement)
			{
				// Create the overall actor offset matrix
				Matrix4x4 offset = Matrix4x4::createIdentity();

				// Get initial scale if there is one
				TiXmlElement* pModelInitScaleElement = pInitOffsetElement->FirstChildElement("scaleAll");
				if (pModelInitScaleElement)
				{
					float initScale = boost::lexical_cast<float>(pModelInitScaleElement->GetText());
					offset *= Matrix4x4::createScale(initScale, initScale, initScale);
				}

				// Get initial rotation X if there is one
				TiXmlElement* pModelInitRotationXElement = pInitOffsetElement->FirstChildElement("rotationX");
				if (pModelInitRotationXElement)
				{
					float initRotationX = boost::lexical_cast<float>(pModelInitRotationXElement->GetText()) / _180_OVER_PI;
					offset *= Matrix4x4::createRotationX(initRotationX);
				}

				// Get initial rotation Y if there is one
				TiXmlElement* pModelInitRotationYElement = pInitOffsetElement->FirstChildElement("rotationY");
				if (pModelInitRotationYElement)
				{
					float initRotationY = boost::lexical_cast<float>(pModelInitRotationYElement->GetText()) / _180_OVER_PI;
					offset *= Matrix4x4::createRotationY(initRotationY);
				}
			
				// Get initial rotation Z if there is one
				TiXmlElement* pModelInitRotationZElement = pInitOffsetElement->FirstChildElement("rotationZ");
				if (pModelInitRotationZElement)
				{
					float initRotationZ = boost::lexical_cast<float>(pModelInitRotationZElement->GetText()) / _180_OVER_PI;
					offset *= Matrix4x4::createRotationZ(initRotationZ);
				}

				// Set the combined offset matrix to the actor
				pActor->setOffset(offset);
			}

			// Insert ID/handle mapping
			m_assetHandlesById.insert(std::make_pair(id, handle));
		}
	}
	
	// Could not load file
	else LOG_ERROR("Cannot parse media manifest file '" + filePath.string() + "': " + std::string(doc.ErrorDesc()));
}

Magnet::HMediaAsset Magnet::MediaManager::getAssetHandleById(const std::string& id)
{
	ASSERT(m_assetHandlesById.find(id) != m_assetHandlesById.end());
	return m_assetHandlesById[id];
}

Magnet::MediaAsset* Magnet::MediaManager::getAsset(HMediaAsset handle)
{
	return m_assets.dereference(handle);
}

Magnet::MediaAsset* Magnet::MediaManager::getAsset(const std::string& id)
{
	ASSERT(m_assetHandlesById.find(id) != m_assetHandlesById.end());
	return this->getAsset(m_assetHandlesById[id]);
}

Magnet::MediaAsset* Magnet::MediaManager::lockAsset(HMediaAsset handle)
{
	Magnet::MediaAsset* pAsset = this->getAsset(handle);
	pAsset->lock();
	return pAsset;
}

Magnet::MediaAsset* Magnet::MediaManager::lockAsset(const std::string& id)
{
	ASSERT(m_assetHandlesById.find(id) != m_assetHandlesById.end());
	return this->lockAsset(m_assetHandlesById[id]);
}

void Magnet::MediaManager::unlockAsset(HMediaAsset handle)
{
	Magnet::MediaAsset* pAsset = m_assets.dereference(handle);
	pAsset->unlock();
}

void Magnet::MediaManager::unlockAsset(const std::string& id)
{
	ASSERT(m_assetHandlesById.find(id) != m_assetHandlesById.end());
	this->unlockAsset(m_assetHandlesById[id]);
}

Magnet::HMediaFile Magnet::MediaManager::getItemHandleById(const std::string& id)
{
	ASSERT(m_fileHandlesById.find(id) != m_fileHandlesById.end());
	return m_fileHandlesById[id];
}

Magnet::MediaFile* Magnet::MediaManager::getFile(HMediaFile handle)
{
	return m_files.dereference(handle);
}

Magnet::MediaFile* Magnet::MediaManager::getFile(const std::string& id)
{
	ASSERT(m_fileHandlesById.find(id) != m_fileHandlesById.end());
	return this->getFile(m_fileHandlesById[id]);
}

Magnet::MediaFile* Magnet::MediaManager::lockFile(HMediaFile handle)
{
	Magnet::MediaFile* pItem = this->getFile(handle);
	pItem->lock();
	return pItem;
}

Magnet::MediaFile* Magnet::MediaManager::lockFile(const std::string& id)
{
	ASSERT(m_fileHandlesById.find(id) != m_fileHandlesById.end());
	return this->lockFile(m_fileHandlesById[id]);
}

void Magnet::MediaManager::unlockFile(HMediaFile handle)
{
	Magnet::MediaFile* pItem = m_files.dereference(handle);
	pItem->unlock();
}

void Magnet::MediaManager::unlockFile(const std::string& id)
{
	ASSERT(m_assetHandlesById.find(id) != m_assetHandlesById.end());
	this->unlockFile(m_fileHandlesById[id]);
}