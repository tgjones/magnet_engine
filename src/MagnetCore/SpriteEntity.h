
#ifndef _MAGNET_CORE_SPRITE_INSTANCE_H
#define _MAGNET_CORE_SPRITE_INSTANCE_H

#include <MagnetUtil/common.h>

namespace Magnet 
{

	class SpriteEntity : public Entity
	{
	public:

		SpriteEntity(HMediaAsset hSprite);

		~SpriteEntity();

		inline void setVisible(bool visible) { m_visible = visible; }

		inline bool isVisible() { return m_visible; }

		inline SpriteAsset* getSprite() { return m_pSprite; }

		inline Vector2& getPosition() { return m_position; }

		inline void setPosition(float x, float y) { m_position.x = x; m_position.y = y; }

		//inline Vector2& getSize() { return m_size; }

		void startAnimation(const std::string& name, bool loop = false);

		inline void stopAnimation() { m_pPlayingAnimation = nullptr; }

		Rectangle& getCurrentFrame();

	private:

		HMediaAsset m_hSprite;

		SpriteAsset* m_pSprite;

		bool m_visible;

		Vector2 m_position;

		//Vector2 m_centroid;

		//Vector2 m_size;

		bool m_animationLooping;

		double m_animationStartTime;

		SpriteAnimation* m_pPlayingAnimation;
	};
}

#endif