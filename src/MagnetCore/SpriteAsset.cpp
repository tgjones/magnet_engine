#include "external.h"
#include <MagnetUtil/common.h>

Magnet::TextureFile* Magnet::SpriteAsset::getDiffuseMap()
{
	ASSERT(!m_hDiffuseMap.isNull());
	TextureFile* pTexture = static_cast<TextureFile*>(g_pMediaManager->getFile(m_hDiffuseMap));
	if (!pTexture->isLoaded()) pTexture->load();
	return pTexture;
}

void Magnet::SpriteAsset::addAnimation(const std::string& id, const SpriteAnimation& animation)
{
	ASSERT(m_animations.find(id) == m_animations.end());
	m_animations.insert(std::make_pair(id, animation));
}

Magnet::SpriteAnimation& Magnet::SpriteAsset::getAnimation(const std::string& id)
{
	Animations::iterator itr = m_animations.find(id);
	ASSERT(itr != m_animations.end());
	return itr->second;
}

