#ifndef _MAGNET_CORE_CAMERA_H
#define _MAGNET_CORE_CAMERA_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class CameraEntity : public Entity
	{
	public:

		inline CameraEntity() : Entity(GO_CAMERA), m_roll(0.0f) {}

		Matrix4x4 getViewTransform();

		inline void setPosition(Vector3& position) { m_position = position; }
		inline void setPosition(float x, float y, float z) { m_position.x = x; m_position.y = y; m_position.z = z; }
		inline Vector3& getPosition() { return m_position; }

		inline void setTarget(const Vector3& target) { m_target = target; }
		inline void setTarget(float x, float y, float z) { m_target.x = x; m_target.y = y; m_target.z = z; }
		inline Vector3& getTarget() { return m_target; }

		inline void setRoll(float roll) { m_roll = roll; }
		inline float getRoll() { return m_roll; }

	private:

		Vector3 m_position;
		Vector3 m_target;
		float m_roll;
	};
}

#endif