#ifndef _MAGNET_CORE_HANDLES_H
#define _MAGNET_CORE_HANDLES_H

#include <MagnetUtil/Handle.h>

namespace Magnet
{
	// Forward declarations
	class Entity;
	class MediaFile;
	class MediaAsset;
	class ViewPort;
	class PhysicalActorEntity;

	// Handle types
	typedef Handle<Entity> HEntity;
	typedef Handle<MediaFile> HMediaFile;
	typedef Handle<MediaAsset> HMediaAsset;
	typedef Handle<ViewPort> HViewPort;
	typedef Handle<PhysicalActorEntity> HPhysicalActorInstance;
}

#endif