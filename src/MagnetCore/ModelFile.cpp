#include "external.h"
#include <MagnetUtil/common.h>

const std::vector<Magnet::AbstractMesh*>& Magnet::ModelFile::getMeshes() 
{ 
	ASSERT(!m_file.getMeshes().empty()); 
	return m_file.getMeshes(); 
}

const std::vector<Magnet::RigidBody>& Magnet::ModelFile::getRigidBodies()
{
	ASSERT(!m_file.getRigidBodies().empty());
	return m_file.getRigidBodies();
}

#if !defined(_MAG_NO_IMPORT)
void Magnet::ModelFile::setImportOptions(const ModelImportOptions& importOptions)
{
	ASSERT(m_filePath.extension().string() != ModelFileSerializer::FILE_EXT);
	m_importOptions = importOptions;
}
#endif

#if !defined(_MAG_NO_IMPORT)
Magnet::ModelImportOptions& Magnet::ModelFile::getImportOptions()
{
	ASSERT(m_filePath.extension().string() != ModelFileSerializer::FILE_EXT);
	return m_importOptions;
}
#endif

void Magnet::ModelFile::load() 
{ 
	ASSERT(!m_filePath.empty());

	// Read native model format
	if (m_filePath.extension().string() == ModelFileSerializer::FILE_EXT)
		m_file.read(m_filePath.string());

	#if !defined(_MAG_NO_IMPORT)
	else m_file.import(m_filePath, m_importOptions);
	#else
	else throw LogicError("This build does not support non-native media file types");
	#endif
}

void Magnet::ModelFile::unload() 
{ 
	BOOST_FOREACH(AbstractMesh* p, m_file.getMeshes()) SAFE_DELETE(p); 
	m_file.getMeshes().clear(); 
}

bool Magnet::ModelFile::isLoaded() 
{ 
	return !m_file.getMeshes().empty(); 
}