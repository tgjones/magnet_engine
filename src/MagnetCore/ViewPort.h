#ifndef _MAGNET_CORE_VIEW_PORT_H
#define _MAGNET_CORE_VIEW_PORT_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	enum ProjectionType
	{
		PT_PERSPECTIVE_DIRECT3D,
		PT_PERSPECTIVE_OPENGL
	};

	class ViewPort
	{
	public:

		ViewPort();

		inline void setName(const std::string& name) { m_name = name; }
		inline std::string& getName() { return m_name; }

		inline void setCamera(const HEntity hCamera) { m_hCamera = hCamera; }
		CameraEntity* getCamera();

		inline void setVisible(bool visible) { m_visible = visible; }
		inline bool isVisible() { return m_visible; }

		inline void setTop(float top) { m_top = top; }
		inline float getTop() { return m_top; }

		inline void setLeft(float left) { m_left = left; }
		inline float getLeft() { return m_left; }

		void setHeight(float height);
		inline float getHeight() { return m_height; }

		void setWidth(float width);
		inline float getWidth() { return m_width; }

		inline void setFieldOfView(float fovDeg) { m_fov = fovDeg / (180.0f / PI); }
		inline float getFieldOfView() { return m_fov; }

		inline void setZClipNear(float zNear) { m_zNear = zNear; }
		inline float getZClipNear() { return m_zNear; }

		inline void setZClipFar(float zFar) { m_zFar = zFar; }
		inline float getZClipFar() { return m_zFar; }

		inline float getAspectRatio() { return m_aspectRatio; }

		inline void setZoom(float zoom) { m_zoom = zoom; }
		inline float getZoom() { return m_zoom; }

		Matrix4x4 getProjectionTransform(const ProjectionType type);

		void updateAspectRatio();
	
	private:

		std::string m_name;

		HEntity m_hCamera;

		bool m_visible;

		float m_top, m_left, m_height, m_width;

		float m_fov, m_zNear, m_zFar, m_aspectRatio, m_zoom;
	};

}

#endif