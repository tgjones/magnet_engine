
#ifndef _MAGNET_CORE_MESH_H
#define _MAGNET_CORE_MESH_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	struct Material	
	{ 
		ColorValue diffuse; 
		ColorValue ambient; 
		ColorValue specular; 
		float shininess; 
		inline Material() : shininess(1.0f) {}
	};

	// Abtract mesh base-class
	class AbstractMesh
	{
	public:

		virtual ~AbstractMesh() {}

		virtual void* getVerticesPtr() = 0;

		virtual void* getIndicesPtr() = 0;

		virtual uint16_t getNumVertices() = 0;

		virtual uint16_t getNumIndices() = 0;

		virtual uint16_t getNumTriangles() = 0;

		virtual VertexUsageEnum getVertexUsage() = 0;

		virtual size_t getVertexSize() = 0;
		
		virtual void addTriangle(uint16_t idx1, uint16_t idx2, uint16_t idx3) = 0;

		virtual uint16_t getNumMaterials() = 0;

		virtual void addMaterial(const Material& material) = 0;

		virtual const Material& getMaterial(uint16_t idx) = 0;

		virtual const std::vector<Material>& getMaterials() = 0;
	};

	// Generic mesh sub-class
	template <VertexUsageEnum VERT_USAGE>
	class GenericMesh : public AbstractMesh
	{
	public:

		typedef typename VertexUsage<VERT_USAGE>::Usage VertexUsage;

		typedef Magnet::VertexBuffer<VertexUsage> VertexBuffer;

		typedef typename VertexBuffer::Vertex Vertex;

		GenericMesh() : m_numTriangles(0), m_vertexUsage(VERT_USAGE),
			m_vertexSize(sizeof(typename VertexUsage::Vertex)) {}

		virtual ~GenericMesh() {}

		void* getVerticesPtr() 
		{ 
			ASSERT(!m_vertices.data.empty());
			return static_cast<void*>(&m_vertices.data[0]); 
		}

		void* getIndicesPtr()
		{
			ASSERT(!m_indices.empty());
			return static_cast<void*>(&m_indices[0]);
		}
        
        std::vector<Vertex>& getVertices()
        {
            return m_vertices.data;
        }
        
        std::vector<uint16_t>& getIndices()
        {
            return m_indices;
        }

		uint16_t addVertex(const Vertex& vert)
		{
			m_vertices.data.push_back(vert);
			return m_vertices.data.size() - 1;
		}

		void addTriangle(uint16_t idx1, uint16_t idx2, uint16_t idx3)
		{
			ASSERT(idx1 < m_vertices.data.size());
			ASSERT(idx2 < m_vertices.data.size());
			ASSERT(idx3 < m_vertices.data.size());
			m_indices.push_back(idx1);
			m_indices.push_back(idx2);
			m_indices.push_back(idx3);
			m_numTriangles++;
		}

		uint16_t getNumVertices() { return m_vertices.data.size(); }

		uint16_t getNumIndices() { return m_indices.size(); }

		uint16_t getNumTriangles() { return m_numTriangles; }

		VertexUsageEnum getVertexUsage() { return m_vertexUsage; }

		size_t getVertexSize() { return m_vertexSize; }

		void addMaterial(const Material& material) { m_materials.push_back(material); }

		uint16_t getNumMaterials() { return m_materials.size(); }

		const Material& getMaterial(uint16_t idx) { return m_materials[idx]; }

		const std::vector<Material>& getMaterials() { return m_materials; }

	protected:

		VertexUsageEnum m_vertexUsage;

		VertexBuffer m_vertices;

		std::vector<uint16_t> m_indices;

		std::vector<Material> m_materials;

		size_t m_vertexSize;

		uint16_t m_numTriangles;

		Vector3 m_minPosition;

		Vector3 m_maxPosition;
	};
}

#endif