
#ifndef _MAGNET_CORE_GAME_OBJECT_H
#define _MAGNET_CORE_GAME_OBJECT_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	enum EntityType
	{
		GO_ROOT_NODE,
		GO_CAMERA,
		GO_SPRITE,
		GO_ACTOR,
		GO_AMBIENT_LIGHT,
		GO_DIRECTIONAL_LIGHT,
		GO_COUNT, // Number of Entitys
	};

	class Entity
	{
	public:

		inline Entity(EntityType type) : m_type(type) {}

		virtual inline ~Entity() {}

		inline EntityType getType() { return m_type; }

		void setParent(HEntity hParent);

		inline HEntity getWorldHandle() { return m_hSelf; }

		Entity* getParent();

	protected:

		virtual	inline void onUpdate(double lastFrameTime) {}

		virtual inline void onSpawn() {}

		virtual inline void onKill() {}

		std::set<HEntity> m_children;

	private:

		friend class GameWorld;

		void orphan();

		EntityType m_type;

		HEntity m_hSelf;

		HEntity m_hParent;
	};
}

#endif