
#ifndef _MAGNET_CORE_SPRITE_ANIMATION_H
#define _MAGNET_CORE_SPRITE_ANIMATION_H

#include <MagnetUtil/common.h>

namespace Magnet 
{

	class SpriteAnimation
	{
	public:

		inline SpriteAnimation() : m_totalTime(0.0f){}

		inline double getTotalTime() { return m_totalTime; }

		void addFrame(const Rectangle& rect, float ticks);

		Rectangle* getFrame(double atTime);

	private:

		double m_totalTime;

		std::vector<double> m_timing;

		std::vector<Rectangle> m_frames;
	};
}

#endif