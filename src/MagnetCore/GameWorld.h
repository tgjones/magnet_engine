#ifndef _MAGNET_CORE_GAME_WORLD_H
#define _MAGNET_CORE_GAME_WORLD_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class GameWorld : public Subsystem
	{
	public:

		static const uint32_t MAX_OBJECTS = HEntity::MAX_INDEX;

		void startup();

		void shutdown();

		void update(double lastFrameTime);

		Entity* getEntity(HEntity handle);

		std::vector<Entity*> getEntitiesOfType(EntityType type);

		inline Entity* operator [] (HEntity handle) { return this->getEntity(handle); }

		inline Entity* getSceneRootNode() { return this->getEntity(m_hSceneRootNode); }

		HEntity spawnEntity(Entity* pEntity);

		HEntity spawnEntity(Entity* pEntity, HEntity parent);

		void killEntity(HEntity hObject);

	protected:

		inline GameWorld() {}

	private:
	
		friend class SubsystemFactory;

		HandleController<Entity*, HEntity> m_objects;

		HEntity m_hSceneRootNode;
	};
}

#endif