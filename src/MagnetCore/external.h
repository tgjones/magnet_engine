#ifndef _MAGNET_CORE_PRECOMPILE_H
#define _MAGNET_CORE_PRECOMPILE_H

#include <MagnetUtil/config.h>

// MagnetUtil external headers
#include <MagnetUtil/external.h>

// C Standard Library
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cctype>

// C++ Standard Library
#include <string>
#include <sstream>
#include <fstream>
#include <memory>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <list>

// TR1 (via Boost headers)
#include <boost/tr1/array.hpp>
#include <boost/tr1/unordered_map.hpp>
#include <boost/tr1/unordered_set.hpp>

// Boost Filesystem
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

// Other Boost Headers
#include <boost/cstdint.hpp>
#include <boost/noncopyable.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

// OpenGL
#ifdef _MAG_USE_GL

	#if defined(_MAG_PLATFORM_WIN32)

	#include <windows.h>
	#include <GL/glew.h>
	#include <GL/wglew.h>
	#include <GL/gl.h>

    #elif defined(_MAG_PLATFORM_MAC_OSX)

    // TODO when porting MagnetCore to OS X

	#elif defined(_MAG_PLATFORM_IOS)

	#import <OpenGLES/ES1/gl.h>
	#import <OpenGLES/ES1/glext.h>
	#import <OpenGLES/ES2/gl.h>
	#import <OpenGLES/ES2/glext.h>

	#endif

#endif

#if defined(_MAG_USE_PHYSX) && !defined(_MAG_NO_PHYSICS)
// NVIDIA PhysX
#include <NxPhysics.h>
#endif

// TinyXML
#include <tinyxml.h>

#endif