#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::Renderer::startup()
{
	// Create fullscreen viewport
	m_hScreen = this->addViewPort(0.f, 0.f, 1.f, 1.f);
	
	// Setup default viewport camera
	CameraEntity* pCamera = new CameraEntity();
	pCamera->setPosition(0.f, 0.f, -10.f);
	pCamera->setTarget(0.f, 0.f, 0.f);
	pCamera->setRoll(0.f); 
	
	// Spawn camera to game world and set to screen
	this->getScreen()->setCamera(g_pGameWorld->spawnEntity(pCamera));

	// Get some config
	g_pConfigurationManager->setSection("Renderer");
	m_shaderPath = g_pConfigurationManager->getParameterAsString("shaderPath", (g_pApplication->getResourceRoot() / "shaders").string());
}

Magnet::HViewPort Magnet::Renderer::addViewPort(const float top, const float left, const float height, const float width)
{
	HViewPort hViewPort;
	ViewPort* pViewPort = m_viewPorts.acquire(hViewPort);
	pViewPort->setTop(top);
	pViewPort->setLeft(left);
	pViewPort->setHeight(height);
	pViewPort->setWidth(width);
	return hViewPort;
}