#ifndef _MAGNET_CORE_CONFIGURATION_MANAGER_H
#define _MAGNET_CORE_CONFIGURATION_MANAGER_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	class ConfigurationManager : public Subsystem
	{
	public:

		void parseConfigFile(const fs::path& filePath);

		bool setSection(const std::string& name);

		bool sectionExists(const std::string& name);

		bool parameterExists(const std::string& name);

		long getParameterAsLong(const std::string& name, long defaultValue);

		inline int getParameterAsInt(const std::string& name, int defaultValue) { return (int)getParameterAsLong(name, (long)defaultValue); }

		bool getParameterAsBool(const std::string& name, bool defaultValue);

		double getParameterAsDouble(const std::string& name, double defaultValue);
	
		inline float getParameterAsFloat(const std::string& name, float defaultValue) { return (float)getParameterAsDouble(name, (double)defaultValue); }

		const std::string& getParameterAsString(const std::string& name, const std::string& defaultValue);

		void setParameter(const std::string& name, const std::string& value);

	protected:

		inline ConfigurationManager() : m_currentSection("") {}

	private:

		friend class SubsystemFactory;

		std::string m_currentSection;

		// Secton, parameter, value 3D map
		std::tr1::unordered_map< std::string, std::tr1::unordered_map<std::string, std::string> > m_parameters;
	};
}

#endif