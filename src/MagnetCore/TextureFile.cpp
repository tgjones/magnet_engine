#include "external.h"
#include <MagnetUtil/common.h>

#if !defined(_MAG_NO_IMPORT)
void Magnet::TextureFile::setImportOptions(const TextureImportOptions& importOptions)
{
	ASSERT(m_filePath.extension().string() != TextureFileSerializer::FILE_EXT);
	m_importOptions = importOptions;
}
#endif

#if !defined(_MAG_NO_IMPORT)
Magnet::TextureImportOptions& Magnet::TextureFile::getImportOptions()
{
	ASSERT(m_filePath.extension().string() != TextureFileSerializer::FILE_EXT);
	return m_importOptions;
}
#endif

void Magnet::TextureFile::load() 
{ 
	ASSERT(!m_filePath.empty());

	// Temporary data file (data is sent to renderer before method returns)
	TextureFileSerializer file;

	// Read native texture file (MTF)
	if (m_filePath.extension().string() == TextureFileSerializer::FILE_EXT)
	{
		file.read(m_filePath.string());

		// Save new stuff to member variables
		m_usage = file.getUsage();
		m_width = file.getWidth();
		m_height = file.getHeight();
	}

	#if !defined(_MAG_NO_IMPORT)
	else
	{
		ASSERT(m_usage != TU_UNSPECIFIED);

		// Do import
		file.import(m_filePath, m_usage, m_importOptions);

		// Save new stuff to member variables
		m_width = file.getWidth();
		m_height = file.getHeight();
	}
	#else
	else throw LogicError("This build does not support non-native media file types");
	#endif
		
	// Send data to renderer (which copies it for its own management)
	if (m_usage == TU_DIFFUSE_MAP)
		g_pRenderer->createDiffuseMap(m_id, file);
	else
		throw RuntimeError("Unsupported texture usage");

	// Notify loaded
	m_isLoaded = true;
}

void Magnet::TextureFile::unload() 
{ 
	ASSERT(m_usage != TU_UNSPECIFIED);

	// Tell renderer to release texture data
	if (m_usage == TU_DIFFUSE_MAP)
		g_pRenderer->releaseDiffuseMap(m_id);

	// Notify unloaded
	m_isLoaded = false;
}
