
#ifndef _MAGNET_CORE_TEXTURE_H
#define _MAGNET_CORE_TEXTURE_H

#include <MagnetUtil/common.h>

namespace Magnet 
{
	class TextureFile : public MediaFile
	{
	public:

		inline TextureFile() : m_isLoaded(false), m_width(0), m_height(0), m_usage(TU_UNSPECIFIED) {}

		void load();

		void unload();

		inline bool isLoaded() { return m_isLoaded; }

		inline void setUsage(const TextureUsage usage) { m_usage = usage; }

		inline TextureUsage getUsage() { ASSERT(m_usage != TU_UNSPECIFIED); return m_usage; }

		#if !defined(_MAG_NO_IMPORT)
		void setImportOptions(const TextureImportOptions& importOptions);
		#endif

		#if !defined(_MAG_NO_IMPORT)
		TextureImportOptions& getImportOptions();
		#endif

		inline uint16_t getWidth() { ASSERT(m_width != 0); return m_width; }

		inline uint16_t getHeight() { ASSERT(m_height != 0); return m_height; }

	protected:

		bool m_isLoaded;

		TextureUsage m_usage;
		
		#if !defined(_MAG_NO_IMPORT)
		TextureImportOptions m_importOptions;
		#endif

		uint32_t m_width;

		uint32_t m_height;
	};
}

#endif