#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::GameWorld::startup()
{
	// Create scene root game object and save handle interanally
	m_objects.acquire(m_hSceneRootNode, new Entity(GO_ROOT_NODE))->m_hSelf = m_hSceneRootNode;
}

void Magnet::GameWorld::shutdown()
{
	m_objects.disposeAll();
}

void Magnet::GameWorld::update(double lastFrameTime)
{
	// Update all game world objects
	for (uint32_t i = 0; i < m_objects.getSize(); i++)
		m_objects[i]->onUpdate(lastFrameTime);
}

Magnet::Entity* Magnet::GameWorld::getEntity(HEntity handle)
{
	return m_objects.dereference(handle);
}

std::vector<Magnet::Entity*> Magnet::GameWorld::getEntitiesOfType(EntityType type)
{
	std::vector<Entity*> objects;

	for (uint32_t i = 0; i < m_objects.getSize(); i++)
		if (m_objects[i]->getType() == type)
			objects.push_back(m_objects[i]);

	return objects;
}

Magnet::HEntity Magnet::GameWorld::spawnEntity(Entity* pEntity)
{
	// Spawn the object with the scene root node as its parent
	return this->spawnEntity(pEntity, m_hSceneRootNode);
}

Magnet::HEntity Magnet::GameWorld::spawnEntity(Entity* pEntity, HEntity parent)
{
	// Acquire handle
	HEntity handle;
	m_objects.acquire(handle, pEntity);

	// Save handle interanally in object
	pEntity->m_hSelf = handle;

	// Set parent objects
	pEntity->setParent(parent);

	// Call game object custom spawn handler function
	pEntity->onSpawn();

	// Return the handle
	return handle;
}


void Magnet::GameWorld::killEntity(HEntity hObject)
{
	// Dereference game object
	Entity* pEntity = m_objects.dereference(hObject);

	// Call game object custom kill handler function
	pEntity->onKill();

	// Orphan game object
	pEntity->orphan();

	// Destroy the handle data
	m_objects.dispose(hObject);
}