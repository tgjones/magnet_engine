#include "external.h"
#include <MagnetUtil/common.h>

Magnet::Matrix4x4 Magnet::CameraEntity::getViewTransform()
{
	// Compute up vector based on camera roll
	Vector3 up(std::sin(m_roll), -std::cos(m_roll), 0.0f);

	// Compute forward vector
	Vector3 forward = m_target - m_position;
	forward.normalise();

	// Compute right facing vector
	Vector3 right = forward.cross(up);
	right.normalise();

	// Re-compute up vector
	up = forward.cross(right);
	up.normalise();

	// Construct view rotation matrix from vectors and return
	return Matrix4x4(
		right.x,                up.x,                forward.x,                0.0f,
		right.y,                up.y,                forward.y,                0.0f,
		right.z,                up.z,                forward.z,                0.0f,
		-right.dot(m_position), -up.dot(m_position), -forward.dot(m_position), 1.0f);
}