#include "external.h"

#include <MagnetUtil/common.h>

void Magnet::ConfigurationManager::parseConfigFile(const fs::path& filePath)
{	
	// Open config file
	std::ifstream config;
	if (filePath.is_relative())
		config.open((g_pApplication->getResourceRoot() / filePath).string().c_str());
	else
		config.open(filePath.string().c_str());

	if (config.is_open())
	{
		LOG_DEBUG("Parsing configuration file '" + filePath.string() + "'");

		// Variables used in parsing
		std::string line;
		std::string section;
		std::string parameter;
		std::string value;

		// Parse file
		while (!config.eof())
		{
			int j, k;

			// Get configuration line and trim
			std::getline(config, line);
			boost::trim(line);

			// Find comment character ; and truncate
			j = line.find_first_of(';');
			if (j != line.npos)
			{
				if (j == 0) continue; // Whole line is a comment
				line = line.substr(0, j);
			}

			// Find comment character # and truncate
			j = line.find_first_of('#');
			if (j != line.npos)
			{
				if (j == 0) continue; // Whole line is a comment
				line = line.substr(0, j);
			}
			
			// If empty line, simply skip it
			if (line.empty()) continue;

			// Ignore leading whitespace
			j = line.find_first_not_of(' ');

			// Process line
			if (line[j] != '[')
			{
				// Find end of parameter (the first space)
				k = line.substr(j, line.size() - 1).find_first_of(' ');

				// Get parameter name
				boost::trim(parameter = toLower(line.substr(j, k)));

				// Find an =
				k = line.find('=');

				// If no = was found log warning
				if (k == line.npos)
				{
					LOG_WARNING("No value found in configuration file for parameter '" + parameter + "'");
					value = "";
				}
				else
				{
					// Get parameter value
					boost::trim(value = line.substr(k + 1, line.size() - k));
				}

				// Replace or add parameter
				m_parameters[section][parameter] = value;

			}
			else
			{
				// Find ]
				int k = line.find_first_of(']');

				// Get section name
				boost::trim(section = toLower(line.substr(j + 1, k - 1)));
			}

		}

		// Finished parsing, close file
		config.close();
	}

	// Fail if we can't read the config file (use code 1000)
	else throw RuntimeError("Cannot open configuration file '" + filePath.string() + "'", 1000);
}

bool Magnet::ConfigurationManager::setSection(const std::string& name)
{
	if (m_parameters.find(toLower(name)) != m_parameters.end())
	{
		m_currentSection = toLower(name);
		return true;
	}
	else return false;
}

bool Magnet::ConfigurationManager::sectionExists(const std::string& name)
{
	return (m_parameters.find(toLower(name)) != m_parameters.end());
}

bool Magnet::ConfigurationManager::parameterExists(const std::string& name)
{
	if (m_currentSection == "")	
		return false;

	return (m_parameters[m_currentSection].find(toLower(name)) != m_parameters[m_currentSection].end());
}

long Magnet::ConfigurationManager::getParameterAsLong(const std::string& name, long defaultValue)
{
	if (m_currentSection == "")	return defaultValue;

	if (m_parameters[m_currentSection].find(toLower(name)) == m_parameters[m_currentSection].end())
		return defaultValue;

	long value;
	try
	{
		value = boost::lexical_cast<long, std::string>(m_parameters[m_currentSection][toLower(name)]);
	}
	catch (boost::bad_lexical_cast e)
	{
		LOG_DEBUG("Could not convert parameter '" + name + "' to long");
		value = defaultValue;
	}
	return value;
}

bool Magnet::ConfigurationManager::getParameterAsBool(const std::string& name, bool defaultValue)
{
	if (m_currentSection == "")	return defaultValue;
	
	if (m_parameters[m_currentSection].find(toLower(name)) == m_parameters[m_currentSection].end())
		return defaultValue;

	try
	{
		// Attempt to convert to integer
		int i = boost::lexical_cast<int, std::string>(toLower(m_parameters[m_currentSection][toLower(name)]));

		// If it was a success then convert to bool and return
		return (i > 0) ? true : false;
	}
	catch (boost::bad_lexical_cast e)
	{
		// Parameter is not an integer so try other common boolean strings
		std::string str = toLower(m_parameters[m_currentSection][toLower(name)]);
		
		// True strings
		if ((str == "true") || (str == "yes") || (str == "on"))	return true;

		// False strings
		else if ((str == "false") || (str == "no") || (str == "off")) return false;

		// Anything else
		else
		{
			LOG_DEBUG("Could not convert parameter '" + name + "' to bool");
			return defaultValue;
		}
	}
}

double Magnet::ConfigurationManager::getParameterAsDouble(const std::string& name, double defaultValue)
{
	if (m_currentSection == "")	return defaultValue;
	
	if (m_parameters[m_currentSection].find(toLower(name)) == m_parameters[m_currentSection].end())
		return defaultValue;

	double value;
	try
	{
		value = boost::lexical_cast<double, std::string>(m_parameters[m_currentSection][toLower(name)]);
	}
	catch (boost::bad_lexical_cast e)
	{
		LOG_DEBUG("Could not convert parameter '" + name + "' to double");
		value = defaultValue;
	}
	return value;
}

const std::string& Magnet::ConfigurationManager::getParameterAsString(const std::string& name, const std::string& defaultValue)
{
	if (m_currentSection == "")	return defaultValue;

	if(m_parameters[m_currentSection].find(toLower(name)) != m_parameters[m_currentSection].end())
		return m_parameters[m_currentSection][toLower(name)];
	else
		return defaultValue;
}

void Magnet::ConfigurationManager::setParameter(const std::string& name, const std::string& value)
{
	ASSERT(m_currentSection != "");

	m_parameters[m_currentSection][name] = value;
}