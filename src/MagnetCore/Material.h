
#ifndef _MAGNET_CORE_MATERIAL_H
#define _MAGNET_CORE_MATERIAL_H

#include <MagnetUtil/common.h>

namespace Magnet
{
	#pragma region Material Attributes

	enum MaterialComponentEnum
	{
		MC_DIFFUSE,
		MC_SPECULAR
	};

	template <MaterialComponentEnum> struct MaterialComponent {};

	template <> struct MaterialComponent<MA_DIFFUSE> 
	{	
		ColorValue diffuse;
	};

	template <> struct MaterialComponent<MA_SPECULAR>
	{
		ColorValue specular;
		float shininess;
	};

	#pragma endregion

	#pragma region Material Attribute Node (MaterialNode class)

	template <MaterialComponentEnum MC, typename NEXT = void>
	class MaterialNode
	{
	public:

		struct Material : public MaterialComponent<MC>, public NEXT::Material {};

	};

	template <MaterialComponentEnum MA>
	class MaterialNode<MC, void>
	{
	public:

		struct Material : public MaterialComponent<MC> {};

	};

	#pragma endregion

	#pragma region Standard Material Usages

	enum MaterialUsageEnum
	{
		MU_UNTEXTURED_PLAIN,
		MU_COUNT // Number of material usages
	};

	template <MaterialUsageEnum> struct MaterialUsage {};

	template <> struct MaterialUsage<MU_UNTEXTURE_PLAIN>
	{
		static const VertexUsageEnum vertexUsageEnum = VU_UNTEXTURED_PLAIN;
		typedef 
			VertexUsage<VU_UNTEXTURED_PLAIN>::Usage VertexUsage;
		typedef
			MaterialNode<MA_DIFFUSE,
			MaterialNode<MA_SPECULAR>> Usage;
	};

	#pragma endregion
}

#endif