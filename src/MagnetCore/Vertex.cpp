#include "external.h"
#include <MagnetUtil/common.h>

Magnet::VertexFormat Magnet::buildVertexFormat(const VertexUsageEnum& vertexUsage)
{
	VertexFormat fmt;

	switch (vertexUsage)
	{
	case VU_POSITION_ONLY:
		VertexUsage<VU_POSITION_ONLY>::Usage::buildFormat(fmt);
		break;

	case VU_UNTEXTURED_PLAIN:
		VertexUsage<VU_UNTEXTURED_PLAIN>::Usage::buildFormat(fmt);
		break;

	default:
		LOG_ERROR("Unsupported vertex format");
	}

	return fmt;
}