#ifndef _MAGNET_CORE_ENGINE_H
#define _MAGNET_CORE_ENGINE_H

#include <boost/filesystem/path.hpp>
namespace fs = boost::filesystem;

namespace Magnet
{
	class Engine
	{
	public:

		static void startup(const fs::path& configFile = "");
		static void shutdown();
	
	private:

		// Prevent direct instantiation of Engine with private constructor
		inline Engine() {}
	};
}

#endif