#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::ActorAsset::setModel(HMediaFile hModel)
{
	m_hModel = hModel;
	this->addFile(hModel);
}

Magnet::ModelFile* Magnet::ActorAsset::getModel()
{
	 ASSERT(!m_hModel.isNull()); 
	 return static_cast<ModelFile*>(g_pMediaManager->getFile(m_hModel));
}