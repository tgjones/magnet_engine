#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::MediaAsset::lock()
{
	// Lock and load (hehe) constituant files
	BOOST_FOREACH(HMediaFile hItem, m_files) g_pMediaManager->lockFile(hItem);

	// Lock this asset
	m_lockCount++;
}

void Magnet::MediaAsset::unlock()
{
	if (m_lockCount > 0)
	{
		// Unlock consituant files
		BOOST_FOREACH(HMediaFile hItem, m_files) g_pMediaManager->unlockFile(hItem);

		// Unlock this asset
		m_lockCount--;
	}
}