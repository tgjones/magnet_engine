#include "external.h"

#ifndef _MAG_NO_PHYSICS

#include <MagnetUtil/common.h>

Magnet::PhysicalActorEntity::PhysicalActorEntity() : ActorEntity(), m_density(1.0f)
{
	m_offset.identity();
	m_initPose.identity();
}

void Magnet::PhysicalActorEntity::onSpawn()
{
	// Tell Physics World about actor instance
	Matrix4x4 initTransform = m_offset * m_initPose;
	m_hPhysicsHandle = g_pPhysicsEngine->createActorInstance(*this, initTransform);
}

void Magnet::PhysicalActorEntity::onKill()
{
	// Release actor instance from Physics World
	g_pPhysicsEngine->releaseActorInstance(m_hPhysicsHandle);
}

Magnet::Matrix4x4 Magnet::PhysicalActorEntity::getWorldTransform()
{
	return this->getAsset()->getOffset() * m_offset * g_pPhysicsEngine->getActorInstanceTransform(m_hPhysicsHandle);
}

#endif