#include "external.h"
#include <MagnetUtil/common.h>

void Magnet::Event::setArgument(const std::string& name, const Variant& value)
{
	bool inserted = m_args.insert(std::pair<std::string, Variant>(name, value)).second;
	LOG_WARNING_IF(!inserted, "Did not add event argument '" + name + "' as it already existed");
}

Magnet::Variant& Magnet::Event::getArgument(const std::string& name)
{
	Args::iterator itr = m_args.find(name);
	LOG_WARNING_IF(itr == m_args.end(), "Could not find event argument '" + name + "'");
	return itr->second;
}

Magnet::Variant& Magnet::Event::operator [] (const std::string& name)
{
	return this->getArgument(name);
}