#include <MagnetUtil/common.h>
#include "CubeEntity.h"
#include "InputEventReceiver.h"

CubeEntity::CubeEntity(InputEventReceiver& inputEventReceiver) : m_inputEventReceiver(inputEventReceiver)
{
	// Set the DirectActorEntity asset ID from media.xml
	this->setAsset("cube");
}

void CubeEntity::onUpdate(double lastFrameTime)
{
	// Digital control
	if (m_inputEventReceiver.getDigitalRotateUp())
		this->getOrientation() *= Magnet::Matrix4x4::createRotationX((float)lastFrameTime);

	if (m_inputEventReceiver.getDigitalRotateDown())
		this->getOrientation() *= Magnet::Matrix4x4::createRotationX(-(float)lastFrameTime);	

	if (m_inputEventReceiver.getDigitalRotateLeft())
		this->getOrientation() *= Magnet::Matrix4x4::createRotationY((float)lastFrameTime);	
	
	if (m_inputEventReceiver.getDigitalRotateRight())
		this->getOrientation() *= Magnet::Matrix4x4::createRotationY(-(float)lastFrameTime);
		
	// Analogue control
	this->getOrientation() *= m_inputEventReceiver.getRotation();
	this->getOrientation() *= m_inputEventReceiver.getTranslation();
	this->getOrientation() *= m_inputEventReceiver.getScale();
}