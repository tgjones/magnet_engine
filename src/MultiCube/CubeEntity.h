#ifndef _MULTI_CUBE_CUBE_ENTITY_H
#define _MULTI_CUBE_CUBE_ENTITY_H

#include <MagnetUtil/common.h>
#include "InputEventReceiver.h"

class CubeEntity : public Magnet::DirectActorEntity
{
public:

	CubeEntity(InputEventReceiver& inputEventReceiver);

	// Called by g_pEventManager
	void onEvent(Magnet::Event& event);
	
protected:

	// Entity::onUpdate() is run every frame
	void onUpdate(double lastFrameTime);

private:

	InputEventReceiver& m_inputEventReceiver;
};


#endif
