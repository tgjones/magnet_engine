#ifndef _MULTI_CUBE_WIN32_H
#define _MULTI_CUBE_WIN32_H

#include <MagnetWin32/common.h>
#include "InputEventReceiver.h"

class MultiCubeWin32Application : public Magnet::Win32Application
{
public:

	virtual void onPostEngineStartup();

private:

	InputEventReceiver m_inputEventReceiver;

};

#endif