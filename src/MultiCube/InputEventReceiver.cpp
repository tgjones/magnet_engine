#include <MagnetUtil/common.h>
#include "InputEventReceiver.h"

using namespace Magnet;

InputEventReceiver::InputEventReceiver() : m_digitalRotateLeft(false), m_digitalRotateRight(false),
	m_digitalRotateUp(false), m_digitalRotateDown(false)
{
	m_rotation.identity();
	m_translation.identity();
	m_scale.identity();
}

void InputEventReceiver::onEvent(Event& event)
{
	// Handle input events
	switch ((int)event.getArgument("mode"))
	{

	// Buttons (or DPAD)
	case Magnet::IM_DIGITAL:

		if (event.getDirective() == "input:up")
			m_digitalRotateUp = (bool)event.getArgument("depressed");

		if (event.getDirective() == "input:down")
			m_digitalRotateDown = (bool)event.getArgument("depressed");

		if (event.getDirective() == "input:left")
			m_digitalRotateLeft = (bool)event.getArgument("depressed");

		if (event.getDirective() == "input:right")
			m_digitalRotateRight = (bool)event.getArgument("depressed");

		break;

	// Thumb sticks
	case Magnet::IM_PLANE:

		if (event.getDirective() == "input:spin")
		{
			float magnitude = (float)event.getArgument("magnitude");
			m_rotation = Matrix4x4::createRotationX((float)event.getArgument("directionY") * magnitude / 100);
			m_rotation *= Matrix4x4::createRotationY(-(float)event.getArgument("directionX") * magnitude / 100);
		}
				
		if (event.getDirective() == "input:move")
		{
			float magnitude = (float)event.getArgument("magnitude");
			m_translation = Matrix4x4::createTranslation((float)event.getArgument("directionX") * magnitude / 100,
				(float)event.getArgument("directionY") * magnitude / 100, 0.0f);
		}

		break;

	// Triggers
	case Magnet::IM_AXIS:

		if (event.getDirective() == "input:scaleDown")
		{
			float magnitude = (float)event.getArgument("magnitude");
			magnitude *= 0.5f;
			magnitude = 1.0f - magnitude;
			m_scale = Matrix4x4::createScale(magnitude, magnitude, magnitude);
		}

		if (event.getDirective() == "input:scaleUp")
		{
			float magnitude = (float)event.getArgument("magnitude");
			magnitude = 1.0f + magnitude;
			m_scale = Matrix4x4::createScale(magnitude, magnitude, magnitude);
		}

	// A different (unsupported) mode
	default: ;

	}
}