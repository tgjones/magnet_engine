#ifndef _MULTI_CUBE_INPUT_HANDLER_H
#define _MULTI_CUBE_INPUT_HANDLER_H

#include <MagnetUtil/common.h>

class InputEventReceiver : public Magnet::EventReceiver
{
public:

	InputEventReceiver();

	// Called by g_pEventManager to update input variables
	void onEvent(Magnet::Event& event);

	inline bool getDigitalRotateLeft() { return m_digitalRotateLeft; }
	inline bool getDigitalRotateRight() { return m_digitalRotateRight; }
	inline bool getDigitalRotateUp() { return m_digitalRotateUp; }
	inline bool getDigitalRotateDown() { return m_digitalRotateDown; }

	inline Magnet::Matrix4x4& getRotation() { return m_rotation; }
	inline Magnet::Matrix4x4& getTranslation() { return m_translation; }
	inline Magnet::Matrix4x4& getScale() { return m_scale; }

private:

	// These bools are for the digital input state;
	bool m_digitalRotateLeft;
	bool m_digitalRotateRight;
	bool m_digitalRotateUp;
	bool m_digitalRotateDown;

	// These matrices are for the analogue input state
	Magnet::Matrix4x4 m_rotation;
	Magnet::Matrix4x4 m_translation;
	Magnet::Matrix4x4 m_scale;
};

#endif