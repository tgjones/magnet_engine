#ifndef _MULTI_CUBE_IOS_H
#define _MULTI_CUBE_IOS_H

#include <MagnetiOS/common.h>
#include "InputEventReceiver.h"

class MultiCubeiOSApplication : public Magnet::iOSApplication
{
public:
    
	virtual void onPostEngineStartup();
    
private:
    
	InputEventReceiver m_inputEventReceiver;
    
};

#endif