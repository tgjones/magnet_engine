#include "MultiCubeiOSApplication.h"
#include "CubeEntity.h"

#define _MAG_APP_CLASS MultiCubeiOSApplication
#include <MagnetUtil/main.h>

using namespace Magnet;

void MultiCubeiOSApplication::onPostEngineStartup()
{
	// Setup ambient light to game world
	AmbientLightEntity* pAmbientLight = new AmbientLightEntity();
	pAmbientLight->color.r = 0.1f;
	pAmbientLight->color.g = 0.1f;
	pAmbientLight->color.b = 0.1f;
	g_pGameWorld->spawnEntity(pAmbientLight);
    
	// Add directional light to game world
	DirectionalLightEntity* pDirectionalLight = new DirectionalLightEntity();
	pDirectionalLight->direction.x = 0.0f;
	pDirectionalLight->direction.y = 0.0f;
	pDirectionalLight->direction.z = -1.0f;
	pDirectionalLight->diffuse.r = 0.5f;
	pDirectionalLight->diffuse.g = 0.5f;
	pDirectionalLight->diffuse.b = 0.5f;
	pDirectionalLight->specular.r = 0.2f;
	pDirectionalLight->specular.g = 0.2f;
	pDirectionalLight->specular.b = 0.8f;
	g_pGameWorld->spawnEntity(pDirectionalLight);
    
	// Register interest in input events
	g_pEventManager->registerReceiver("input:up", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:down", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:left", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:right", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:spin", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:move", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:scaleUp", m_inputEventReceiver);
	g_pEventManager->registerReceiver("input:scaleDown", m_inputEventReceiver);
	
	// Initialise cube instances and add to GO
	static const uint16_t NUM_CUBES = 8;
	CubeEntity* cubes[NUM_CUBES];
	for (uint16_t i = 0; i < NUM_CUBES; i++)
		cubes[i] = new CubeEntity(m_inputEventReceiver);
    
	// Position cubes
	Matrix4x4 translate = Matrix4x4::createTranslation(-1.2f, 1.2f, -1.2f);
	cubes[0]->setOffset(translate);
    
	translate.setTranslation(1.2f, 1.2f, -1.2f);
	cubes[1]->setOffset(translate);
    
	translate.setTranslation(-1.2f, -1.2f, -1.2f);
	cubes[2]->setOffset(translate);
    
	translate.setTranslation(1.2f, -1.2f, -1.2f);
	cubes[3]->setOffset(translate);
    
	translate.setTranslation(-1.2f, 1.2f, 1.2f);
	cubes[4]->setOffset(translate);
    
	translate.setTranslation(1.2f, 1.2f, 1.2f);
	cubes[5]->setOffset(translate);
    
	translate.setTranslation(-1.2f, -1.2f, 1.2f);
	cubes[6]->setOffset(translate);
    
	translate.setTranslation(1.2f, -1.2f, 1.2f);
	cubes[7]->setOffset(translate);
    
	// Add cubes to game world
	for (uint16_t i = 0; i < NUM_CUBES; i++)
		g_pGameWorld->spawnEntity(cubes[i]);
}