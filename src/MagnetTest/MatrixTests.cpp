#include "external.h"
#include <MagnetUtil/global.h>
#include <MagnetUtil/Matrix4x4.h>

// Prevent the need for a '<<' operator for logging
BOOST_TEST_DONT_PRINT_LOG_VALUE(Magnet::Matrix4x4);

BOOST_AUTO_TEST_SUITE(MatrixTests)

BOOST_AUTO_TEST_CASE(MatrixDefaultConstructor)
{
	Magnet::Matrix4x4 m;
	BOOST_CHECK_EQUAL(m._11, 0.0f);
	BOOST_CHECK_EQUAL(m._12, 0.0f);
	BOOST_CHECK_EQUAL(m._13, 0.0f);
	BOOST_CHECK_EQUAL(m._14, 0.0f);
	BOOST_CHECK_EQUAL(m._21, 0.0f);
	BOOST_CHECK_EQUAL(m._22, 0.0f);
	BOOST_CHECK_EQUAL(m._23, 0.0f);
	BOOST_CHECK_EQUAL(m._24, 0.0f);
	BOOST_CHECK_EQUAL(m._31, 0.0f);
	BOOST_CHECK_EQUAL(m._32, 0.0f);
	BOOST_CHECK_EQUAL(m._33, 0.0f);
	BOOST_CHECK_EQUAL(m._34, 0.0f);
	BOOST_CHECK_EQUAL(m._41, 0.0f);
	BOOST_CHECK_EQUAL(m._42, 0.0f);
	BOOST_CHECK_EQUAL(m._43, 0.0f);
	BOOST_CHECK_EQUAL(m._44, 0.0f);
}

BOOST_AUTO_TEST_CASE(MatrixCustomConstructor)
{
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	BOOST_CHECK_EQUAL(m._11, 1.1f);
	BOOST_CHECK_EQUAL(m._12, 1.2f);
	BOOST_CHECK_EQUAL(m._13, 1.3f);
	BOOST_CHECK_EQUAL(m._14, 1.4f);
	BOOST_CHECK_EQUAL(m._21, 2.1f);
	BOOST_CHECK_EQUAL(m._22, 2.2f);
	BOOST_CHECK_EQUAL(m._23, 2.3f);
	BOOST_CHECK_EQUAL(m._24, 2.4f);
	BOOST_CHECK_EQUAL(m._31, 3.1f);
	BOOST_CHECK_EQUAL(m._32, 3.2f);
	BOOST_CHECK_EQUAL(m._33, 3.3f);
	BOOST_CHECK_EQUAL(m._34, 3.4f);
	BOOST_CHECK_EQUAL(m._41, 4.1f);
	BOOST_CHECK_EQUAL(m._42, 4.2f);
	BOOST_CHECK_EQUAL(m._43, 4.3f);
	BOOST_CHECK_EQUAL(m._44, 4.4f);
}

BOOST_AUTO_TEST_CASE(MatrixArrayConstructor)
{
	float f[16] = {
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f};
	Magnet::Matrix4x4 m(f);
	BOOST_CHECK_EQUAL(m._11, 1.1f);
	BOOST_CHECK_EQUAL(m._12, 1.2f);
	BOOST_CHECK_EQUAL(m._13, 1.3f);
	BOOST_CHECK_EQUAL(m._14, 1.4f);
	BOOST_CHECK_EQUAL(m._21, 2.1f);
	BOOST_CHECK_EQUAL(m._22, 2.2f);
	BOOST_CHECK_EQUAL(m._23, 2.3f);
	BOOST_CHECK_EQUAL(m._24, 2.4f);
	BOOST_CHECK_EQUAL(m._31, 3.1f);
	BOOST_CHECK_EQUAL(m._32, 3.2f);
	BOOST_CHECK_EQUAL(m._33, 3.3f);
	BOOST_CHECK_EQUAL(m._34, 3.4f);
	BOOST_CHECK_EQUAL(m._41, 4.1f);
	BOOST_CHECK_EQUAL(m._42, 4.2f);
	BOOST_CHECK_EQUAL(m._43, 4.3f);
	BOOST_CHECK_EQUAL(m._44, 4.4f);
}

BOOST_AUTO_TEST_CASE(MatrixCopyConstructor)
{
	Magnet::Matrix4x4 m1(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	Magnet::Matrix4x4 m2(m1);
	BOOST_CHECK_EQUAL(m2._11, 1.1f);
	BOOST_CHECK_EQUAL(m2._12, 1.2f);
	BOOST_CHECK_EQUAL(m2._13, 1.3f);
	BOOST_CHECK_EQUAL(m2._14, 1.4f);
	BOOST_CHECK_EQUAL(m2._21, 2.1f);
	BOOST_CHECK_EQUAL(m2._22, 2.2f);
	BOOST_CHECK_EQUAL(m2._23, 2.3f);
	BOOST_CHECK_EQUAL(m2._24, 2.4f);
	BOOST_CHECK_EQUAL(m2._31, 3.1f);
	BOOST_CHECK_EQUAL(m2._32, 3.2f);
	BOOST_CHECK_EQUAL(m2._33, 3.3f);
	BOOST_CHECK_EQUAL(m2._34, 3.4f);
	BOOST_CHECK_EQUAL(m2._41, 4.1f);
	BOOST_CHECK_EQUAL(m2._42, 4.2f);
	BOOST_CHECK_EQUAL(m2._43, 4.3f);
	BOOST_CHECK_EQUAL(m2._44, 4.4f);
}

BOOST_AUTO_TEST_CASE(MatrixDataAccess)
{
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	BOOST_CHECK_EQUAL(*m.getDataPtr(), 1.1f);
	BOOST_CHECK_EQUAL(m.getDataPtr(), &m._11);
	BOOST_CHECK_EQUAL(m[0],  1.1f);
	BOOST_CHECK_EQUAL(m[1],  1.2f);
	BOOST_CHECK_EQUAL(m[2],  1.3f);
	BOOST_CHECK_EQUAL(m[3],  1.4f);
	BOOST_CHECK_EQUAL(m[4],  2.1f);
	BOOST_CHECK_EQUAL(m[5],  2.2f);
	BOOST_CHECK_EQUAL(m[6],  2.3f);
	BOOST_CHECK_EQUAL(m[7],  2.4f);
	BOOST_CHECK_EQUAL(m[8],  3.1f);
	BOOST_CHECK_EQUAL(m[9],  3.2f);
	BOOST_CHECK_EQUAL(m[10], 3.3f);
	BOOST_CHECK_EQUAL(m[11], 3.4f);
	BOOST_CHECK_EQUAL(m[12], 4.1f);
	BOOST_CHECK_EQUAL(m[13], 4.2f);
	BOOST_CHECK_EQUAL(m[14], 4.3f);
	BOOST_CHECK_EQUAL(m[15], 4.4f);
}

BOOST_AUTO_TEST_CASE(MatrixEquality)
{
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	BOOST_CHECK(m == Magnet::Matrix4x4(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f));
	BOOST_CHECK(!(m == Magnet::Matrix4x4(
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f)));
	BOOST_CHECK(m != Magnet::Matrix4x4(
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f));
	BOOST_CHECK(!(m != Magnet::Matrix4x4(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f)));
}

BOOST_AUTO_TEST_CASE(MatrixNegative)
{
	Magnet::Matrix4x4 ma(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	Magnet::Matrix4x4 mb = -ma;
	BOOST_CHECK_EQUAL(mb, Magnet::Matrix4x4(
		-1.1f, -1.2f, -1.3f, -1.4f,
		-2.1f, -2.2f, -2.3f, -2.4f,
		-3.1f, -3.2f, -3.3f, -3.4f,
		-4.1f, -4.2f, -4.3f, -4.4f));
}

BOOST_AUTO_TEST_CASE(MatrixIdentity)
{
	Magnet::Matrix4x4 ma;
	ma.identity();
	BOOST_CHECK_EQUAL(ma, Magnet::Matrix4x4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f));
	Magnet::Matrix4x4 mb = Magnet::Matrix4x4::createIdentity();
	BOOST_CHECK_EQUAL(mb, Magnet::Matrix4x4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f));
}

BOOST_AUTO_TEST_CASE(MatrixTranspose)
{
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	m.transpose();
	BOOST_CHECK_EQUAL(m, Magnet::Matrix4x4(
		1.1f, 2.1f, 3.1f, 4.1f,
		1.2f, 2.2f, 3.2f, 4.2f,
		1.3f, 2.3f, 3.3f, 4.3f,
		1.4f, 2.4f, 3.4f, 4.4f));
}

BOOST_AUTO_TEST_CASE(MatrixAddition)
{
}

BOOST_AUTO_TEST_CASE(MatrixSubtraction)
{
}

BOOST_AUTO_TEST_CASE(MatrixMultiplication)
{
	Magnet::Matrix4x4 ma(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);
	Magnet::Matrix4x4 mb(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);

	// *
	BOOST_CHECK_EQUAL((ma * mb), Magnet::Matrix4x4(
		13.5f, 14.0f, 14.5f, 15.0f,
		23.9f, 24.8f, 25.7f, 26.6f,
		34.3f, 35.6f, 36.9f, 38.2f,
		44.7f, 46.4f, 48.1f, 49.8f));

	// *=
	ma *= mb;
	BOOST_CHECK_EQUAL(ma, Magnet::Matrix4x4(
		13.5f, 14.0f, 14.5f, 15.0f,
		23.9f, 24.8f, 25.7f, 26.6f,
		34.3f, 35.6f, 36.9f, 38.2f,
		44.7f, 46.4f, 48.1f, 49.8f));
}

BOOST_AUTO_TEST_CASE(MatrixScalarMultiplication)
{
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);

	// *
	BOOST_CHECK_EQUAL((m * 5.0f), Magnet::Matrix4x4(
		 5.5f,  6.0f,  6.5f,  7.0f,
		10.5f, 11.0f, 11.5f, 12.0f,
		15.5f, 16.0f, 16.5f, 17.0f,
		20.5f, 21.0f, 21.5f, 22.0f));

	// *=
	m *= 5.0f;
	BOOST_CHECK_EQUAL(m, Magnet::Matrix4x4(
		 5.5f,  6.0f,  6.5f,  7.0f,
		10.5f, 11.0f, 11.5f, 12.0f,
		15.5f, 16.0f, 16.5f, 17.0f,
		20.5f, 21.0f, 21.5f, 22.0f));
}

BOOST_AUTO_TEST_CASE(MatrixRotation)
{
	// X
	Magnet::Matrix4x4 x = Magnet::Matrix4x4::createRotationX(PI/3);
	BOOST_CHECK_EQUAL(x, Magnet::Matrix4x4(
		1.0f,  0.0f,     0.0f,    0.0f,
		0.0f,  0.5f,     0.86602f, 0.0f,
		0.0f, -0.86602f, 0.5f,    0.0f,
		0.0f,  0.0f,     0.0f,    1.0f));

	// Y
	Magnet::Matrix4x4 y = Magnet::Matrix4x4::createRotationY(PI/3);
	BOOST_CHECK_EQUAL(y, Magnet::Matrix4x4(
		0.5f,     0.0f, -0.86602f, 0.0f,
		0.0f,     1.0f,  0.0f,     0.0f,
		0.86602f, 0.0f,  0.5f,     0.0f,
		0.0f,     0.0f,  0.0f,     1.0f));

	// Z
	Magnet::Matrix4x4 z = Magnet::Matrix4x4::createRotationZ(PI/3);
	BOOST_CHECK_EQUAL(z, Magnet::Matrix4x4(
		 0.5f,     0.86602f, 0.0f, 0.0f,
		-0.86602f, 0.5f,     0.0f, 0.0f,
		 0.0f,     0.0f,     1.0f, 0.0f,
		 0.0f,     0.0f,     0.0f, 1.0f));

	// Arbitrary
	// TODO: Test when implemented
}

BOOST_AUTO_TEST_CASE(MatrixScale)
{
	// Normal
	Magnet::Matrix4x4 scale = Magnet::Matrix4x4::createScale(1.0f, 2.0f, 3.0f);
	BOOST_CHECK_EQUAL(scale, Magnet::Matrix4x4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 2.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 3.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f));

	// Inverse
	Magnet::Matrix4x4 inverseScale = Magnet::Matrix4x4::createInverseScale(1.0f, 2.0f, 3.0f);
	BOOST_CHECK_EQUAL(inverseScale, Magnet::Matrix4x4(
		1.0f, 0.0f, 0.0f,     0.0f,
		0.0f, 0.5f, 0.0f,     0.0f,
		0.0f, 0.0f, 0.33333f, 0.0f,
		0.0f, 0.0f, 0.0f,     1.0f));
}

BOOST_AUTO_TEST_CASE(MatrixTranslation)
{
	// Normal
	Magnet::Matrix4x4 translation = Magnet::Matrix4x4::createTranslation(1.0f, 2.0f, 3.0f);
	BOOST_CHECK_EQUAL(translation, Magnet::Matrix4x4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		1.0f, 2.0f, 3.0f, 1.0f));

	// Inverse
	Magnet::Matrix4x4 inverseTranslation = Magnet::Matrix4x4::createInverseTranslation(1.0f, 2.0f, 3.0f);
	BOOST_CHECK_EQUAL(inverseTranslation, Magnet::Matrix4x4(
		 1.0f,  0.0f,  0.0f, 0.0f,
		 0.0f,  1.0f,  0.0f, 0.0f,
		 0.0f,  0.0f,  1.0f, 0.0f,
		-1.0f, -2.0f, -3.0f, 1.0f));
}

BOOST_AUTO_TEST_SUITE_END()