#include "external.h"
#include <MagnetUtil/global.h>
#include <MagnetUtil/Vector2.h>
#include <MagnetUtil/Vector3.h>
#include <MagnetUtil/Vector4.h>
#include <MagnetUtil/Matrix4x4.h>

// Prevent the need for a '<<' operator for logging
BOOST_TEST_DONT_PRINT_LOG_VALUE(Magnet::Vector2);
BOOST_TEST_DONT_PRINT_LOG_VALUE(Magnet::Vector3);
BOOST_TEST_DONT_PRINT_LOG_VALUE(Magnet::Vector4);

BOOST_AUTO_TEST_SUITE(VectorTests)

BOOST_AUTO_TEST_CASE(VectorDefaultConstructor)
{
	Magnet::Vector2 v2a;
	Magnet::Vector3 v3a;
	Magnet::Vector4 v4a;
	BOOST_CHECK_EQUAL(v2a.x, 0.0f);
	BOOST_CHECK_EQUAL(v2a.y, 0.0f);

	BOOST_CHECK_EQUAL(v3a.x, 0.0f);
	BOOST_CHECK_EQUAL(v3a.y, 0.0f);
	BOOST_CHECK_EQUAL(v3a.z, 0.0f);

	BOOST_CHECK_EQUAL(v4a.x, 0.0f);
	BOOST_CHECK_EQUAL(v4a.y, 0.0f);
	BOOST_CHECK_EQUAL(v4a.z, 0.0f);
	BOOST_CHECK_EQUAL(v4a.w, 0.0f);
}

BOOST_AUTO_TEST_CASE(VectorCustomConstructor)
{
	Magnet::Vector2 v2b(1.1f, 2.2f);
	Magnet::Vector3 v3b(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4b(1.1f, 2.2f, 3.3f, 4.4f);
	BOOST_CHECK_EQUAL(v2b.x, 1.1f);
	BOOST_CHECK_EQUAL(v2b.y, 2.2f);

	BOOST_CHECK_EQUAL(v3b.x, 1.1f);
	BOOST_CHECK_EQUAL(v3b.y, 2.2f);
	BOOST_CHECK_EQUAL(v3b.z, 3.3f);

	BOOST_CHECK_EQUAL(v4b.x, 1.1f);
	BOOST_CHECK_EQUAL(v4b.y, 2.2f);
	BOOST_CHECK_EQUAL(v4b.z, 3.3f);
	BOOST_CHECK_EQUAL(v4b.w, 4.4f);
}

BOOST_AUTO_TEST_CASE(VectorArrayConstructor)
{
	float f2[2] = {1.1f, 2.2f};
	float f3[3] = {1.1f, 2.2f, 3.3f};
	float f4[4] = {1.1f, 2.2f, 3.3f, 4.4f};
	Magnet::Vector2 v2c(f2);
	Magnet::Vector3 v3c(f3);
	Magnet::Vector4 v4c(f4);
	BOOST_CHECK_EQUAL(v2c.x, 1.1f);
	BOOST_CHECK_EQUAL(v2c.y, 2.2f);

	BOOST_CHECK_EQUAL(v3c.x, 1.1f);
	BOOST_CHECK_EQUAL(v3c.y, 2.2f);
	BOOST_CHECK_EQUAL(v3c.z, 3.3f);

	BOOST_CHECK_EQUAL(v4c.x, 1.1f);
	BOOST_CHECK_EQUAL(v4c.y, 2.2f);
	BOOST_CHECK_EQUAL(v4c.z, 3.3f);
	BOOST_CHECK_EQUAL(v4c.w, 4.4f);
}

BOOST_AUTO_TEST_CASE(VectorCopyConstructor)
{
	Magnet::Vector2 v2a(1.1f, 2.2f);
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f);	
	Magnet::Vector2 v2b(v2a);
	Magnet::Vector3 v3b(v3a);
	Magnet::Vector4 v4b(v4a);

	BOOST_CHECK_EQUAL(v2b.x, 1.1f);
	BOOST_CHECK_EQUAL(v2b.y, 2.2f);

	BOOST_CHECK_EQUAL(v3b.x, 1.1f);
	BOOST_CHECK_EQUAL(v3b.y, 2.2f);
	BOOST_CHECK_EQUAL(v3b.z, 3.3f);

	BOOST_CHECK_EQUAL(v4b.x, 1.1f);
	BOOST_CHECK_EQUAL(v4b.y, 2.2f);
	BOOST_CHECK_EQUAL(v4b.z, 3.3f);
	BOOST_CHECK_EQUAL(v4b.w, 4.4f);
}

BOOST_AUTO_TEST_CASE(VectorDataAccess)
{
	Magnet::Vector2 v2(1.1f, 2.2f);
	Magnet::Vector3 v3(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4(1.1f, 2.2f, 3.3f, 4.4f);
	
	BOOST_CHECK_EQUAL(*v2.getDataPtr(), 1.1f);
	BOOST_CHECK_EQUAL(v2.getDataPtr(), &v2.x);
	BOOST_CHECK_EQUAL(v2[0], 1.1f);
	BOOST_CHECK_EQUAL(v2[1], 2.2f);
	
	BOOST_CHECK_EQUAL(*v3.getDataPtr(), 1.1f);
	BOOST_CHECK_EQUAL(v3.getDataPtr(), &v3.x);
	BOOST_CHECK_EQUAL(v3[0], 1.1f);
	BOOST_CHECK_EQUAL(v3[1], 2.2f);
	BOOST_CHECK_EQUAL(v3[2], 3.3f);

	BOOST_CHECK_EQUAL(*v4.getDataPtr(), 1.1f);
	BOOST_CHECK_EQUAL(v4.getDataPtr(), &v4.x);
	BOOST_CHECK_EQUAL(v4[0], 1.1f);
	BOOST_CHECK_EQUAL(v4[1], 2.2f);
	BOOST_CHECK_EQUAL(v4[2], 3.3f);
	BOOST_CHECK_EQUAL(v4[3], 4.4f);
}

BOOST_AUTO_TEST_CASE(VectorEquality)
{
	Magnet::Vector2 v2(1.1f, 2.2f);
	Magnet::Vector3 v3(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4(1.1f, 2.2f, 3.3f, 4.4f);

	BOOST_CHECK(v2 == Magnet::Vector2(1.1f, 2.2f));
	BOOST_CHECK(!(v2 == Magnet::Vector2(0.0f, 0.0f)));
	BOOST_CHECK(v2 != Magnet::Vector2(0.0f, 0.0f));
	BOOST_CHECK(!(v2 != Magnet::Vector2(1.1f, 2.2f)));
	
	BOOST_CHECK(v3 == Magnet::Vector3(1.1f, 2.2f, 3.3f));
	BOOST_CHECK(!(v3 == Magnet::Vector3(0.0f, 0.0f, 0.0f)));
	BOOST_CHECK(v3 != Magnet::Vector3(0.0f, 0.0f, 0.0f));
	BOOST_CHECK(!(v3 != Magnet::Vector3(1.1f, 2.2f, 3.3f)));
	
	BOOST_CHECK(v4 == Magnet::Vector4(1.1f, 2.2f, 3.3f, 4.4f));
	BOOST_CHECK(!(v4 == Magnet::Vector4(0.0f, 0.0f, 0.0f, 0.0f)));
	BOOST_CHECK(v4 != Magnet::Vector4(0.0f, 0.0f, 0.0f, 0.0f));
	BOOST_CHECK(!(v4 != Magnet::Vector4(1.1f, 2.2f, 3.3f, 4.4f)));
}

BOOST_AUTO_TEST_CASE(VectorNegative)
{
	Magnet::Vector2 v2(1.1f, 2.2f);
	Magnet::Vector3 v3(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4(1.1f, 2.2f, 3.3f, 4.4f);	
	BOOST_CHECK_EQUAL(-v2, Magnet::Vector2(-1.1f, -2.2f));
	BOOST_CHECK_EQUAL(-v3, Magnet::Vector3(-1.1f, -2.2f, -3.3f));
	BOOST_CHECK_EQUAL(-v4, Magnet::Vector4(-1.1f, -2.2f, -3.3f, -4.4f));
}

BOOST_AUTO_TEST_CASE(VectorAddition)
{
	Magnet::Vector2 v2a(1.1f, 2.2f), v2b(2.2f, 4.4f);
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f), v3b(2.2f, 4.4f, 6.6f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f), v4b(2.2f, 4.4f, 6.6f, 8.8f);

	// +
	BOOST_CHECK_EQUAL((v2a + v2b), Magnet::Vector2(3.3f, 6.6f));
	BOOST_CHECK_EQUAL((v3a + v3b), Magnet::Vector3(3.3f, 6.6f, 9.9f));
	BOOST_CHECK_EQUAL((v4a + v4b), Magnet::Vector4(3.3f, 6.6f, 9.9f, 13.2f));

	// +=
	v2a += v2b; 
	v3a += v3b;
	v4a += v4b; 
	BOOST_CHECK_EQUAL(v2a, Magnet::Vector2(3.3f, 6.6f));
	BOOST_CHECK_EQUAL(v3a, Magnet::Vector3(3.3f, 6.6f, 9.9f));
	BOOST_CHECK_EQUAL(v4a, Magnet::Vector4(3.3f, 6.6f, 9.9f, 13.2f));
}

BOOST_AUTO_TEST_CASE(VectorScalarAddition)
{
	Magnet::Vector2 v2(1.1f, 2.2f);
	Magnet::Vector3 v3(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4(1.1f, 2.2f, 3.3f, 4.4f);

	// +
	BOOST_CHECK_EQUAL((v2 + 5.0f), Magnet::Vector2(6.1f, 7.2f));
	BOOST_CHECK_EQUAL((v3 + 5.0f), Magnet::Vector3(6.1f, 7.2f, 8.3f));
	BOOST_CHECK_EQUAL((v4 + 5.0f), Magnet::Vector4(6.1f, 7.2f, 8.3f, 9.4f));

	// +=
	v2 += 5.0f;
	v3 += 5.0f;
	v4 += 5.0f;
	BOOST_CHECK_EQUAL(v2, Magnet::Vector2(6.1f, 7.2f));
	BOOST_CHECK_EQUAL(v3, Magnet::Vector3(6.1f, 7.2f, 8.3f));
	BOOST_CHECK_EQUAL(v4, Magnet::Vector4(6.1f, 7.2f, 8.3f, 9.4f));
}

BOOST_AUTO_TEST_CASE(VectorSubtraction)
{
	Magnet::Vector2 v2a(1.1f, 2.2f), v2b(2.2f, 4.4f);
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f), v3b(2.2f, 4.4f, 6.6f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f), v4b(2.2f, 4.4f, 6.6f, 8.8f);

	// -
	BOOST_CHECK_EQUAL((v2a - v2b), Magnet::Vector2(-1.1f, -2.2f));
	BOOST_CHECK_EQUAL((v3a - v3b), Magnet::Vector3(-1.1f, -2.2f, -3.3f));
	BOOST_CHECK_EQUAL((v4a - v4b), Magnet::Vector4(-1.1f, -2.2f, -3.3f, -4.4f));

	// -=
	v2a -= v2b;
	v3a -= v3b;
	v4a -= v4b;
	BOOST_CHECK_EQUAL(v2a, Magnet::Vector2(-1.1f, -2.2f));
	BOOST_CHECK_EQUAL(v3a, Magnet::Vector3(-1.1f, -2.2f, -3.3f));
	BOOST_CHECK_EQUAL(v4a, Magnet::Vector4(-1.1f, -2.2f, -3.3f, -4.4f));
}

BOOST_AUTO_TEST_CASE(VectorScalarSubtraction)
{	
	Magnet::Vector2 v2a(1.1f, 2.2f);
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f);

	// -
	BOOST_CHECK_EQUAL((v2a - 5.0f), Magnet::Vector2(-3.9f, -2.8f));
	BOOST_CHECK_EQUAL((v3a - 5.0f), Magnet::Vector3(-3.9f, -2.8f, -1.7f));
	BOOST_CHECK_EQUAL((v4a - 5.0f), Magnet::Vector4(-3.9f, -2.8f, -1.7f, -0.6f));

	// -=
	v2a -= 5.0f;
	v3a -= 5.0f;
	v4a -= 5.0f;
	BOOST_CHECK_EQUAL(v2a, Magnet::Vector2(-3.9f, -2.8f));
	BOOST_CHECK_EQUAL(v3a, Magnet::Vector3(-3.9f, -2.8f, -1.7f));
	BOOST_CHECK_EQUAL(v4a, Magnet::Vector4(-3.9f, -2.8f, -1.7f, -0.6f));
}

BOOST_AUTO_TEST_CASE(VectorScalarMultiplication)
{
	Magnet::Vector2 v2a(1.1f, 2.2f); 
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f);

	// * 
	BOOST_CHECK_EQUAL((v2a * 5.0f), Magnet::Vector2(5.5f, 11.0f));
	BOOST_CHECK_EQUAL((v3a * 5.0f), Magnet::Vector3(5.5f, 11.0f, 16.5f));
	BOOST_CHECK_EQUAL((v4a * 5.0f), Magnet::Vector4(5.5f, 11.0f, 16.5f, 22.0f));

	// *=
	v2a *= 5.0f;
	v3a *= 5.0f;
	v4a *= 5.0f;
	BOOST_CHECK_EQUAL(v2a, Magnet::Vector2(5.5f, 11.0f));
	BOOST_CHECK_EQUAL(v3a, Magnet::Vector3(5.5f, 11.0f, 16.5f));
	BOOST_CHECK_EQUAL(v4a, Magnet::Vector4(5.5f, 11.0f, 16.5f, 22.0f));
}

BOOST_AUTO_TEST_CASE(VectorMatrixMultiplication)
{
	Magnet::Vector4 v(1.1f, 2.2f, 3.3f, 4.4f);
	Magnet::Matrix4x4 m(
		1.1f, 1.2f, 1.3f, 1.4f,
		2.1f, 2.2f, 2.3f, 2.4f,
		3.1f, 3.2f, 3.3f, 3.4f,
		4.1f, 4.2f, 4.3f, 4.4f);

	// *
	BOOST_CHECK_EQUAL((v * m), Magnet::Vector4(34.1f, 35.2f, 36.3f, 37.4f));

	// *=
	v *= m;
	BOOST_CHECK_EQUAL(v, Magnet::Vector4(34.1f, 35.2f, 36.3f, 37.4f));
}

BOOST_AUTO_TEST_CASE(VectorCrossProduct)
{
	Magnet::Vector3 va(1.1f, 2.2f, 3.3f), vb(3.3f, 2.2f, 1.1f);

	// cross()
	BOOST_CHECK_EQUAL(va.cross(vb), Magnet::Vector3(-4.84f, 9.68f, -4.84f));

	// ^
	BOOST_CHECK_EQUAL((va ^ vb), Magnet::Vector3(-4.84f, 9.68f, -4.84f));

	// ^=
	va ^= vb;
	BOOST_CHECK_EQUAL(va, Magnet::Vector3(-4.84f, 9.68f, -4.84f));

}

BOOST_AUTO_TEST_CASE(VectorDotProduct)
{
	Magnet::Vector2 v2a(1.1f, 2.2f), v2b(1.1f, 2.2f);
	Magnet::Vector3 v3a(1.1f, 2.2f, 3.3f), v3b(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4a(1.1f, 2.2f, 3.3f, 4.4f), v4b(1.1f, 2.2f, 3.3f, 4.4f);

	// dot()
	float dot2a = v2a.dot(v2b);
	float dot3a = v3a.dot(v3b);
	float dot4a = v4a.dot(v4b);
	BOOST_CHECK((dot2a < 6.05f + EPSILON) && (dot2a > 6.05f - EPSILON));
	BOOST_CHECK((dot3a < 16.94f + EPSILON) && (dot3a > 16.94f - EPSILON));
	BOOST_CHECK((dot4a < 36.3f + EPSILON) && (dot4a > 36.3f - EPSILON));

	// *
	float dot2b = v2a * v2b;
	float dot3b = v3a * v3b;
	float dot4b = v4a * v4b;
	BOOST_CHECK((dot2b < 6.05f + EPSILON) && (dot2b > 6.05f - EPSILON));
	BOOST_CHECK((dot3b < 16.94f + EPSILON) && (dot3b > 16.94f - EPSILON));
	BOOST_CHECK((dot4b < 36.3f + EPSILON) && (dot4b > 36.3f - EPSILON));
}

BOOST_AUTO_TEST_CASE(VectorNormalise)
{
	Magnet::Vector2 v2(1.1f, 2.2f);
	Magnet::Vector3 v3(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4(1.1f, 2.2f, 3.3f, 4.4f);
	BOOST_CHECK_EQUAL(v2.normalise(), Magnet::Vector2(0.44721359f, 0.89442719f));
	BOOST_CHECK_EQUAL(v3.normalise(), Magnet::Vector3(0.26726124f, 0.53452248f, 0.80178372f));
	BOOST_CHECK_EQUAL(v4.normalise(), Magnet::Vector4(0.18257418f, 0.36514837f, 0.54772255f, 0.73029674f));
}

BOOST_AUTO_TEST_CASE(VectorSerialise)
{
	static const char* VECTOR2_BIN = "vector2.bin.tmp";
	static const char* VECTOR3_BIN = "vector3.bin.tmp";
	static const char* VECTOR4_BIN = "vector4.bin.tmp";
		
	Magnet::Vector2 v2out(1.1f, 2.2f);
	Magnet::Vector3 v3out(1.1f, 2.2f, 3.3f);
	Magnet::Vector4 v4out(1.1f, 2.2f, 3.3f, 4.4f);
	std::ofstream v2ofs(VECTOR2_BIN, std::ios::out | std::ios::binary);
	std::ofstream v3ofs(VECTOR3_BIN, std::ios::out | std::ios::binary);
	std::ofstream v4ofs(VECTOR4_BIN, std::ios::out | std::ios::binary);
	BOOST_REQUIRE(v2ofs.is_open());
	BOOST_REQUIRE(v3ofs.is_open());
	BOOST_REQUIRE(v4ofs.is_open());
	boost::archive::binary_oarchive v2oa(v2ofs);
	boost::archive::binary_oarchive v3oa(v3ofs);
	boost::archive::binary_oarchive v4oa(v4ofs);
	v2oa << v2out;
	v3oa << v3out;
	v4oa << v4out;
	v2ofs.close();
	v3ofs.close();
	v4ofs.close();

	Magnet::Vector2 v2in;
	Magnet::Vector3 v3in;
	Magnet::Vector4 v4in;
	std::ifstream v2ifs(VECTOR2_BIN, std::ios::in | std::ios::binary);
	std::ifstream v3ifs(VECTOR3_BIN, std::ios::in | std::ios::binary);
	std::ifstream v4ifs(VECTOR4_BIN, std::ios::in | std::ios::binary);
	BOOST_REQUIRE(v2ifs.is_open());
	BOOST_REQUIRE(v3ifs.is_open());
	BOOST_REQUIRE(v4ifs.is_open());
	boost::archive::binary_iarchive v2ia(v2ifs);
	boost::archive::binary_iarchive v3ia(v3ifs);
	boost::archive::binary_iarchive v4ia(v4ifs);
	v2ia >> v2in;
	v3ia >> v3in;
	v4ia >> v4in;
	v2ifs.close();
	v3ifs.close();
	v4ifs.close();

	BOOST_CHECK_EQUAL(v2in, Magnet::Vector2(1.1f, 2.2f));
	BOOST_CHECK_EQUAL(v3in, Magnet::Vector3(1.1f, 2.2f, 3.3f));
	BOOST_CHECK_EQUAL(v4in, Magnet::Vector4(1.1f, 2.2f, 3.3f, 4.4f));
}

BOOST_AUTO_TEST_SUITE_END()