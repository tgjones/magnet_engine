
#ifndef _MAGNET_TEST_PRECOMPILE_H
#define _MAGNET_TEST_PRECOMPILE_H

#include <MagnetUtil/config.h>

// Standard C++
#include <fstream>

// For serialisation testing
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244) // 'conversion from 'stlpd_std::streamsize' to 'size_t'
#endif
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#ifdef _MSC_VER
#pragma warning(pop)
#endif

// Win32 platform
#ifdef _MAG_PLATFORM_WIN32
#include <windows.h>
#endif

// Boost Test
#define BOOST_TEST_MODULE MagnetTestModule
#include <boost/test/unit_test.hpp>

#endif