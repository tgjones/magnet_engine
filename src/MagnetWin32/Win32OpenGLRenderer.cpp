#include "external.h"

#ifdef _MAG_USE_GL

#include <MagnetWin32/common.h>

void Magnet::Win32OpenGLRenderer::startup()
{
	ASSERT(m_hGlrc == NULL);
	
	// Setup OpenGL pixel format
    PIXELFORMATDESCRIPTOR pfd =
    {   
        sizeof(PIXELFORMATDESCRIPTOR),  // size
        1,                              // version
		PFD_SUPPORT_OPENGL |			// OpenGL window
		PFD_DRAW_TO_WINDOW |	        // render to window
		PFD_DOUBLEBUFFER,               // support double-buffering
		PFD_TYPE_RGBA,                  // color type
		16,								// prefered color depth
		0, 0, 0, 0, 0, 0,				// color bits (ignored)
		0,								// no alpha buffer
		0,								// alpha bits (ignored)
		0,								// no accumulation buffer
		0, 0, 0, 0,						// accum bits (ignored)
		16,								// depth buffer
		0,								// no stencil buffer
		0,								// no auxiliary buffers
		PFD_MAIN_PLANE,					// main layer
		0,								// reserved
		0, 0, 0,						// no layer, visible, damage masks
    };

	// De-reference and down-cast the display pointer now
	Win32Application* pApplication = dynamic_cast<Win32Application*>(g_pApplication);

	// Dynamic cast will return null if it was unsuccessful
	ASSERT_MSG(pApplication != nullptr, "Win32OpenGLRenderer can only be paired with Win32Application");

	// Save device context
	m_hDc = pApplication->getDeviceContextHandle();

	// Get pixel format
	int pixelFormat = ChoosePixelFormat(m_hDc, &pfd);
	ASSERT_MSG(pixelFormat != 0, "Cannot find pixel format");

	// Set pixel format to device context
    SetPixelFormat(m_hDc, pixelFormat, &pfd);

	// Create the OpenGL context
	m_hGlrc = wglCreateContext(m_hDc);

	LOG_DEBUG("OpenGL context successfully created");

	// Make the context current
	wglMakeCurrent(m_hDc, m_hGlrc);

	// Enable/disable vSync
	/*
	if (m_vSync)
		wglSwapIntervalEXT(1);
	else
		wglSwapIntervalEXT(0);
	*/

	// Run cross-platform startup code
	OpenGLRenderer::startup();

	// Reshape
	this->reshape();
}

void Magnet::Win32OpenGLRenderer::shutdown()
{
	// Run base-class code
	OpenGLRenderer::shutdown();

	// Un-current context
	wglMakeCurrent(NULL, NULL);

	// Delete context
	wglDeleteContext(m_hGlrc);
	m_hGlrc = NULL;

	LOG_DEBUG("OpenGL context deleted");
}

void Magnet::Win32OpenGLRenderer::render()
{
	// Cross-platform render code (in base-class)
	OpenGLRenderer::render();

	// Swap buffers
	SwapBuffers(m_hDc);
}

#endif