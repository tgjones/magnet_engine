#include "external.h"
#include <MagnetWin32/common.h>

void Magnet::Win32Application::startup()
{
	// Check hInstance has been set
	ASSERT(m_hInstance);

	// Register Win32 subsystems
	#ifndef _MAG_NO_PHYSICS
	REGISTER_SUBSYSTEM(ST_PHYSICS_ENGINE, PhysXPhysicsEngine);
	#endif
	REGISTER_SUBSYSTEM(ST_INPUT_ENGINE, Win32InputEngine);

	// Compute multiplier to be used by getGameTime() to return milliseconds
	int64_t frequency;
	QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
	m_timeMultiplier = (1.0f / (double)frequency);

	// Set configuration section
	g_pConfigurationManager->setSection("app");

	// Get some general config
	m_title		       = g_pConfigurationManager->getParameterAsString("title", "Magnet Application");
	m_fullscreenWidth  = g_pConfigurationManager->getParameterAsInt("fullscreenWidth", GetSystemMetrics(SM_CXSCREEN));
	m_fullscreenHeight = g_pConfigurationManager->getParameterAsInt("fullscreenHeight", GetSystemMetrics(SM_CYSCREEN));
	m_windowedWidth    = g_pConfigurationManager->getParameterAsInt("width", 640);
	m_windowedHeight   = g_pConfigurationManager->getParameterAsInt("height", 480);
	m_fullscreen       = g_pConfigurationManager->getParameterAsBool("fullscreen", false);

	// Get renderer
	std::string renderer = toLower(g_pConfigurationManager->getParameterAsString("renderer", "direct3d9"));

	// Setup Direct3D 9 renderer
	if ((renderer == "direct3d9") || (renderer == "d3d9") || (renderer == "d3d"))
	{
		REGISTER_SUBSYSTEM(Magnet::ST_RENDERER, Magnet::D3D9Renderer);
		LOG_DEBUG("Using Direct3D 9 renderer");
		m_rendererType = Magnet::WIN32_RENDERER_D3D9;
	}

	// Setup OpenGL renderer
	#ifdef _MAG_USE_GL
	else if ((renderer == "opengl") || (renderer == "ogl") || (renderer == "gl"))
	{
		REGISTER_SUBSYSTEM(Magnet::ST_RENDERER, Magnet::Win32OpenGLRenderer);
		LOG_DEBUG("Using Win32 OpenGL renderer");
		m_rendererType = Magnet::WIN32_RENDERER_OPENGL;
	}
	#endif

	// Unrecognised renderer
	else throw LogicError("Unrecognised/unsupported renderer: '" + renderer + "'");

	LOG_DEBUG("Setting up Win32 display");

	WNDCLASSEX wc;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	const char* className = "MagnetWindowClass";

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = m_hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	if (!m_fullscreen) wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = className;

	HR(RegisterClassEx(&wc));

	/// @todo Look into ChangeScreenResolution API call
	if (m_fullscreen)
	{
		m_hWnd = CreateWindowEx(NULL, className, m_title.c_str(), WS_EX_TOPMOST | WS_POPUP, 0, 0, 
			m_fullscreenWidth, m_fullscreenHeight, HWND_DESKTOP, NULL, NULL, NULL);
		
		ASSERT(m_hWnd != NULL);
		LOG_DEBUG("Opening fullscreen display at " + toString(m_fullscreenWidth) + "x" + toString(m_fullscreenHeight));
	}
	else
	{
		// Figure out actual window size including borders etc
		RECT R = {0, 0, m_windowedWidth, m_windowedHeight};
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);

		// Create window
		m_hWnd = CreateWindowEx(NULL, className, m_title.c_str(), WS_OVERLAPPEDWINDOW, 0, 0, 
			R.right, R.bottom, HWND_DESKTOP, NULL, NULL, NULL);

		ASSERT(m_hWnd != NULL);
		LOG_DEBUG("Opening windowed display at " + toString(m_windowedWidth) + "x" + toString(m_windowedHeight));
	}

	// Get device context
	m_hDc = GetDC(m_hWnd);
	ASSERT(m_hDc != NULL);

	// display the window on the screen
	ShowWindow(m_hWnd, SW_SHOWNORMAL);
}

void Magnet::Win32Application::shutdown()
{
	// Release device context
	ReleaseDC(m_hWnd, m_hDc);
}

void Magnet::Win32Application::setRenderer(Magnet::Win32RendererType rendererType)
{
	ASSERT(m_rendererType != Magnet::WIN32_RENDERER_UNSPECIFIED);

	// Set new renderer
	switch (rendererType)
	{
	case Magnet::WIN32_RENDERER_D3D9:
		REGISTER_SUBSYSTEM(Magnet::ST_RENDERER, Magnet::D3D9Renderer);
		LOG_DEBUG("Switching to Direct3D 9 renderer");
		break;

	case Magnet::WIN32_RENDERER_OPENGL:
		REGISTER_SUBSYSTEM(Magnet::ST_RENDERER, Magnet::Win32OpenGLRenderer);
		LOG_DEBUG("Switching to Win32 OpenGL renderer");
		break;

	case Magnet::WIN32_RENDERER_UNSPECIFIED:
	default:
		throw Magnet::RuntimeError("Unable to switch Win32 renderer");
	}
		
	// Save new renderer
	m_rendererType = rendererType;

	// Startup new renderer
	g_pRenderer->startup();
}

fs::path Magnet::Win32Application::getResourceRoot()
{
	// Run GetCurrentDirectory once to get buffer length
	uint32_t bufferLen = GetCurrentDirectory(0, NULL);

	// Create text buffer
	char* buffer = new char[bufferLen];

	// Run GetCurrentDirectory again to actually retrieve directory
	GetCurrentDirectory(bufferLen, buffer);

	// Convert buffer to fs::path and check path is absolute
	fs::path resourceRoot(buffer);
	ASSERT(resourceRoot.is_absolute());

	// Free temporary buffer
	delete [] buffer;

	// Return path
	return resourceRoot;
}

void Magnet::Win32Application::quit(int code)
{
	LOG_DEBUG("Quit request made");
	m_quit = true; 
	m_quitCode = code;
}

int Magnet::Win32Application::run()
{
	LOG_DEBUG("Starting game loop");

	// Timing variables
	static double timeStamp = 0.0f;
	static double lastFrameTime = 0.0f;
	static double frameStartTime = 0.0f;

	// Infinite loop
	while(!m_quit)
	{
		// Timing
		timeStamp = this->getAbsoluteTime();
		if (frameStartTime != 0.0f) // Skip on first frame
			lastFrameTime = timeStamp - frameStartTime;
		frameStartTime = timeStamp;

		// Win32 message pump
		MSG msg;
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// Ensure keyboard messages are the correct format
			TranslateMessage(&msg);

			// Forward message onto WindowProc
			DispatchMessage(&msg);
		}		

		// Exiting is an exceptional case
		if(msg.message == WM_QUIT) this->quit(0);

		#ifndef _MAG_DEBUG
		// Only do work when application is active, free CPU/GPU otherwise
		if (!this->isActive()) continue;
		#endif

		// Dispatch any scheduled events
		g_pEventManager->dispatchScheduledEvents();

		#ifndef _MAG_NO_PHYSICS
		// Simulate physics
		g_pPhysicsEngine->update(lastFrameTime);
		#endif

		// Pole for new input
		g_pInputEngine->update();
		
		// Update game world
		g_pGameWorld->update(lastFrameTime);

		// Dispatch events queued in this frame
		g_pEventManager->dispatchQueuedEvents();

		// Draw frame
		g_pRenderer->render();
	}

	LOG_DEBUG("Stopping game loop");

	// Return the exit code
	return m_quitCode;
}

// Static Windows callback procedure
LRESULT CALLBACK Magnet::Win32Application::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Downcast a couple of globals
	Win32Application* pWin32Application = static_cast<Win32Application*>(g_pApplication);
	Win32InputEngine* pWin32InputEngine = static_cast<Win32InputEngine*>(g_pInputEngine);

	// Variables used in message switch
	static bool minimized = false;
	static bool maximized = false;
	UINT rawInputSize = sizeof(RAWINPUT);
	RAWINPUT rawInput;
	ZeroMemory(&rawInput, rawInputSize);

	switch(msg)
    {
		// The user has exited the window
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;

		// The window has been minimized/maximized/restored
		case WM_SIZE:

			// Save new windowed size
			if (!pWin32Application->isFullscreen())
			{
				pWin32Application->m_windowedWidth = LOWORD(lParam);
				pWin32Application->m_windowedHeight = HIWORD(lParam);
			}

			// Minimizing window
			if (wParam == SIZE_MINIMIZED)
			{
				minimized = true;
				pWin32Application->setActive(false);
			}

			// Maximizing window
			else if (wParam == SIZE_MAXIMIZED)
			{
				maximized = true;
				pWin32Application->setActive(true);
				g_pRenderer->reshape();
			}

			// Restoring window 
			else if (wParam == SIZE_RESTORED)
			{
				// Only reset if we were minimized or maximized and windowed
				if ((minimized || maximized) && !pWin32Application->isFullscreen())
				{
					g_pRenderer->reshape();
					pWin32Application->setActive(true);
					minimized = false;
					maximized = false;
				}
			}

			break;

		// The user has finished resizing the window
		case WM_EXITSIZEMOVE:

			// Get new client area size
			RECT R;
			GetClientRect(pWin32Application->getWindowHandle(), &R);

			// Save new size
			pWin32Application->setSize((uint16_t)R.right, (uint16_t)R.bottom);

			// Reshape renderer
			g_pRenderer->reshape();

			break;

		// The application has been activated/deactivated
		case WM_ACTIVATEAPP:

			// Activated (and not minimized)
			if ((wParam == TRUE) && (!minimized))
			{
				// Set the application to active (take CPU)
				pWin32Application->setActive(true);

				// Get up to date raw input
				GetRawInputData((HRAWINPUT)lParam, RID_INPUT, &rawInput, &rawInputSize, sizeof(RAWINPUTHEADER));

				// Queue new input (we are in a seperate thread here)
				pWin32InputEngine->queueRawInput(rawInput);
			}

			// Deactivated
			else if (wParam == false)

				// Ease off the CPU
				pWin32Application->setActive(false);

			break;

		// An input device has been used
        case WM_INPUT:

			// Get raw input
            GetRawInputData((HRAWINPUT)lParam, RID_INPUT, &rawInput, &rawInputSize, sizeof(RAWINPUTHEADER));

			// Queue new input (we are in a seperate thread here)
			pWin32InputEngine->queueRawInput(rawInput);

			break;
    }

    return DefWindowProc (hWnd, msg, wParam, lParam);
}

double Magnet::Win32Application::getAbsoluteTime()
{
	ASSERT(m_timeMultiplier != 0.0f);

	// Get time count
	int64_t time = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&time);

	// Return millseconds
	return (double)time * m_timeMultiplier;
}

void Magnet::Win32Application::setFullscreen(bool fullscreen)
{
	if (m_fullscreen != fullscreen)
	{
		// Toggle fullsceen
		m_fullscreen = fullscreen;

		if (m_fullscreen)
		{
			SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_EX_TOPMOST | WS_POPUP);
			SetWindowPos(m_hWnd, HWND_TOP, 0, 0, m_fullscreenWidth, m_fullscreenHeight, SWP_NOZORDER | SWP_SHOWWINDOW);
		}
		else
		{	
			// Figure out actual window size, including borders
			RECT R = {0, 0, m_windowedWidth, m_windowedHeight};
			AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);

			// Set new size and style
			SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
			SetWindowPos(m_hWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);

		}

		// Manually send window message
		SendMessage(m_hWnd, WM_EXITSIZEMOVE, 0, 0);
	}
}

uint16_t Magnet::Win32Application::getPixelWidth()
{
	if (m_fullscreen)
		return m_fullscreenWidth;
	else
		return m_windowedWidth;
}

uint16_t Magnet::Win32Application::getPixelHeight()
{
	if (m_fullscreen)
		return m_fullscreenHeight;
	else
		return m_windowedHeight;
}

void Magnet::Win32Application::setSize(uint16_t width, uint16_t height)
{
	if (m_fullscreen)
	{
		m_fullscreenWidth = width;
		m_fullscreenHeight = height;
	}
	else
	{
		m_windowedWidth = width;
		m_windowedHeight = height;
	}
}