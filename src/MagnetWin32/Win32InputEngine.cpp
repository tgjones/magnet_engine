#include "external.h"
#include <MagnetWin32/common.h>

static const byte_t DEVICE_MOUSE    = 0x01;
static const byte_t DEVICE_KEYBOARD = 0x02;
static const byte_t DEVICE_GAMEPAD  = 0x04;

void Magnet::Win32InputEngine::startup()
{
	g_pConfigurationManager->setSection("input");

	// Parse custom map file is there is one, otherwise use "input-win32.xml"
	this->parseMap(g_pConfigurationManager->getParameterAsString("map", "input-win32.xml"));

	// De-reference and down-cast display now
	Win32Application* pApplication = static_cast<Win32Application*>(g_pApplication);

	// Array for mouseand keyboard devices
	RAWINPUTDEVICE rid[2];

	// Setup keyboard if required
	if (m_enabledDevices & DEVICE_KEYBOARD)
	{
		rid[0].usUsage = 0x06;
		rid[0].usUsagePage = 0x01;
		rid[0].dwFlags = NULL;
		rid[0].hwndTarget = pApplication->getWindowHandle();
	}

	// Setup mouse if required
	if (m_enabledDevices & DEVICE_MOUSE)
	{
		rid[1].usUsage = 0x02;
		rid[1].usUsagePage = 0x01;
		rid[1].dwFlags = NULL;
		rid[1].hwndTarget = pApplication->getWindowHandle();
	}

	// Register keyboard and mouse raw devices as required
	if ((m_enabledDevices & DEVICE_KEYBOARD) && (m_enabledDevices & DEVICE_MOUSE))
	{
		RegisterRawInputDevices(rid, 2, sizeof(RAWINPUTDEVICE));
	}
	else if (m_enabledDevices & DEVICE_KEYBOARD)
	{
		RegisterRawInputDevices(rid, 1, sizeof(RAWINPUTDEVICE));
	}
	else if (m_enabledDevices & DEVICE_MOUSE)
	{
		RegisterRawInputDevices(&rid[1], 1, sizeof(RAWINPUTDEVICE));
	}
}

void Magnet::Win32InputEngine::parseMap(const std::string& mapFile)
{
	// Load document
	TiXmlDocument doc(mapFile.c_str());
	if (doc.LoadFile())
	{
		TiXmlElement *pRoot = doc.FirstChildElement("magnet:input");
		if (pRoot == nullptr)
			throw LogicError("Cannot find root element 'magnet:input' in '" + mapFile + "'"); 

		// Get handle to root node
		TiXmlHandle rootHandle(pRoot);

		#pragma region Parse keyboard key

		// Parse keyboard keys
		for (TiXmlElement* pKey = rootHandle.FirstChild("keyboard").FirstChild("key").ToElement(); 
			pKey != nullptr; pKey = pKey->NextSiblingElement("key"))
		{
			// Enable the keyboard
			if (!(m_enabledDevices & DEVICE_KEYBOARD))
				m_enabledDevices |= DEVICE_KEYBOARD;

			// Get label
			std::string label = pKey->Attribute("label");

			Control key;

			#pragma region Key enumerations

			if      (label == "KEY_1")       key = K_KEY_1;
			else if (label == "KEY_2")       key = K_KEY_2;
			else if (label == "KEY_3")       key = K_KEY_3;
			else if (label == "KEY_4")       key = K_KEY_4;
			else if (label == "KEY_5")       key = K_KEY_5;
			else if (label == "KEY_6")       key = K_KEY_6;
			else if (label == "KEY_7")       key = K_KEY_7;
			else if (label == "KEY_8")       key = K_KEY_8;
			else if (label == "KEY_9")       key = K_KEY_9;
			else if (label == "KEY_0")       key = K_KEY_0;
			else if (label == "KEY_A")       key = K_KEY_A;
			else if (label == "KEY_B")       key = K_KEY_B;
			else if (label == "KEY_C")       key = K_KEY_C;
			else if (label == "KEY_D")       key = K_KEY_D;
			else if (label == "KEY_E")       key = K_KEY_E;
			else if (label == "KEY_F")       key = K_KEY_F;
			else if (label == "KEY_G")       key = K_KEY_G;
			else if (label == "KEY_H")       key = K_KEY_H;
			else if (label == "KEY_I")       key = K_KEY_I;
			else if (label == "KEY_J")       key = K_KEY_J;
			else if (label == "KEY_K")       key = K_KEY_K;
			else if (label == "KEY_L")       key = K_KEY_L;
			else if (label == "KEY_M")       key = K_KEY_M;
			else if (label == "KEY_N")       key = K_KEY_N;
			else if (label == "KEY_O")       key = K_KEY_O;
			else if (label == "KEY_P")       key = K_KEY_P;
			else if (label == "KEY_Q")       key = K_KEY_Q;
			else if (label == "KEY_R")       key = K_KEY_R;
			else if (label == "KEY_S")       key = K_KEY_S;
			else if (label == "KEY_T")       key = K_KEY_T;
			else if (label == "KEY_U")       key = K_KEY_U;
			else if (label == "KEY_V")       key = K_KEY_V;
			else if (label == "KEY_W")       key = K_KEY_W;
			else if (label == "KEY_X")       key = K_KEY_X;
			else if (label == "KEY_Y")       key = K_KEY_Y;
			else if (label == "KEY_Z")       key = K_KEY_Z;
			else if (label == "KEY_MINUS")   key = K_KEY_MINUS;
			else if (label == "KEY_PLUS")    key = K_KEY_PLUS;
			else if (label == "KEY_L_SQ_BR") key = K_KEY_L_SQ_BR;
			else if (label == "KEY_R_SQ_BR") key = K_KEY_R_SQ_BR;
			else if (label == "KEY_COMMA")   key = K_KEY_COMMA;
			else if (label == "KEY_PERIOD")  key = K_KEY_PERIOD;
			else if (label == "KEY_L_SHIFT") key = K_KEY_L_SHIFT;
			else if (label == "KEY_R_SHIFT") key = K_KEY_R_SHIFT;
			else if (label == "KEY_L_CTRL")  key = K_KEY_L_CTRL;
			else if (label == "KEY_R_CTRL")  key = K_KEY_R_CTRL;
			else if (label == "KEY_L_ALT")   key = K_KEY_L_ALT;
			else if (label == "KEY_R_ALT")   key = K_KEY_L_ALT;
			else if (label == "KEY_SPACE")   key = K_KEY_SPACE;
			else if (label == "KEY_ARROW_R") key = K_KEY_ARROW_R;
			else if (label == "KEY_ARROW_L") key = K_KEY_ARROW_L;
			else if (label == "KEY_ARROW_U") key = K_KEY_ARROW_U;
			else if (label == "KEY_ARROW_D") key = K_KEY_ARROW_D;
			else if (label == "KEY_F1")      key = K_KEY_F1;
			else if (label == "KEY_F2")      key = K_KEY_F2;
			else if (label == "KEY_F3")      key = K_KEY_F3;
			else if (label == "KEY_F4")      key = K_KEY_F4;
			else if (label == "KEY_F5")      key = K_KEY_F5;
			else if (label == "KEY_F6")      key = K_KEY_F6;
			else if (label == "KEY_F7")      key = K_KEY_F7;
			else if (label == "KEY_F8")      key = K_KEY_F8;
			else if (label == "KEY_F9")      key = K_KEY_F9;
			else if (label == "KEY_F10")     key = K_KEY_F10;
			else if (label == "KEY_F11")     key = K_KEY_F11;
			else if (label == "KEY_F12")     key = K_KEY_F12;
			else
				throw LogicError("Unrecognised keyboard key '" + label + "'");

			#pragma endregion

			// Register directives
			for (TiXmlElement* pDirective = pKey->FirstChildElement("directive");
				pDirective != nullptr; pDirective = pDirective->NextSiblingElement("directive"))
			{
				std::string directive = pDirective->GetText();
				m_directives[key].push_back(directive);
			}
		}

		#pragma endregion

		#pragma region Parse gamepad buttons

		// Parse gamepad buttons
		for (TiXmlElement* pButton = rootHandle.FirstChild("gamepad").FirstChild("button").ToElement();
			pButton != nullptr; pButton = pButton->NextSiblingElement("button"))
		{
			// Enable the gamepad
			if (!(m_enabledDevices & DEVICE_GAMEPAD))
				m_enabledDevices |= DEVICE_GAMEPAD;

			// Get label
			std::string label = pButton->Attribute("label");

			Control button;
			
			#pragma region Button enumerations

			if (label == "DPAD_U")				button = G_DPAD_U;
			else if (label == "DPAD_UR")		button = G_DPAD_UR;
			else if (label == "DPAD_R")			button = G_DPAD_R;
			else if (label == "DPAD_DR")		button = G_DPAD_DR;
			else if (label == "DPAD_D")			button = G_DPAD_D;
			else if (label == "DPAD_DL")		button = G_DPAD_DL;
			else if (label == "DPAD_L")			button = G_DPAD_L;
			else if (label == "DPAD_UL")		button = G_DPAD_UL;
			else if (label == "BUTTON_A")		button = G_BUTTON_A;
			else if (label == "BUTTON_B")		button = G_BUTTON_B;
			else if (label == "BUTTON_X")		button = G_BUTTON_X;
			else if (label == "BUTTON_Y")		button = G_BUTTON_Y;
			else if (label == "BUTTON_LB")		button = G_BUTTON_LB;
			else if (label == "BUTTON_RB")		button = G_BUTTON_RB;
			else if (label == "BUTTON_THUMB_L") button = G_BUTTON_THUMB_L;
			else if (label == "BUTTON_THUMB_R") button = G_BUTTON_THUMB_R;
			else if (label == "BUTTON_BACK")    button = G_BUTTON_BACK;
			else if (label == "BUTTON_START")   button = G_BUTTON_START;
			else throw LogicError("Unrecognised gamepad button '" + label + "'");

			#pragma endregion

			// Register directives
			for (TiXmlElement* pDirective = pButton->FirstChildElement("directive");
				pDirective != nullptr; pDirective = pDirective->NextSiblingElement("directive"))
			{
				std::string directive = pDirective->GetText();
				m_directives[button].push_back(directive);
			}
		}

		#pragma endregion

		#pragma region Parse gamepad planes

		// Parse gamepad planes
		for (TiXmlElement* pPlane = rootHandle.FirstChild("gamepad").FirstChild("plane").ToElement();
			pPlane != nullptr; pPlane = pPlane->NextSiblingElement("plane"))
		{
			// Enable the gamepad
			if (!(m_enabledDevices & DEVICE_GAMEPAD))
				m_enabledDevices |= DEVICE_GAMEPAD;

			// Get label
			std::string label = pPlane->Attribute("label");

			Control plane;

			#pragma region Plane enumerationrs

			if (label == "PLANE_THUMB_L") plane = G_PLANE_THUMB_L;
			else if (label == "PLANE_THUMB_R") plane = G_PLANE_THUMB_R;
			else
				throw LogicError("Unrecognised gamepad plane '" + label + "'");

			#pragma endregion

			// Register directives
			for (TiXmlElement* pDirective = pPlane->FirstChildElement("directive");
				pDirective != nullptr; pDirective = pDirective->NextSiblingElement("directive"))
			{
				std::string directive = pDirective->GetText();
				m_directives[plane].push_back(directive);
			}
		}

		#pragma endregion
				
		#pragma region Parse gamepad axes

		// Parse gamepad axes
		for (TiXmlElement* pAxis = rootHandle.FirstChild("gamepad").FirstChild("axis").ToElement();
			pAxis != nullptr; pAxis = pAxis->NextSiblingElement("axis"))
		{
			// Enable the gamepad
			if (!(m_enabledDevices & DEVICE_GAMEPAD))
				m_enabledDevices |= DEVICE_GAMEPAD;

			// Get label
			std::string label = pAxis->Attribute("label");

			Control axis;

			#pragma region Axis enumerationrs

			if (label == "AXIS_LT") axis = G_AXIS_LT;
			else if (label == "AXIS_RT") axis = G_AXIS_RT;
			else throw LogicError("Unrecognised gamepad axis '" + label + "'");

			#pragma endregion

			// Register directives
			for (TiXmlElement* pDirective = pAxis->FirstChildElement("directive");
				pDirective != nullptr; pDirective = pDirective->NextSiblingElement("directive"))
			{
				std::string directive = pDirective->GetText();
				m_directives[axis].push_back(directive);
			}
		}

		#pragma endregion

		LOG_DEBUG("Input map '" + mapFile + "' parsed");
	}

	// Could not load file
	else LOG_WARNING("Cannot open input map '" + mapFile + "'");
}

void Magnet::Win32InputEngine::update()
{	
	// For each queued raw input struct
	while (!m_rawInput.empty())
	{
		RAWINPUT rawInput = m_rawInput.front();
		m_rawInput.pop();

		// If keyboard input is received
		if ((m_enabledDevices & DEVICE_KEYBOARD) && (rawInput.header.dwType == RIM_TYPEKEYBOARD))
		{
			bool depressed = !(rawInput.data.keyboard.Flags & RI_KEY_BREAK);

			switch (rawInput.data.keyboard.VKey)
			{

			case 0x31: // 1
				m_currentState[K_KEY_1].setDigital(depressed);
				break;

			case 0x32: // 2
				m_currentState[K_KEY_2].setDigital(depressed);
				break;

			case 0x33: // 3
				m_currentState[K_KEY_3].setDigital(depressed);
				break;

			case 0x34: // 4
				m_currentState[K_KEY_4].setDigital(depressed);
				break;

			case 0x35: // 5
				m_currentState[K_KEY_5].setDigital(depressed);
				break;

			case 0x36: // 6
				m_currentState[K_KEY_6].setDigital(depressed);
				break;

			case 0x37: // 7
				m_currentState[K_KEY_7].setDigital(depressed);
				break;

			case 0x38: // 8
				m_currentState[K_KEY_8].setDigital(depressed);
				break;

			case 0x39: // 9
				m_currentState[K_KEY_9].setDigital(depressed);
				break;

			case 0x41: // A
				m_currentState[K_KEY_A].setDigital(depressed);
				break;

			case 0x42: // B
				m_currentState[K_KEY_B].setDigital(depressed);
				break;

			case 0x43: // C
				m_currentState[K_KEY_C].setDigital(depressed);
				break;

			case 0x44: // D
				m_currentState[K_KEY_D].setDigital(depressed);
				break;

			case 0x45: // E
				m_currentState[K_KEY_E].setDigital(depressed);
				break;

			case 0x46: // F
				m_currentState[K_KEY_F].setDigital(depressed);
				break;

			case 0x47: // G
				m_currentState[K_KEY_G].setDigital(depressed);
				break;

			case 0x48: // H
				m_currentState[K_KEY_H].setDigital(depressed);
				break;

			case 0x49: // I
				m_currentState[K_KEY_I].setDigital(depressed);
				break;

			case 0x4A: // J
				m_currentState[K_KEY_J].setDigital(depressed);
				break;

			case 0x4B: // K
				m_currentState[K_KEY_K].setDigital(depressed);
				break;

			case 0x4C: // L
				m_currentState[K_KEY_L].setDigital(depressed);
				break;

			case 0x4D: // M
				m_currentState[K_KEY_M].setDigital(depressed);
				break;

			case 0x4E: // N
				m_currentState[K_KEY_N].setDigital(depressed);
				break;

			case 0x4F: // O
				m_currentState[K_KEY_O].setDigital(depressed);
				break;

			case 0x50: // P
				m_currentState[K_KEY_P].setDigital(depressed);
				break;

			case 0x51: // Q
				m_currentState[K_KEY_Q].setDigital(depressed);
				break;

			case 0x52: // R
				m_currentState[K_KEY_R].setDigital(depressed);
				break;

			case 0x53: // S
				m_currentState[K_KEY_S].setDigital(depressed);
				break;

			case 0x54: // T
				m_currentState[K_KEY_T].setDigital(depressed);
				break;

			case 0x55: // U
				m_currentState[K_KEY_U].setDigital(depressed);
				break;

			case 0x56: // V
				m_currentState[K_KEY_V].setDigital(depressed);
				break;

			case 0x57: // W
				m_currentState[K_KEY_W].setDigital(depressed);
				break;

			case 0x58: // X
				m_currentState[K_KEY_X].setDigital(depressed);
				break;

			case 0x59: // Y
				m_currentState[K_KEY_Y].setDigital(depressed);
				break;

			case 0x5A: // Z
				m_currentState[K_KEY_Z].setDigital(depressed);
				break;

			case VK_F1: // F1
				m_currentState[K_KEY_F1].setDigital(depressed);
				break;

			case VK_F2: // F2
				m_currentState[K_KEY_F2].setDigital(depressed);
				break;

			case VK_F3: // F3
				m_currentState[K_KEY_F3].setDigital(depressed);
				break;

			case VK_F4: // F4
				m_currentState[K_KEY_F4].setDigital(depressed);
				break;

			case VK_F5: // F5
				m_currentState[K_KEY_F5].setDigital(depressed);
				break;

			case VK_F6: // F6
				m_currentState[K_KEY_F6].setDigital(depressed);
				break;

			case VK_F7: // F7
				m_currentState[K_KEY_F7].setDigital(depressed);
				break;

			case VK_F8: // F8
				m_currentState[K_KEY_F8].setDigital(depressed);
				break;

			case VK_F9: // F9
				m_currentState[K_KEY_F9].setDigital(depressed);
				break;

			case VK_F10: // F10
				m_currentState[K_KEY_F10].setDigital(depressed);
				break;

			case VK_F11: // F11
				m_currentState[K_KEY_F11].setDigital(depressed);
				break;

			case VK_F12: // F12
				m_currentState[K_KEY_F12].setDigital(depressed);
				break;

			case VK_OEM_MINUS:
				m_currentState[K_KEY_MINUS].setDigital(depressed);
				break;

			case VK_OEM_PLUS:
				m_currentState[K_KEY_PLUS].setDigital(depressed);
				break;

			case VK_OEM_4: // [{ on US keyboards
				m_currentState[K_KEY_L_SQ_BR].setDigital(depressed);
				break;

			case VK_OEM_6: // ]} on US keyboards
				m_currentState[K_KEY_R_SQ_BR].setDigital(depressed);
				break;

			case VK_OEM_COMMA:
				m_currentState[K_KEY_COMMA].setDigital(depressed);
				break;

			case VK_OEM_PERIOD:
				m_currentState[K_KEY_PERIOD].setDigital(depressed);
				break;

			case VK_SHIFT:

				// Left
				if (rawInput.data.keyboard.Flags & RI_KEY_E0) 
					m_currentState[K_KEY_L_SHIFT].setDigital(depressed);

				// Right
				else if (rawInput.data.keyboard.Flags & RI_KEY_E1)
					m_currentState[K_KEY_R_SHIFT].setDigital(depressed);

				break;

			case VK_CONTROL:

				// Left
				if (rawInput.data.keyboard.Flags & RI_KEY_E0)
					m_currentState[K_KEY_L_CTRL].setDigital(depressed);

				// Right
				else if (rawInput.data.keyboard.Flags & RI_KEY_E1)
					m_currentState[K_KEY_R_CTRL].setDigital(depressed);

				break;

			case VK_MENU:

				// Left
				if (rawInput.data.keyboard.Flags & RI_KEY_E0)
					m_currentState[K_KEY_L_ALT].setDigital(depressed);

				// Right
				else if (rawInput.data.keyboard.Flags & RI_KEY_E1)
					m_currentState[K_KEY_R_ALT].setDigital(depressed);

				break;
				
			case VK_SPACE:
				m_currentState[K_KEY_SPACE].setDigital(depressed);
				break;

			case VK_RETURN:
				m_currentState[K_KEY_RETURN].setDigital(depressed);
				break;

			case VK_RIGHT:
				m_currentState[K_KEY_ARROW_R].setDigital(depressed);
				break;

			case VK_LEFT:
				m_currentState[K_KEY_ARROW_L].setDigital(depressed);
				break;

			case VK_UP:
				m_currentState[K_KEY_ARROW_U].setDigital(depressed);
				break;

			case VK_DOWN:
				m_currentState[K_KEY_ARROW_D].setDigital(depressed);
				break;
			}
		}

		// If mouse input is received
		else if ((m_enabledDevices & DEVICE_MOUSE) && (rawInput.header.dwType == RIM_TYPEMOUSE))
		{

		}

		// No input received (which means I need to clear everything)
		else
		{
			// Clear first three floats of all the state structs
			for (auto itr = m_currentState.begin(); itr != m_currentState.end(); ++itr)
				ZeroMemory(&*itr, 3 * sizeof(float));
		}
	}
		
	// For XInput (gamepad) pole XInput sub-system
	if (m_enabledDevices & DEVICE_GAMEPAD)
	{
		//! @todo Support more than 1 controller!

		XINPUT_STATE xInputState;
		ZeroMemory(&xInputState, sizeof(XINPUT_STATE));
		DWORD dwResult = XInputGetState(0, &xInputState);
	
		// If controller is connected
		if(dwResult == ERROR_SUCCESS)
		{
			// Clear the whole DPAD
			m_currentState[G_DPAD_U].setDigital(false);
			m_currentState[G_DPAD_UR].setDigital(false);
			m_currentState[G_DPAD_R].setDigital(false);
			m_currentState[G_DPAD_DR].setDigital(false);
			m_currentState[G_DPAD_D].setDigital(false);
			m_currentState[G_DPAD_DL].setDigital(false);
			m_currentState[G_DPAD_L].setDigital(false);
			m_currentState[G_DPAD_UL].setDigital(false);

			// G_DPAD_UR
			if ((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) &&
				(xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT))
				m_currentState[G_DPAD_UR].setDigital(true);

			// G_DPAD_DR
			else if ((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) &&
				(xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT))
				m_currentState[G_DPAD_DR].setDigital(true);

			// G_DPAD_DL
			else if ((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) &&
				(xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT))
				m_currentState[G_DPAD_DL].setDigital(true);

			// G_DPAD_UL
			else if ((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) &&
				(xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT))
				m_currentState[G_DPAD_UL].setDigital(true);
				
			// G_DPAD_U
			else if (xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
				m_currentState[G_DPAD_U].setDigital(true);
				
			// G_DPAD_L
			else if (xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
				m_currentState[G_DPAD_L].setDigital(true);

			// G_DPAD_D
			else if (xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
				m_currentState[G_DPAD_D].setDigital(true);

			// G_DPAD_R
			else if (xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
				m_currentState[G_DPAD_R].setDigital(true);

			m_currentState[G_BUTTON_A].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_A) > 0);
			m_currentState[G_BUTTON_B].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_B) > 0);
			m_currentState[G_BUTTON_X].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_X) > 0);
			m_currentState[G_BUTTON_Y].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_Y) > 0);
			m_currentState[G_BUTTON_LB].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) > 0);
			m_currentState[G_BUTTON_RB].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) > 0);
			m_currentState[G_BUTTON_THUMB_L].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) > 0);
			m_currentState[G_BUTTON_THUMB_R].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) > 0);
			m_currentState[G_BUTTON_START].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_START) > 0);		
			m_currentState[G_BUTTON_BACK].setDigital((xInputState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) > 0);

			float LX = xInputState.Gamepad.sThumbLX;
			float LY = xInputState.Gamepad.sThumbLY;

			// Determine how far the controller is pushed
			float magnitudeL = std::sqrt(LX * LX + LY * LY);

			// Check if the controller is outside a circular dead zone
			if (magnitudeL > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
			{
				// Determine the direction the controller is pushed
				float normalisedLX = LX / magnitudeL;
				float normalisedLY = LY / magnitudeL;

				// Clip the magnitude at its expected maximum value
				if (magnitudeL > 32767) magnitudeL = 32767;
  
				// Adjust magnitude relative to the end of the dead zone
				magnitudeL -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;

				// Normalise the magnitude with respect to its expected range (range 0.0 -> 1.0)
				float normalisedMagnitudeL = magnitudeL / (32767 - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

				// Set state
				m_currentState[G_PLANE_THUMB_L].setPlane(normalisedMagnitudeL, normalisedLX, normalisedLY);
			}

			// Clear state
			else m_currentState[G_PLANE_THUMB_L].setPlane(0.0f, 0.0f, 0.0f);

			float RX = xInputState.Gamepad.sThumbRX;
			float RY = xInputState.Gamepad.sThumbRY;

			// Determine how far the controller is pushed
			float magnitudeR = std::sqrt(RX * RX + RY * RY);

			// Check if the controller is outside a circular dead zone
			if (magnitudeR > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
			{
				// Determine the direction the controller is pushed
				float normalisedRX = RX / magnitudeR;
				float normalisedRY = RY / magnitudeR;

				// Clip the magnitude at its expected maximum value
				if (magnitudeR > 32767) magnitudeR = 32767;
  
				// Adjust magnitude relative to the end of the dead zone
				magnitudeR -= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;

				// Normalise the magnitude with respect to its expected range (range 0.0 -> 1.0)
				float normalisedMagnitudeR = magnitudeR / (32767 - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

				// Set state
				m_currentState[G_PLANE_THUMB_R].setPlane(normalisedMagnitudeR, normalisedRX, normalisedRY);
			}

			// Clear state
			else m_currentState[G_PLANE_THUMB_R].setPlane(0.0f, 0.0f, 0.0f);

			BYTE LT = xInputState.Gamepad.bLeftTrigger;
			if (LT > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
			{
				float magnitude = ((float)LT - XINPUT_GAMEPAD_TRIGGER_THRESHOLD) / 
					(255.0f - XINPUT_GAMEPAD_TRIGGER_THRESHOLD);

				// Set state
				m_currentState[G_AXIS_LT].setAxis(magnitude);
			}

			// Clear state
			else m_currentState[G_AXIS_LT].setAxis(0.0f);

			BYTE RT = xInputState.Gamepad.bRightTrigger;
			if (RT > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
			{
				float magnitude = ((float)RT - XINPUT_GAMEPAD_TRIGGER_THRESHOLD) / 
					(255.0f - XINPUT_GAMEPAD_TRIGGER_THRESHOLD);

				// Set state
				m_currentState[G_AXIS_RT].setAxis(magnitude);
			}

			// Clear state
			else m_currentState[G_AXIS_RT].setAxis(0.0f);
		}
	}

	// Fast return if input state has not changed
	if (std::memcmp((void*)&m_currentState[0], (void*)&m_previousState[0], 
		NUM_CONTROLS * sizeof(ControlState)) == 0) return;

	// For every control
	for (int i = 0; i < NUM_CONTROLS; i++)
	{
		// Get current and previous states
		ControlState* pCurrentState = &m_currentState[i];
		ControlState* pPreviousState = &m_previousState[i];

		switch (pCurrentState->mode)
		{
		case IM_DIGITAL:

			// If state has changed
			if (pCurrentState->digital.depressed != pPreviousState->digital.depressed)
			{
				// For each mapped directive
				for (auto itr = m_directives[i].begin(); itr != m_directives[i].end(); ++itr)
				{
					Event event("input:" + *itr);
					event.setArgument("mode", Variant(IM_DIGITAL));
					event.setArgument("depressed", Variant(pCurrentState->digital.depressed));
					g_pEventManager->sendEvent(event);
				}

				// Update previous state
				pPreviousState->digital.depressed = pCurrentState->digital.depressed;
			}

			break;

		case IM_AXIS:

			// If state has changed
			if (pCurrentState->axis.magnitude != pPreviousState->axis.magnitude)
			{
				// For each mapped directive
				for (auto itr = m_directives[i].begin(); itr != m_directives[i].end(); ++itr)
				{
					Event event("input:" + *itr);
					event.setArgument("mode", Variant(IM_AXIS));
					event.setArgument("magnitude", Variant(pCurrentState->axis.magnitude));
					g_pEventManager->sendEvent(event);
				}
				
				// Update previous state
				pPreviousState->axis.magnitude = pCurrentState->axis.magnitude;
			}

			break;

		case IM_PLANE:

			// If state has changed
			if ((pCurrentState->plane.magnitude != pPreviousState->plane.magnitude) ||
				(pCurrentState->plane.x != pPreviousState->plane.x) ||
				(pCurrentState->plane.y != pPreviousState->plane.y))
			{
				// For each mapped directive
				for (auto itr = m_directives[i].begin(); itr != m_directives[i].end(); ++itr)
				{
					Event event("input:" + *itr);
					event.setArgument("mode", Variant(IM_PLANE));
					event.setArgument("magnitude", Variant(pCurrentState->plane.magnitude));
					event.setArgument("directionX", Variant(pCurrentState->plane.x));
					event.setArgument("directionY", Variant(pCurrentState->plane.y));
					g_pEventManager->sendEvent(event);
				}

				// Update previous state
				pPreviousState->plane.magnitude = pCurrentState->plane.magnitude;
				pPreviousState->plane.x = pCurrentState->plane.x;
				pPreviousState->plane.y = pCurrentState->plane.y;
			}

			break;
		}
	}
}