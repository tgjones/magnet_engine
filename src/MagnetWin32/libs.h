#ifndef _MAGNET_WIN32_LIBS_H
#define _MAGNET_WIN32_LIBS_H

// OpenGL
#ifdef _MAG_USE_GL
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32.lib")
#endif

// Direct3D
#ifdef _MAG_USE_D3D
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3dx9.lib")
#ifdef _MAG_DEBUG
#pragma comment(lib, "DxErr.lib")
#endif
#endif

// XInput
#pragma comment(lib, "XInput.lib")

// PhysX
#if !defined(_MAG_NO_PHYSICS) && defined(_MAG_USE_PHYSX)
#pragma comment(lib, "PhysXLoader.lib")
#pragma comment(lib, "PhysXCore.lib")
#endif

// TinyXML
#pragma comment(lib, "tinyxml.lib")

// Google Protocol Buffers
#pragma comment(lib, "libprotobuf.lib")

// Cut import dependencies from slimline and STLport builds
#if !defined(_MAG_NO_IMPORT)

	// PNG and ZLib C libs (release mode, seem to work...)
	#pragma comment(lib, "libpng.lib")
	#pragma comment(lib, "zlibstat.lib")

	// Assimp *appears* to not mind the release LIB and DLL being
	// used in debug mode, is this bad?!
	#pragma comment(lib, "assimp.lib")

	// COLLADA DOM (COLLADA Physics)
	#if !defined(_MAG_NO_PHYSICS)
		#if defined(_MAG_DEBUG)
		#pragma comment(lib, "libcollada14dom22-d.lib")
		#else
		#pragma comment(lib, "libcollada14dom22.lib")
		#endif
	#endif

#endif

#endif