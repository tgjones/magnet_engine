#ifndef _MAGNET_WIN32_WIN32_INPUT_ENGINE
#define _MAGNET_WIN32_WIN32_INPUT_ENGINE

#include <MagnetWin32/common.h>

namespace Magnet 
{
	class Win32Application;

	class Win32InputEngine : public InputEngine
	{

	public:

		void startup();

		inline void shutdown() {}

	protected:

		inline Win32InputEngine()  : m_enabledDevices(0x00), m_hWnd(nullptr) {}

		void parseMap(const std::string& mapFile);

		inline void queueRawInput(const RAWINPUT& rawInput) { m_rawInput.push(rawInput); }

		void update();

	private:

		friend class SubsystemFactory;

		friend class Win32Application;

		HWND m_hWnd;

		enum Control
		{
			M_BUTTON_1,
			M_BUTTON_2,
			M_BUTTON_3,
			M_AXIS_X,
			M_AXIS_Y,
			K_KEY_1,
			K_KEY_2,      
			K_KEY_3,     
			K_KEY_4,    
			K_KEY_5,   
			K_KEY_6,  
			K_KEY_7,
			K_KEY_8,
			K_KEY_9,
			K_KEY_0,
			K_KEY_A,
			K_KEY_B,
			K_KEY_C,
			K_KEY_D,
			K_KEY_E,
			K_KEY_F,
			K_KEY_G,
			K_KEY_H,
			K_KEY_I,
			K_KEY_J,
			K_KEY_K,
			K_KEY_L,
			K_KEY_M,
			K_KEY_N,
			K_KEY_O,
			K_KEY_P,
			K_KEY_Q,
			K_KEY_R,
			K_KEY_S,
			K_KEY_T,
			K_KEY_U,
			K_KEY_V,
			K_KEY_W,
			K_KEY_X,
			K_KEY_Y,
			K_KEY_Z,
			K_KEY_MINUS,
			K_KEY_PLUS,
			K_KEY_L_SQ_BR,
			K_KEY_R_SQ_BR,
			K_KEY_COMMA,
			K_KEY_PERIOD,
			K_KEY_L_SHIFT,
			K_KEY_R_SHIFT,
			K_KEY_L_CTRL,
			K_KEY_R_CTRL,
			K_KEY_L_ALT,
			K_KEY_R_ALT,
			K_KEY_SPACE,
			K_KEY_RETURN,
			K_KEY_ARROW_R,
			K_KEY_ARROW_L,
			K_KEY_ARROW_U,
			K_KEY_ARROW_D,
			K_KEY_F1,
			K_KEY_F2,
			K_KEY_F3,
			K_KEY_F4,
			K_KEY_F5,
			K_KEY_F6,
			K_KEY_F7,
			K_KEY_F8,
			K_KEY_F9,
			K_KEY_F10,
			K_KEY_F11,
			K_KEY_F12,
			G_DPAD_U,
			G_DPAD_UR,
			G_DPAD_R,
			G_DPAD_DR,
			G_DPAD_D,
			G_DPAD_DL,
			G_DPAD_L,
			G_DPAD_UL,
			G_BUTTON_A,
			G_BUTTON_B,
			G_BUTTON_X,
			G_BUTTON_Y,
			G_BUTTON_LB,
			G_BUTTON_RB,
			G_BUTTON_START,
			G_BUTTON_BACK,
			G_BUTTON_THUMB_L,
			G_BUTTON_THUMB_R,
			G_PLANE_THUMB_R,
			G_PLANE_THUMB_L,
			G_AXIS_LT,
			G_AXIS_RT,
			NUM_CONTROLS // Number of controls
		};

		// Enabled device flags.
		// 0x01 : Mouse
		// 0x02 : Keyboard
		// 0x03 : Gamepad
		byte_t m_enabledDevices;

		// Raw input state struct queue
		std::queue<RAWINPUT> m_rawInput;

		// Registered directives
		std::tr1::array<std::list<std::string>, NUM_CONTROLS> m_directives;
				
		// Control state struct
		struct ControlState
		{
			// Control state data
			union
			{
				struct { bool depressed; } digital;
				struct { float magnitude; } axis;
				struct { float magnitude; float x; float y; } plane;
			};

			// Input mode
			InputMode mode;

			inline void setDigital(bool depressed)
			{
				mode = IM_DIGITAL;
				digital.depressed = depressed;
			}

			inline void setAxis(float magnitude)
			{
				mode = IM_AXIS;
				axis.magnitude = magnitude;
			}

			inline void setPlane(float magnitude, float x, float y)
			{
				mode = IM_PLANE;
				plane.magnitude = magnitude;
				plane.x = x;
				plane.y = y;
			}
		};

		// Current state of the input devices
		std::tr1::array<ControlState, NUM_CONTROLS> m_currentState;
		
		// Previous state of the input devices (for comparison)
		std::tr1::array<ControlState, NUM_CONTROLS> m_previousState;
	};
}

#endif