
#ifndef _MAGNET_WIN32_WIN32_OPEN_GL_RENDERER_H
#define _MAGNET_WIN32_WIN32_OPEN_GL_RENDERER_H
#ifdef _MAG_USE_GL

#include <MagnetWin32/common.h>

namespace Magnet 
{

	class Win32OpenGLRenderer : public OpenGLRenderer
	{
	public:

		inline ~Win32OpenGLRenderer() {}

		void startup();

		void shutdown();

		void render();

		inline HGLRC getNativeContext() { return m_hGlrc; }

	protected:

		inline Win32OpenGLRenderer() : m_hGlrc(NULL), m_hDc(NULL) {}

	private:

		friend class SubsystemFactory;

		HDC m_hDc; // Copy of Win32Application's hDc
		HGLRC m_hGlrc; // Created and owned by this class
	};
}

#endif
#endif