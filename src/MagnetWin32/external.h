#ifndef _MAGNET_WIN32_PRECOMPILE_H
#define _MAGNET_WIN32_PRECOMPILE_H

#include <MagnetUtil/config.h>

// C++ standard library
#include <string>
#include <sstream>
#include <queue>

// Boost
#include <boost/array.hpp>

// Windows
#include <windows.h>

// Direct 3D
#ifdef _MAG_USE_D3D

#ifdef _MAG_DEBUG
#define D3D_DEBUG_INFO
//#include <DxErr.h>
#endif

#include <d3d9.h>
//#include <d3dx9.h>

#endif

// XInput
#include <XInput.h>

#endif
