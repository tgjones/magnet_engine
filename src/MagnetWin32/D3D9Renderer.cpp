#include "external.h"
#include <MagnetWin32/common.h>

#ifdef _MAG_USE_D3D

void Magnet::D3D9Renderer::startup()
{
	// Run base-class code
	Renderer::startup();

	// Set config secion
	g_pConfigurationManager->setSection("renderer");

	// De-reference and down-cast the display pointer now
	Win32Application* pApplication =  nullptr;
	if ((pApplication = dynamic_cast<Win32Application*>(g_pApplication)) == nullptr)
		throw LogicError("D3D9Renderer can only be paired with Win32Application");

	// Create D3D9 interface
	IDirect3D9* pDirect3D = Direct3DCreate9(D3D_SDK_VERSION);

	// Check windowed display format
	D3DDISPLAYMODE mode;
	pDirect3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	HR(pDirect3D->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, mode.Format, mode.Format, true));

	// Check fullscreen display format
	HR(pDirect3D->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false));

	// Get device capabilities
	HR(pDirect3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &m_deviceCaps));

	// Set vSync from config
	if (g_pConfigurationManager->getParameterAsBool("vSync", true))
		m_presentParameters.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
	else
		m_presentParameters.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	// Set fullscreen/windowed stuff
	if (pApplication->isFullscreen())
	{
		m_presentParameters.Windowed = FALSE;
		m_presentParameters.BackBufferFormat = mode.Format;
	}
	else
	{
		m_presentParameters.Windowed = TRUE;
		m_presentParameters.BackBufferFormat = D3DFMT_X8R8G8B8;
	}

	// Set back buffer size
	m_presentParameters.BackBufferWidth = pApplication->getPixelWidth();
	m_presentParameters.BackBufferHeight = pApplication->getPixelHeight();

	// Set some other presentation parameters
	m_presentParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_presentParameters.hDeviceWindow = pApplication->getWindowHandle();
	m_presentParameters.EnableAutoDepthStencil = TRUE;
    m_presentParameters.AutoDepthStencilFormat = D3DFMT_D16;

	// Set default settings
	UINT adapter = D3DADAPTER_DEFAULT;
	D3DDEVTYPE deviceType = D3DDEVTYPE_HAL;

	// Look for NVIDIA PerfHUD adapter in debug builds
	#ifdef _MAG_DEBUG
	for (uint16_t i = 0; i < pDirect3D->GetAdapterCount(); i++)
	{
		D3DADAPTER_IDENTIFIER9 identifier;
		HR(pDirect3D->GetAdapterIdentifier(i, 0, &identifier));
		if (std::strstr(identifier.Description, "PerfHUD") != 0)
		{
			adapter = i;
			deviceType = D3DDEVTYPE_REF;
			break;
		}
	}
	#endif

	// Create device
	HR(pDirect3D->CreateDevice(adapter, deviceType, pApplication->getWindowHandle(), 
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_presentParameters, &m_pDevice));

	// Get device capabilities again, this time after initialisation 
	HR(m_pDevice->GetDeviceCaps(&m_deviceCaps));

	LOG_DEBUG("Direct3D 9 device successfully created");

	// SpriteAsset helper class
	HR(D3DXCreateSprite(m_pDevice, &m_pD3DXSprite));

	// Release D3D9 interface now everything is created
	pDirect3D->Release();

	// Check for vertex shader version 2.0 support
	LOG_FATAL_IF(m_deviceCaps.VertexShaderVersion < D3DVS_VERSION(2, 0), 
		"Vertex Shader 2.0 not supported by video card");

	// Check for pixel shader version 2.0 support
	LOG_FATAL_IF(m_deviceCaps.PixelShaderVersion < D3DPS_VERSION(2, 0), 
		"Pixel Shader 2.0 not supported by video card");

	// For each vertex usage pre-computer declaration
	D3DXHANDLE hTech;
	EffectParameters ep;
	for (int i = 0; i < VU_COUNT; i++)
	{
		VertexUsageEnum usage = (VertexUsageEnum)i;

		// Convert vertex formats to D3D vertex declarations
		m_vertexFormats[usage] = Magnet::buildVertexFormat(usage);
		IDirect3DVertexDeclaration9* decl = this->buildVertexDeclaration(m_vertexFormats[usage]);
		m_vertexDeclarations.insert(std::pair<VertexUsageEnum, IDirect3DVertexDeclaration9*>(usage,  decl));

		// Create corresponding HLSL effects
		m_effects[i] = this->createEffect(usage);

		// Empty temporary effect parameters
		ZeroMemory(&ep, sizeof(ep));

		// Get handles for transforms (by semantics) if they exist
		ep.world                 = m_effects[i]->GetParameterBySemantic(0, "World");
		ep.worldViewProjection   = m_effects[i]->GetParameterBySemantic(0, "WorldViewProjection");
		ep.worldInverseTranspose = m_effects[i]->GetParameterBySemantic(0, "WorldInverseTranspose");

		// Get handles for lights if they exist
		ep.ambientLight  = m_effects[i]->GetParameterByName(0, "g_ambientLight");
		ep.diffuseLight  = m_effects[i]->GetParameterByName(0, "g_diffuseLight");
		ep.specularLight = m_effects[i]->GetParameterByName(0, "g_specularLight");

		// Get handles for materials if they exist
		ep.ambientMaterial  = m_effects[i]->GetParameterByName(0, "g_ambientMaterial");
		ep.diffuseMaterial  = m_effects[i]->GetParameterByName(0, "g_diffuseMaterial");
		ep.specularMaterial = m_effects[i]->GetParameterByName(0, "g_specularMaterial");

		// Get other handles
		ep.specularPower  = m_effects[i]->GetParameterByName(0, "g_specularPower");
		ep.lightDirection = m_effects[i]->GetParameterByName(0, "g_lightDirectionW");
		ep.eyePos         = m_effects[i]->GetParameterByName(0, "g_eyePosW");

		// Create effect parameter struct
		m_effectParameters.insert(std::make_pair(i, ep));

		// Get first valid technique
		HR(m_effects[i]->FindNextValidTechnique(NULL, &hTech));

		// Map effect technique handle
		m_effectTechniques.insert(std::make_pair(usage, hTech));
	}
}

void Magnet::D3D9Renderer::shutdown()
{
	// Release vertex declarations
	for (auto itr = m_vertexDeclarations.begin(); itr != m_vertexDeclarations.end(); ++itr)
		SAFE_COM_RELEASE(itr->second);
	m_vertexDeclarations.clear();

	// Release diffuse maps
	for (auto itr = m_diffuseMaps.begin(); itr != m_diffuseMaps.end(); ++itr)
		SAFE_COM_RELEASE(itr->second);
	m_diffuseMaps.clear();

	// Release effects
	for (auto itr = m_effects.begin(); itr !=  m_effects.end(); ++itr)
		SAFE_COM_RELEASE(*itr);

	// Release sprite helper
	SAFE_COM_RELEASE(m_pD3DXSprite);

	// Release root D3D9 device
	SAFE_COM_RELEASE(m_pDevice);

	LOG_DEBUG("Direct3D 9 device released");
}

IDirect3DVertexDeclaration9* Magnet::D3D9Renderer::buildVertexDeclaration(const Magnet::VertexFormat& fmt) const
{
	ASSERT(!fmt.nodes.empty());

	D3DVERTEXELEMENT9* elements = new D3DVERTEXELEMENT9[fmt.nodes.size() + 1];

	// For each format node construct native format
	int i = 0;
	for (auto itr = fmt.nodes.begin(); itr != fmt.nodes.end(); ++itr, i++)
	{
		elements[i].Stream     = 0;
		elements[i].Offset     = itr->offset;
		elements[i].Method     = D3DDECLMETHOD_DEFAULT;

		switch (itr->attribute)
		{

		// Position component
		case VA_POSITION:
			elements[i].Type       = D3DDECLTYPE_FLOAT3;
			elements[i].Usage      = D3DDECLUSAGE_POSITION;
			elements[i].UsageIndex = 0;
			break;

		// Normal component
		case VA_NORMAL:
			elements[i].Type	   = D3DDECLTYPE_FLOAT3;
			elements[i].Usage      = D3DDECLUSAGE_NORMAL;
			elements[i].UsageIndex = 0;
			break;

		// Diffuse component
		case VA_DIFFUSE:
			elements[i].Type       = D3DDECLTYPE_D3DCOLOR;
			elements[i].Usage      = D3DDECLUSAGE_COLOR;
			elements[i].UsageIndex = 0; // Diffuse
			break;

		// Specular component
		case VA_SPECULAR:
			elements[i].Type	   = D3DDECLTYPE_D3DCOLOR;
			elements[i].Usage	   = D3DDECLUSAGE_COLOR;
			elements[i].UsageIndex = 1; // Specular
			break;

		// Unsupported component
		default: LOG_ERROR("Unsupported vertex component");

		};
	}

	// End declarator macro
	D3DVERTEXELEMENT9 end = D3DDECL_END();
	elements[i] = end;

	// Get native declaration
	IDirect3DVertexDeclaration9* pDecl = NULL;
	HR(m_pDevice->CreateVertexDeclaration(elements, &pDecl));

	// Finished with elements array
	delete[] elements;

	// Return new declaration
	return pDecl;
}

ID3DXEffect* Magnet::D3D9Renderer::createEffect(const VertexUsageEnum usage) const
{
	// Number of FX files
	static const int FX_COUNT = 2;
		
	// Ensure, at compile time, there is an effect file for every vertex usage
	static_assert(FX_COUNT == VU_COUNT, "Vertex usage count not equal to FX file count");

	// Define all shader files, one for each Vertex Usage
	static char* fxFiles[FX_COUNT] = { 
		"PositionOnly.fx",    // VU_POSITION_ONLY
		"UntexturedPlain.fx", // VU_UNTEXTURED_PLAIN
	};

	// Setup shader options
	DWORD shaderFlags = 0;
	#ifndef _MAG_NO_IMPORT
	shaderFlags |= D3DXSHADER_DEBUG;
	shaderFlags |= D3DXSHADER_SKIPOPTIMIZATION;
	#else
	shaderFlags |= D3DXSHADER_SKIPVALIDATION;
	#endif

	// Do compilation and create effect
	static ID3DXEffect *pEffect = nullptr;
	static ID3DXBuffer *pErrors = nullptr;
	HR(D3DXCreateEffectFromFile(m_pDevice, (m_shaderPath + "\\hlsl\\" + fxFiles[(int)usage]).c_str(), 0, 0,
		shaderFlags, 0, &pEffect, &pErrors));

	// Display compilation errors if there are any
	if (pErrors != nullptr)
	{
		std::stringstream ss;
		ss << "HLSL Shader compilation error(s): " << std::endl << (char*)pErrors->GetBufferPointer();
		pErrors->Release();
		throw LogicError(ss.str().c_str());
	}

	// Return created effect
	return pEffect;
}

void Magnet::D3D9Renderer::createDiffuseMap(const std::string& id, const TextureFileSerializer& data)
{
	ASSERT(m_diffuseMaps.find(id) == m_diffuseMaps.end());

	// Why does this work?! DirectX is ARGB and Magnet is RGBA...??!

	// Create native texture object
	IDirect3DTexture9* pNativeTexture = nullptr;
	HR(D3DXCreateTexture(m_pDevice, data.getWidth(), data.getHeight(), D3DX_DEFAULT, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &pNativeTexture));

	// Copy pixel data across
	D3DLOCKED_RECT rect;
	HR(pNativeTexture->LockRect(0, &rect, nullptr, 0));
	std::memcpy(rect.pBits, data.getPixelData(), data.getWidth() * data.getHeight() * 4);
	HR(pNativeTexture->UnlockRect(0));

	// Add loaded texture to member map
	m_diffuseMaps.insert(std::make_pair(id, pNativeTexture));
}

void Magnet::D3D9Renderer::releaseDiffuseMap(const std::string& id)
{
	ASSERT(m_diffuseMaps.find(id) != m_diffuseMaps.end());

	// Get iterator to texture
	auto itr = m_diffuseMaps.find(id);

	// Release COM object
	SAFE_COM_RELEASE(itr->second);

	// Remove from member map
	m_diffuseMaps.erase(itr);
}

void Magnet::D3D9Renderer::render()
{
	HRESULT hr = m_pDevice->TestCooperativeLevel();

	// Device is lost and unrecoverable at this time
	if (hr == D3DERR_DEVICELOST)
	{
		Sleep(20);
		return;
	}
	
	// Driver error
	else if (hr == D3DERR_DRIVERINTERNALERROR)
		throw RuntimeError("Internal Direct3D 9 driver error");

	// Device is lost and can be reset
	else if (hr == D3DERR_DEVICENOTRESET)
		this->reset();

	m_pDevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);
	m_pDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
    m_pDevice->BeginScene();

	// For each viewport
	for (int i = 0; i < g_pRenderer->getAllViewPorts().getSize(); i++)
	{
		// Dereference viewport
		Magnet::ViewPort* pViewPort = g_pRenderer->getAllViewPorts()[i];

		// Get projection matrix for viewport
		D3DXMATRIX projection = *(D3DXMATRIX*)pViewPort->getProjectionTransform(PT_PERSPECTIVE_DIRECT3D).getDataPtr();

		// Get view matrix for viewport
		D3DXMATRIX view = *(D3DXMATRIX*)pViewPort->getCamera()->getViewTransform().getDataPtr();

		// Get mesh instance components
		std::vector<Entity*> actorInstances = g_pGameWorld->getEntitiesOfType(GO_ACTOR);

		// Get light components
		std::vector<Entity*> ambientLights  = g_pGameWorld->getEntitiesOfType(GO_AMBIENT_LIGHT);
		std::vector<Entity*> directionalLights  = g_pGameWorld->getEntitiesOfType(GO_DIRECTIONAL_LIGHT);

		// For each actor instance
		BOOST_FOREACH(Entity* pEntity, actorInstances)
		{
			// Down cast actor instance
			ActorEntity* pActorInstance = static_cast<ActorEntity*>(pEntity);

			// Get actor and model
			ActorAsset* pActor = pActorInstance->getAsset();
			ModelFile* pModel = pActor->getModel();

			// For each mesh in the model
			BOOST_FOREACH(AbstractMesh* pMesh, pModel->getMeshes())
			{
				// Get vertex usage
				VertexUsageEnum usage = pMesh->getVertexUsage();

				// Get vertex size (stride)
				size_t stride = pMesh->getVertexSize();

				// Get pre-computed native vertex declaration
				IDirect3DVertexDeclaration9* pVertexDeclaration = m_vertexDeclarations[usage];

				// Get mesh properties
				int numVertices  = pMesh->getNumVertices();
				int numIndices   = pMesh->getNumIndices();
				int numTriangles = pMesh->getNumTriangles();

				// Get vertex and index pointers
				void* pVertices = pMesh->getVerticesPtr();
				void* pIndices  = pMesh->getIndicesPtr();

				// Create vertex buffer
				IDirect3DVertexBuffer9* pVertexBuffer = nullptr;
				HR(m_pDevice->CreateVertexBuffer(numVertices * stride, D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &pVertexBuffer, 0));

				// Lock the vertex buffer
				void* pVertexData = nullptr;
				HR(pVertexBuffer->Lock(0, 0, &pVertexData, 0));
				std::memcpy(pVertexData, pVertices, numVertices * stride);
				HR(pVertexBuffer->Unlock());

				// Create index buffer and copy index data
				IDirect3DIndexBuffer9* pIndexBuffer = nullptr;
				HR(m_pDevice->CreateIndexBuffer(numIndices * sizeof(uint16_t), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &pIndexBuffer, 0)); 

				// Copy index data
				void* pIndexData = nullptr;
				HR(pIndexBuffer->Lock(0, 0, &pIndexData, 0));
				std::memcpy(pIndexData, pIndices, numIndices * sizeof(uint16_t));
				HR(pIndexBuffer->Unlock());

				// Setup vertex and index buffer
				HR(m_pDevice->SetStreamSource(0, pVertexBuffer, 0, stride));
				HR(m_pDevice->SetIndices(pIndexBuffer));
				HR(m_pDevice->SetVertexDeclaration(pVertexDeclaration));

				// Get effect for vertex usage
				ID3DXEffect* pEffect = m_effects[(uint16_t)usage];

				// Set the technique
				pEffect->SetTechnique(m_effectTechniques[(uint16_t)usage]);

				// Get effect parameters struct
				EffectParameters* pEffectParameters = &m_effectParameters[(uint16_t)usage];

				// Set world matrix
				D3DXMATRIX world = *(D3DXMATRIX*)pActorInstance->getWorldTransform().getDataPtr();
				if (pEffectParameters->world)
					pEffect->SetMatrix(pEffectParameters->world, &world);

				// Set world view matrix
				D3DXMATRIX worldViewProjection = world * view * projection;
				if (pEffectParameters->worldViewProjection)
					pEffect->SetMatrix(pEffectParameters->worldViewProjection, &worldViewProjection);
				
				// Set world transpose matrix
				if (pEffectParameters->worldInverseTranspose)
				{
					D3DXMATRIX worldInverseTranspose;
					D3DXMatrixInverse(&worldInverseTranspose, 0, &world);
					D3DXMatrixTranspose(&worldInverseTranspose, &worldInverseTranspose);
					pEffect->SetMatrix(pEffectParameters->worldInverseTranspose, &worldInverseTranspose);
				}

				// Set ambient settings
				// TODO: Support multiple ambient lights
				if (!ambientLights.empty())
				{
					AmbientLightEntity* pAmbientLight = static_cast<AmbientLightEntity*>(ambientLights[0]);

					if (pEffectParameters->ambientMaterial)
						pEffect->SetValue(pEffectParameters->ambientMaterial, (float*)&pMesh->getMaterial(0).ambient, 4 * sizeof(float));

					if ((pEffectParameters->ambientLight) && (pAmbientLight->on))
						pEffect->SetValue(pEffectParameters->ambientLight, (float*)&pAmbientLight->color, 4 * sizeof(float));
				}

				// Set directional settings
				// TODO: Support multiple directional lights
				if (!directionalLights.empty())
				{
					DirectionalLightEntity* pDirectionalLight = static_cast<DirectionalLightEntity*>(directionalLights[0]);

					if (pEffectParameters->diffuseMaterial)
						pEffect->SetValue(pEffectParameters->diffuseMaterial, (float*)&pMesh->getMaterial(0).diffuse, 4 * sizeof(float));

					if ((pEffectParameters->diffuseLight) && (pDirectionalLight->on))
						pEffect->SetValue(pEffectParameters->diffuseLight, (float*)&pDirectionalLight->diffuse, 4 * sizeof(float));

					if (pEffectParameters->specularMaterial)
						pEffect->SetValue(pEffectParameters->specularMaterial, (float*)&pMesh->getMaterial(0).specular, 4 * sizeof(float));

					if ((pEffectParameters->specularLight) && (pDirectionalLight->on))
						pEffect->SetValue(pEffectParameters->specularLight, (float*)&pDirectionalLight->specular, 4 * sizeof(float));

					if ((pEffectParameters->specularPower) && (pDirectionalLight->on))
						pEffect->SetFloat(pEffectParameters->specularPower,  pMesh->getMaterial(0).shininess);

					if ((pEffectParameters->lightDirection) && (pDirectionalLight->on))
						pEffect->SetValue(pEffectParameters->lightDirection, (float*)&pDirectionalLight->direction, 3 * sizeof(float));

					if (pEffectParameters->eyePos)
						pEffect->SetValue(pEffectParameters->eyePos, (float*)&pViewPort->getCamera()->getPosition(), 3 * sizeof(float));
				}

				// Commit effect parameter changes
				HR(pEffect->CommitChanges());

				// Draw effect passes
				UINT numPasses = 0;
				HR(pEffect->Begin(&numPasses, 0));
				for (UINT i = 0; i < numPasses; ++i)
				{
					HR(pEffect->BeginPass(i));
					HR(m_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, numVertices, 0, numTriangles));
					HR(pEffect->EndPass());
				}
				HR(pEffect->End());

				// Release buffers
				pVertexBuffer->Release();
				pIndexBuffer->Release();
			}
		}

		// Start drawing sprites
		m_pD3DXSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_DEPTH_BACKTOFRONT | D3DXSPRITE_DO_NOT_ADDREF_TEXTURE);

		// TODO: Optimise the loops below by sing g_pGameWorld->getComponents() directly (like actors above)

		RECT rect;

		// Get world objects that have sprite instances
		std::vector<Entity*> spriteObjects = g_pGameWorld->getEntitiesOfType(GO_SPRITE);

		// For each sprite instance
		BOOST_FOREACH(Entity* pEntity, spriteObjects)
		{

			// Down-cast sprite instance
			SpriteEntity* pInstance = static_cast<SpriteEntity*>(pEntity);

			if (pInstance->isVisible())
			{
				SpriteAsset* pSprite   = pInstance->getSprite();
				Rectangle& frame  = pInstance->getCurrentFrame();
				Vector2& position = pInstance->getPosition();

				// Get diffuse map ID
				std::string diffuseMapId = pSprite->getDiffuseMap()->getId();

				// Get native texture
				ASSERT(m_diffuseMaps.find(diffuseMapId) != m_diffuseMaps.end());
				IDirect3DTexture9* pNativeTexture = m_diffuseMaps[diffuseMapId];

				// Draw sprite using D3DX
				SetRect(&rect, frame.left, frame.top, frame.right, frame.bottom);
				D3DXVECTOR3 pos(position.x, position.y, 0);
				m_pD3DXSprite->Draw(pNativeTexture, &rect, nullptr, &pos, D3DCOLOR_ARGB(255,255,255,255));
			}
		}

		// End drawing sprites
		m_pD3DXSprite->End();
	}
	
    m_pDevice->EndScene();

	// Present the back buffer (swap buffers)
    m_pDevice->Present(NULL, NULL, NULL, NULL);
}

void Magnet::D3D9Renderer::reshape()
{
	Win32Application* pApplication = dynamic_cast<Win32Application*>(g_pApplication);

	// Change size of back format
	if (pApplication != nullptr)
	{
		if (pApplication->isFullscreen())
		{
			m_presentParameters.Windowed = FALSE;
			m_presentParameters.BackBufferFormat = D3DFMT_X8R8G8B8;
		}
		else
		{
			m_presentParameters.Windowed = TRUE;
			m_presentParameters.BackBufferFormat = D3DFMT_UNKNOWN;
		}
	}

	// Change back buffer size
	m_presentParameters.BackBufferWidth  = pApplication->getPixelWidth();
	m_presentParameters.BackBufferHeight = pApplication->getPixelHeight();

	// Reset to apply changes
	this->reset();
}

void Magnet::D3D9Renderer::reset()
{	
	// Prepare sprite and font device for reset
	HR(m_pD3DXSprite->OnLostDevice());

	// Prepare shaders for reset
	for (auto itr = m_effects.begin(); itr != m_effects.end(); ++itr)
		(*itr)->OnLostDevice();

	// Do reset
	HR(m_pDevice->Reset(&m_presentParameters));

	// Restart sprite and font devices
	HR(m_pD3DXSprite->OnResetDevice());

	// Restart shaders
	for (auto itr = m_effects.begin(); itr != m_effects.end(); ++itr)
		(*itr)->OnResetDevice();
}

#endif