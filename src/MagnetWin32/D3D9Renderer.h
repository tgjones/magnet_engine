
#ifndef _MAGNET_WIN32_D3D9_RENDERER_H
#define _MAGNET_WIN32_D3D9_RENDERER_H
#ifdef _MAG_USE_D3D

#include <MagnetWin32/common.h>

namespace Magnet
{
	class D3D9Renderer : public Renderer
	{
	public:

		inline virtual ~D3D9Renderer() {}

		void startup();
		void shutdown();

		void render();

		void reshape();

		void reset();

		void createDiffuseMap(const std::string& id, const TextureFileSerializer& data);

		void releaseDiffuseMap(const std::string& id);

		inline IDirect3DDevice9* getNativeDevice() { return m_pDevice; }

	protected:

		// Constructor
		inline D3D9Renderer() :
			m_pDevice(nullptr),
			m_pD3DXSprite(nullptr)
		{
			for (int i = 0; i < VU_COUNT; i++) m_effects[i] = nullptr;
			ZeroMemory(&m_presentParameters, sizeof(D3DPRESENT_PARAMETERS));
		}

		// Build a vertex declaration
		IDirect3DVertexDeclaration9* buildVertexDeclaration(const VertexFormat& fmt) const;

		// Create HLSL effect
		ID3DXEffect* createEffect(const VertexUsageEnum usage) const;
		
	private:

		friend class SubsystemFactory;

		// Main Direc3D 9 device
		IDirect3DDevice9* m_pDevice;

		// SpriteAsset helper
		ID3DXSprite* m_pD3DXSprite;

		// D3D present parameters
		D3DPRESENT_PARAMETERS m_presentParameters;

		// Device capabilities
		D3DCAPS9 m_deviceCaps;

		// Vertex formats (Magnet native)
		std::tr1::unordered_map<uint16_t, VertexFormat> m_vertexFormats;

		// Vertex declarations (Direct3D9 native)
		std::tr1::unordered_map<uint16_t, IDirect3DVertexDeclaration9*> m_vertexDeclarations;

		// Currently loaded diffuse maps
		std::tr1::unordered_map<std::string, IDirect3DTexture9*> m_diffuseMaps;

		// HLSL effects
		std::tr1::array<ID3DXEffect*, VU_COUNT> m_effects;

		// Universal shader parameters struct
		struct EffectParameters
		{
			D3DXHANDLE world;
			D3DXHANDLE worldViewProjection;
			D3DXHANDLE worldInverseTranspose;
			D3DXHANDLE ambientMaterial;
			D3DXHANDLE diffuseMaterial;
			D3DXHANDLE specularMaterial;
			D3DXHANDLE ambientLight;
			D3DXHANDLE diffuseLight;
			D3DXHANDLE specularLight;
			D3DXHANDLE specularPower;
			D3DXHANDLE lightDirection;
			D3DXHANDLE eyePos;
		};

		// Effect parameters
		std::tr1::unordered_map<uint16_t, EffectParameters> m_effectParameters;

		// Effect technique handles
		std::tr1::unordered_map<uint16_t, D3DXHANDLE> m_effectTechniques;
	};
}

#endif
#endif