#ifndef _MAGNET_WIN32_COMMON_H
#define _MAGNET_WIN32_COMMON_H

#include <MagnetUtil/config.h>

// Include core common headers
#include <MagnetUtil/common.h>

// Win32 external headers and lib files
#include <MagnetWin32/external.h>
#include <MagnetWin32/libs.h>

// Win32 class implementations
#include <MagnetWin32/Win32Application.h>
#include <MagnetWin32/Win32OpenGLRenderer.h>
#include <MagnetWin32/Win32InputEngine.h>
#include <MagnetWin32/D3D9Renderer.h>

#endif