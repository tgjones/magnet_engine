#ifndef _MAGNET_WIN32_WIN32_APPLICATION_H
#define _MAGNET_WIN32_WIN32_APPLICATION_H

#include <MagnetWin32/common.h>

namespace Magnet 
{
	enum Win32RendererType
	{
		WIN32_RENDERER_UNSPECIFIED,
		WIN32_RENDERER_D3D9,
		WIN32_RENDERER_OPENGL
	};
	
	class Win32Application : public Application
	{
	public:

		inline void setActive(bool active) { m_active = active; }

		inline bool isActive() { return m_active; }

		inline void setTitle(const std::string& title) { m_title = title; }

		std::string getTitle() { return m_title; }

		uint16_t getPixelWidth();

		uint16_t getPixelHeight();

		fs::path getResourceRoot();

		/// @todo Look into AdjustWindowRectEx (&windowRect, windowStyle, 0, windowExtendedStyle)

		void setFullscreen(bool fullscreen);

		inline void toggleFullscreen() { this->setFullscreen(!m_fullscreen); }

		inline bool isFullscreen() { return m_fullscreen; }

		void setSize(uint16_t width, uint16_t height);

		inline HINSTANCE getInstanceHandle() { return m_hInstance; }

		inline HWND getWindowHandle() { return m_hWnd; }

		inline HDC getDeviceContextHandle() { return m_hDc; }

		inline Win32RendererType getRendererType() { return m_rendererType; }

		void setRenderer(Win32RendererType rendererType);

		double getAbsoluteTime();

	protected:
	
		friend int WINAPI ::WinMain(HINSTANCE, HINSTANCE, LPSTR, int);

		// Protected so only WinMain can instantiate
		inline Win32Application() : m_timeMultiplier(0.0), m_active(true), m_quit(false), m_quitCode(0), m_hInstance(nullptr), m_hWnd(NULL), 
			m_hDc(NULL), m_rendererType(WIN32_RENDERER_UNSPECIFIED) {}

		void startup();

		void shutdown();
	
		int run();

		void quit(int code = 0);

	private:

		bool m_active;

		std::string m_title;

		bool m_quit;

		int m_quitCode;

		Win32RendererType m_rendererType;
	
		HINSTANCE m_hInstance;

		HWND m_hWnd;

		HDC m_hDc;

		bool m_fullscreen;

		uint16_t m_windowedWidth;

		uint16_t m_windowedHeight;

		uint16_t m_fullscreenWidth;

		uint16_t m_fullscreenHeight;

		double m_timeMultiplier;

		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	};
}

#endif