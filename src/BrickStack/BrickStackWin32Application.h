#ifndef _BRICK_STACK_WIN32_H
#define _BRICK_STACK_WIN32_H

#include <MagnetWin32/common.h>

class BrickStackWin32Application : public Magnet::Win32Application
{
public:

	virtual void onPostEngineStartup();
};

#endif