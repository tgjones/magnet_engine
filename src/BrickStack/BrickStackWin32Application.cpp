#include "BrickStackWin32Application.h"
#include "BrickEntity.h"

#define _MAG_APP_CLASS BrickStackWin32Application
#include <MagnetUtil/main.h>

using namespace Magnet;

void BrickStackWin32Application::onPostEngineStartup()
{
	// Setup ambient light to game world
	AmbientLightEntity* pAmbientLight = new AmbientLightEntity();
	pAmbientLight->color.r = 0.1f;
	pAmbientLight->color.g = 0.1f;
	pAmbientLight->color.b = 0.1f;
	g_pGameWorld->spawnEntity(pAmbientLight);

	// Add directional light to game world
	DirectionalLightEntity* pDirectionalLight = new DirectionalLightEntity();
	pDirectionalLight->direction.x = 0.0f;
	pDirectionalLight->direction.y = 0.0f;
	pDirectionalLight->direction.z = -1.0f;
	pDirectionalLight->diffuse.r = 0.5f;
	pDirectionalLight->diffuse.g = 0.5f;
	pDirectionalLight->diffuse.b = 0.5f;
	pDirectionalLight->specular.r = 0.2f;
	pDirectionalLight->specular.g = 0.2f;
	pDirectionalLight->specular.b = 0.8f;
	g_pGameWorld->spawnEntity(pDirectionalLight);

	// Add a simple ground plane to physics world
	Plane plane;
	plane.setDistance(-3.1f);
	g_pPhysicsEngine->createPlane(plane); 

	// Create bricks
	static const uint16_t NUM_BRICKS = 15;
	BrickEntity* bricks[NUM_BRICKS];
	for (uint16_t i = 0; i < NUM_BRICKS; i++)
	{
		// Create brick
		bricks[i] = new BrickEntity();

		// Scale down the brick a bit
		bricks[i]->getOffset() *= Matrix4x4::createScale(0.5f, 0.5f, 0.5f);
	}
	 
	// Position bricks
	bricks[0]->getInitPose() *= Matrix4x4::createTranslation(-5.0f, 0.0f, 0.0f);
	bricks[1]->getInitPose() *= Matrix4x4::createTranslation(-3.0f, 0.0f, 0.0f);
	bricks[2]->getInitPose() *= Matrix4x4::createTranslation(-1.0f, 0.0f, 0.0f);
	bricks[3]->getInitPose() *= Matrix4x4::createTranslation(1.0f, 0.0f, 0.0f);
	bricks[4]->getInitPose() *= Matrix4x4::createTranslation(3.0f, 0.0f, 0.0f);

	bricks[5]->getInitPose() *= Matrix4x4::createTranslation(-4.0f, 3.0f, 0.0f);
	bricks[6]->getInitPose() *= Matrix4x4::createTranslation(-2.0f, 3.0f, 0.0f);
	bricks[7]->getInitPose() *= Matrix4x4::createTranslation(0.0f, 3.0f, 0.0f);
	bricks[8]->getInitPose() *= Matrix4x4::createTranslation(2.0f, 3.0f, 0.0f);
	
	bricks[9]->getInitPose() *= Matrix4x4::createTranslation(-3.0f, 6.0f, 0.0f);
	bricks[10]->getInitPose() *= Matrix4x4::createTranslation(-1.0f, 6.0f, 0.0f);
	bricks[11]->getInitPose() *= Matrix4x4::createTranslation(1.0f, 6.0f, 0.0f);
	
	bricks[12]->getInitPose() *= Matrix4x4::createTranslation(-2.0f, 9.0f, 0.0f);
	bricks[13]->getInitPose() *= Matrix4x4::createTranslation(0.0f, 9.0f, 0.0f);
	
	bricks[14]->getInitPose() *= Matrix4x4::createTranslation(2.0f, 9.0f, 0.0f);

	// Add bricks to game world
	for (uint16_t i = 0; i < NUM_BRICKS; i++)
		g_pGameWorld->spawnEntity(bricks[i]);
}