#include <MagnetUtil/common.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#ifdef _MAG_PLATFORM_WIN32
#include <MagnetWin32/libs.h>
#endif

#define MCOOK_VERSION "0.3.1"

namespace fs = boost::filesystem;
namespace po = boost::program_options;

void convertModel(const fs::path& in, Magnet::ModelImportOptions& options)
{
	// MMF already converted
	ASSERT(in.extension() != Magnet::ModelFileSerializer::FILE_EXT);

	// Generate new file name
	fs::path out(in);
	out.replace_extension(Magnet::ModelFileSerializer::FILE_EXT);

	// Progress message
	std::cout << "[INFO] Converting model file " << in << " to " << out << "..." << std::endl;
		
	Magnet::Timer t;
	Magnet::ModelFileSerializer interim;

	// Convert formats (timed)
	t.start();
	interim.import(in, options); 
	interim.write(out);
	t.stop();

	// Progress message
	std::cout << "[INFO] Conversion completed in approximately " << t.last() << " seconds" << std::endl;
}

void convertTexture(const fs::path& in, const Magnet::TextureUsage usage, Magnet::TextureImportOptions& options)
{
	// MTF already converted
	ASSERT(in.extension() != Magnet::TextureFileSerializer::FILE_EXT);

	// Generate new file name
	fs::path out(in);
	out.replace_extension(Magnet::TextureFileSerializer::FILE_EXT);

	// Progress message
	std::cout << "[INFO] Converting texture file " << in << " to " << out << "..." << std::endl;

	Magnet::Timer t;
	Magnet::TextureFileSerializer interim;

	// Convert formats (timed)
	t.start();
	interim.import(in, usage, options);
	interim.write(out);
	t.stop();

	// Progress message
	std::cout << "[INFO] Conversion completed in approximately " << t.last() << " seconds" << std::endl;
}

void processBatch(const fs::path& batchFile)
{
	// Load document
	TiXmlDocument doc(batchFile.string().c_str());
	if (doc.LoadFile())
	{
		TiXmlElement *pRoot = doc.FirstChildElement("magnet:mcook");
		if (!pRoot)	throw Magnet::LogicError("Cannot find root element in '" + batchFile.string() + "'");

		// Textures
		for (TiXmlElement* pTextureElement = pRoot->FirstChildElement("texture"); pTextureElement != nullptr;
			pTextureElement = pTextureElement->NextSiblingElement("texture"))
		{
			Magnet::TextureImportOptions options;

			// Input file path
			fs::path path;			
			TiXmlElement* pInputFileElement  = pTextureElement->FirstChildElement("path");
			if (pInputFileElement)
				path = pInputFileElement->GetText();
			else
				throw Magnet::LogicError("Mandatory texture input file tag not found in XML batch");

			// TextureFile usage
			Magnet::TextureUsage usage = Magnet::TU_UNSPECIFIED;
			TiXmlElement* pTextureUsageElement = pTextureElement->FirstChildElement("usage");
			std::string usageStr = pTextureUsageElement->GetText();
			if (usageStr == "DIFFUSE_MAP")
				usage = Magnet::TU_DIFFUSE_MAP;

			// Import options
			Magnet::TextureImportOptions importOptions;
			TiXmlElement *pImportOptionsElement = pTextureElement->FirstChildElement("inputOptions");
			if (pImportOptionsElement && (path.extension() != Magnet::TextureFileSerializer::FILE_EXT))
			{
				// Color key
				TiXmlElement* pColorKeyElement = pTextureElement->FirstChildElement("colorKey");
				if (pColorKeyElement != nullptr)
				{
					// Make sure usage allows color key
					if (usage == Magnet::TU_DIFFUSE_MAP)
					{
						options.flags |= Magnet::TIF_USE_COLOUR_KEY;
						options.colorKey = pColorKeyElement->GetText();
						options.colorKey.a = 0x00;
					}

					// Color key not supported by usage
					else throw Magnet::LogicError("TextureFile usage does not support a color key");
				}
			}
			else if (pImportOptionsElement)
				throw Magnet::LogicError("Import options are not supported for Magnet native file formats");

			// Do conversion (with default import options, TODO: construct options from batch file)
			convertTexture(path, usage, importOptions);
		}
		
		// Models
		for (TiXmlElement* pModelElement = pRoot->FirstChildElement("model"); pModelElement != nullptr;
			pModelElement = pModelElement->NextSiblingElement("model"))
		{
			// Input file path
			fs::path path;			
			TiXmlElement* pInputFileElement  = pModelElement->FirstChildElement("path");
			if (pInputFileElement)
				path = pInputFileElement->GetText();
			else
				throw Magnet::LogicError("Mandatory model input file tag not found in XML batch");

			// Import options
			Magnet::ModelImportOptions importOptions;
			TiXmlElement *pImportOptionsElement = pModelElement->FirstChildElement("importOptions");
			if (pImportOptionsElement && (path.extension() != Magnet::ModelFileSerializer::FILE_EXT))
			{
				// Generate normals
				TiXmlElement *pGenerateNormalsElement = pImportOptionsElement->FirstChildElement("generateNormals");
				if (pGenerateNormalsElement)
				{
					if (std::string(pGenerateNormalsElement->GetText()) == "FACE_NORMALS")
						importOptions.flags |= Magnet::MIF_FACE_NORMALS;
					else if (std::string(pGenerateNormalsElement->GetText()) == "SMOOTH_NORMALS")
						importOptions.flags |= Magnet::MIF_SMOOTH_NORMALS;
					else
						throw Magnet::LogicError("Unsupported generate normals enumeration '" + 
							std::string(pGenerateNormalsElement->GetText()) + "'");
				}

				// Override normals
				TiXmlElement *pOverrideNormalsElement = pImportOptionsElement->FirstChildElement("overrideNormals");
				if (pOverrideNormalsElement)
					if ((std::string(pOverrideNormalsElement->GetText()) == "true") || 
						(std::string(pOverrideNormalsElement->GetText()) == "1"))
						importOptions.flags |= Magnet::MIF_OVERRIDE_NORMALS;
					else if ((std::string(pOverrideNormalsElement->GetText()) != "false") || 
						(std::string(pOverrideNormalsElement->GetText()) != "0"))
						throw Magnet::LogicError("Problem reading override normals switch, please specify either 'true' or 'false'");

				// Hard Edge Threshold
				TiXmlElement *pHardEdgeThreshElement = pImportOptionsElement->FirstChildElement("hardEdgeThresh");
				if (pHardEdgeThreshElement)
					try
					{
						importOptions.hardEdgeThresh = boost::lexical_cast<float, const char*>(pHardEdgeThreshElement->GetText());
					}
					catch (boost::bad_lexical_cast&)
					{
						throw Magnet::LogicError("Problem reading hard edge threshold, please specify a valid floating point number");
					}
				
				// Generate bounding boxes
				TiXmlElement *pGenerateBoundingBoxes = pImportOptionsElement->FirstChildElement("generateBoundingBoxes");
				if (pGenerateBoundingBoxes)
					if ((std::string(pGenerateBoundingBoxes->GetText()) == "true") || 
						(std::string(pGenerateBoundingBoxes->GetText()) == "1"))
						importOptions.flags |= Magnet::MIF_BOUNDING_BOXES;
					else if ((std::string(pGenerateBoundingBoxes->GetText()) != "false") &&
						(std::string(pGenerateBoundingBoxes->GetText()) != "0"))
						throw Magnet::LogicError("Problem reading generate bounding boxes switch, please specify either 'true' or 'false'");
			}
			else if (pImportOptionsElement)
				throw Magnet::LogicError("Import options are not supported for Magnet native file formats");

			// Do conversion (with default import options, TODO: construct options from batch file)
			convertModel(path, importOptions);
		}
	}

	// Problem loading XML file
	else throw Magnet::RuntimeError("Cannot open MCook batch file '" + batchFile.string() + "'");
}

int main(int argc, char* argv[])
{
	try
	{
		// Welcome message
		std::cout << "Magnet Media Cooker (MCook) v" << MCOOK_VERSION << std::endl;

		// Specify any non-named options to be input files
		po::positional_options_description p;
		p.add("input-file", -1);

		// Declare the supported options
		po::options_description desc("Allowed options");
		desc.add_options()
			("version,v", "print version string")
			("help,h", "produce help message")
			//("input-file,i", po::value<std::string>(), "input file (supported extensions: .png, .obj)")
			//("output-file,o", po::value<std::string>(), "output file (supported extensions: .magtex, .magmdl)")
			("batch-file,b", po::value<std::string>(), "ascii file containing files to convert")
		;

		// Parse command line
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
		po::notify(vm);

		// Do batch processing if specified
		if (vm.count("batch-file"))
		{
			// Ensure no other parameters were specified
			if (vm.size() != 1)
				throw Magnet::RuntimeError("If converting a batch then other arguments are not allowed");

			// Execute batch conversion
			processBatch(vm["batch-file"].as<std::string>());
		}
/*
		// Convert single file
		else if (vm.count("input-file"))
		{
			// If an output filename has been specified
			if (vm.count("output-file"))
				convert(vm["input-file"].as<std::string>(), vm["output-file"].as<std::string>());

			// If the default output file name is to be used
			else
				convert(vm["input-file"].as<std::string>());
		}

		// All other (erroneous) eventualities
		else
			throw Magnet::ArgumentException("[ERRO] You must specify at least an input file");
*/	}

	// Catch exceptions, print error and return non-zero
	catch (Magnet::Exception& e)
	{
		std::cerr << e.getReason() << std::endl;
		return 3;
	}
	// Return success
	return 0;
}