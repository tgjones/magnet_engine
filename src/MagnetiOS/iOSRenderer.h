#ifndef _MAGNET_IOS_IOS_RENDERER
#define _MAGNET_IOS_IOS_RENDERER

#include <MagnetiOS/common.h>

namespace Magnet
{
	class iOSRenderer : public OpenGLRenderer
	{
	public:
		
		void startup();
		
		void shutdown();
		
		void render();
		
		inline EAGLContext* getNativeContext() { return m_pContext; }
		
	protected:
				
		friend class SubsystemFactory;
		
		inline iOSRenderer() : m_pContext(nil) {}
		
		
	private:
		
		EAGLContext* m_pContext;

	};
}

#endif