#import <MagnetiOS/common.h>

void Magnet::iOSApplication::startup()
{
	// Register iOS subsystems
	REGISTER_SUBSYSTEM(ST_RENDERER, iOSRenderer);
	REGISTER_SUBSYSTEM(ST_INPUT_ENGINE, iOSInputEngine);
	
	// Create view controller
	m_pViewController = [[MagnetiOSViewController alloc] init];
}

void Magnet::iOSApplication::shutdown()
{
	// Release view controller
	[m_pViewController release];
}

fs::path Magnet::iOSApplication::getResourceRoot()
{
	// Get app bundle resource directory and check its an absolute path
	fs::path resourceRoot([[[NSBundle mainBundle] resourcePath] UTF8String]);
	ASSERT(resourceRoot.is_absolute());
	
	// Return path
	return resourceRoot;
}

double Magnet::iOSApplication::getAbsoluteTime()
{
	return (double)CACurrentMediaTime();
}

uint16_t Magnet::iOSApplication::getPixelWidth()
{
	if (UIInterfaceOrientationIsPortrait(m_pViewController.interfaceOrientation))
		return boost::numeric_cast<uint16_t>(CGRectGetWidth([UIScreen mainScreen].bounds));
	else // if (UIInterfaceOrientationIsLandscape(m_pViewController.interfaceOrientation))
		return boost::numeric_cast<uint16_t>(CGRectGetHeight([UIScreen mainScreen].bounds));
}

uint16_t Magnet::iOSApplication::getPixelHeight()
{
	if (UIInterfaceOrientationIsPortrait(m_pViewController.interfaceOrientation))
		return boost::numeric_cast<uint16_t>(CGRectGetHeight([UIScreen mainScreen].bounds));
	else // if (UIInterfaceOrientationIsLandscape(m_pViewController.interfaceOrientation))
		return boost::numeric_cast<uint16_t>(CGRectGetWidth([UIScreen mainScreen].bounds));
}