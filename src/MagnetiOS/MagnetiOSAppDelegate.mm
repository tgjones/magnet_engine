#import <MagnetiOS/common.h>

@implementation MagnetiOSAppDelegate

@synthesize vc, window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{	
	try 
	{
		// Startup Magnet
		Magnet::Engine::startup();
		
		// Down-cast and save iOSApplication pointer
		m_pApplication = static_cast<Magnet::iOSApplication*>(g_pApplication);
	}
	catch (Magnet::Exception &e)
	{
		// Get exception class name from RTTI and demangle
		int status;
		NSString* name = [NSString stringWithString:@"Unknown exception"];
		if (char* p = abi::__cxa_demangle(typeid(e).name(), NULL, NULL, &status))
		{
			name = [NSString stringWithCString:p encoding:NSUTF8StringEncoding];
			std::free(p);
		}
			
		// Output log message
		NSLog(@"Exception %@ caught: %@", name, [NSString stringWithCString:e.getReason().c_str() encoding:NSUTF8StringEncoding]);
		
		// Call termination delegate method and exit app with halt code
		[self applicationWillTerminate:(UIApplication *)application];
		std::exit(-1);
	}
	
	// Fade the status bar out of view
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];

	// Create, configure and show window
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[window setBackgroundColor:[UIColor blackColor]];
	[window setRootViewController:m_pApplication->getViewController()];
	[window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Shutdown Magnet
	Magnet::Engine::shutdown();
}

- (void)dealloc
{	
	// Cleanup system
	[window release];
	[super dealloc];
}

@end
