#ifndef _MAGNET_IOS_MAGNET_IOS_EAGL_VIEW_H
#define _MAGNET_IOS_MAGNET_IOS_EAGL_VIEW_H

#import <MagnetiOS/common.h>

// This class was taken pretty much verbatum from the OpenGL ES template sample generated
// by Xcode, EAGL by the way stands for Embedded Apple GL.


// This class wraps the CAEAGLLayer from CoreAnimation into a convenient UIView subclass.
// The view content is basically an EAGL surface you render your OpenGL scene into.
// Note that setting the view non-opaque will only work if the EAGL surface has an alpha channel.
@interface MagnetiOSView : UIView
{
@private
    
	EAGLContext *context;
	
    // Buffers
    GLuint defaultFramebuffer, depthRenderbuffer, colorRenderbuffer;
}

@property (nonatomic, retain) EAGLContext *context;

- (void)setFramebuffer;
- (void)resetFramebuffer;
- (BOOL)presentFramebuffer;

@end

#endif