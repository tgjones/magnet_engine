#ifndef _MAGNET_IOS_COMMON_H
#define _MAGENT_IOS_COMMON_H

#import <MagnetUtil/config.h>

// MagnetCore common C++ functionality
#import <MagnetUtil/common.h>

// External headers (precompiled header)
#import <MagnetiOS/external.h>

// iOS C++ headers
#import <MagnetiOS/iOSRenderer.h>
#import <MagnetiOS/iOSInputEngine.h>
#import <MagnetiOS/iOSApplication.h>

// Magnet specific Objective-C headers
#import <MagnetiOS/MagnetiOSView.h>
#import <MagnetiOS/MagnetiOSViewController.h>
#import <MagnetiOS/MagnetiOSAppDelegate.h>

#endif