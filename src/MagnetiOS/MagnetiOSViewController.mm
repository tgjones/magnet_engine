#import <MagnetiOS/common.h>

@implementation MagnetiOSViewController

@synthesize displayLink, frameInterval, currentOrientation;
@synthesize supportsOrientationPortrait, supportsOrientationPortraitUpsideDown;
@synthesize supportsOrientationLandscapeLeft, supportsOrientationLandscapeRight;

- (id)init
{
    [super init];
	if (self)
	{
		// Set only portrait as default orientation
		supportsOrientationPortrait = YES;
		supportsOrientationPortraitUpsideDown = NO;
		supportsOrientationLandscapeLeft = NO;
		supportsOrientationLandscapeRight = NO;
	}
	return self;
}

- (void)loadView
{	
	// Setup root view as an instance of MagnetiOSView
	MagnetiOSAppDelegate *delegate = (MagnetiOSAppDelegate*)[[UIApplication sharedApplication] delegate];
		  
	self.view = [[MagnetiOSView alloc] initWithFrame:delegate.window.bounds];
}

- (void)viewDidLoad
{
	// Initialise root view
	[(MagnetiOSView*)self.view setContext:static_cast<Magnet::iOSRenderer*>(g_pRenderer)->getNativeContext()];
	[(MagnetiOSView*)self.view setFramebuffer];
	
	// Reshape renderer
	g_pRenderer->reshape();
	
	// Load shaders if we're using ES 2
	//if ([self.context API] == kEAGLRenderingAPIOpenGLES2)
	//	[self loadShaders];
	
	// (Re)Initialise display link stuff
	frameInterval = 1;
	displayLink = nil;
}
	
- (void)viewWillAppear:(BOOL)animated
{
	[self setActive:YES];
	[super viewWillAppear:(BOOL)animated];
}
		
- (void)viewWillDisappear:(BOOL)animated
{
	[self setActive:NO];
	[super viewWillDisappear:(BOOL)animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	switch (interfaceOrientation)
	{
		case UIInterfaceOrientationPortrait: 
			return supportsOrientationPortrait; 
			break;
			
		case UIInterfaceOrientationPortraitUpsideDown: 
			return supportsOrientationPortraitUpsideDown;
			break;
			
		case UIInterfaceOrientationLandscapeLeft:
			return supportsOrientationLandscapeLeft;
			break;
			
		case UIInterfaceOrientationLandscapeRight:
			return supportsOrientationLandscapeRight;
			break;
	}
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	// Only do this stuff if we've gone from horizontal to vertical
	if (UIInterfaceOrientationIsPortrait([self interfaceOrientation]) != (UIInterfaceOrientationIsPortrait(fromInterfaceOrientation)))
	{
		// Re-create buffers
		[(MagnetiOSView*)self.view resetFramebuffer];
		
		// Reshape renderer
		g_pRenderer->reshape();
	}
}

- (void)dealloc
{
	[(MagnetiOSView*)self.view release];
	[super dealloc];
}
		
- (void)setActive:(BOOL)active
{
	// Activate
	if (active)
	{
		// Setup display link
		self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(processFrame)];
		[self.displayLink setFrameInterval:frameInterval];
		[self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	}
	
	// Deactivate
	else 
	{
		// Invalidate display link
		[self.displayLink invalidate];
		self.displayLink = nil;	
	}	
}

- (BOOL)isActive
{
	return (self.displayLink != nil);
}
	
- (void)processFrame
{
	// Timing variables
	static double timeStamp = 0.0f;
	static double lastFrameTime = 0.0f;
	static double frameStartTime = 0.0f;
	
	// Timing
	timeStamp = g_pApplication->getAbsoluteTime();
	if (frameStartTime != 0.0f) // Skip on first frame
		lastFrameTime = timeStamp - frameStartTime;
	frameStartTime = timeStamp;
		
	// Dispatch any scheduled events
	g_pEventManager->dispatchScheduledEvents();
	
	// Update game world
	g_pGameWorld->update(lastFrameTime);

	// Draw frame
	g_pRenderer->render();
}

@end
