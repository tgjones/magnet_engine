#include <MagnetiOS/common.h>

void Magnet::iOSRenderer::startup()
{	
	// TODO: Move over to ES 2 which will require shader methods in OpenGLRenderer

	// Create ES context
	m_pContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
	if (!m_pContext)
		throw RuntimeError("Failed to create OpenGL ES 1.1 context");
	
	// Try and set context current
	else if (![EAGLContext setCurrentContext:m_pContext])
		throw RuntimeError("Failed to set OpenGL ES context current");
	
	// Super-class startup stuff
	OpenGLRenderer::startup();
}

void Magnet::iOSRenderer::shutdown()
{	
	// Tear down ES context
	if ([EAGLContext currentContext] == m_pContext)
		[EAGLContext setCurrentContext:nil];
	
	// Delete context
	[m_pContext release];
}

void Magnet::iOSRenderer::render()
{
	// Prepare frame buffer
    [(MagnetiOSView*)static_cast<Magnet::iOSApplication*>(g_pApplication)->getViewController().view setFramebuffer];	
	
	// Cross-platform code
	OpenGLRenderer::render();
	
	// Swap frame buffers
    [(MagnetiOSView*)static_cast<Magnet::iOSApplication*>(g_pApplication)->getViewController().view presentFramebuffer];		
}
