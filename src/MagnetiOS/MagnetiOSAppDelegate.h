#ifndef _MAGNET_IOS_MAGNET_IOS_APP_DELEGATE_H
#define _MAGNET_IOS_MAGNET_IOS_APP_DELEGATE_H

#import <MagnetiOS/common.h>

@interface MagnetiOSAppDelegate : NSObject <UIApplicationDelegate> 
{
	UIWindow* window;
	
	Magnet::iOSApplication* m_pApplication;
}

@property (nonatomic, retain) UIWindow* window;

@property (nonatomic, retain) MagnetiOSViewController* vc; 

@end

#endif
