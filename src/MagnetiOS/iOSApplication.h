#ifndef _MAGNET_IOS_IOS_APPLICATION_H
#define _MAGNET_IOS_IOS_APPLICATION_H

#import <MagnetiOS/common.h>

@class MagnetiOSViewController;

namespace Magnet
{
	class iOSApplication : public Application
	{
	public:
		
		void startup();
		
		void shutdown();
		
		fs::path getResourceRoot();
		
		double getAbsoluteTime();
		
		uint16_t getPixelWidth();
		
		uint16_t getPixelHeight();
		
		inline MagnetiOSViewController* getViewController() { ASSERT(m_pViewController); return m_pViewController; }
		
	protected:
		
		friend class Core;
		
		inline iOSApplication() : m_pViewController(nil) {}
		
	private:
		
		MagnetiOSViewController* m_pViewController;
	};
}

#endif