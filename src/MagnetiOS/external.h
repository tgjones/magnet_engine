#ifndef _MAGNET_IOS_EXTERNAL_H
#define _MAGNET_IOS_EXTERNAL_H

#import <MagnetUtil/config.h>

// C standard library
#import <cstdlib>

// C++ standard library
#import <typeinfo>
#import <sstream>

// Other C++ headers
#include <cxxabi.h>

// Essential Apple SDK headers
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Quartz
#import <QuartzCore/QuartzCore.h>

// OpenGL ES
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#endif