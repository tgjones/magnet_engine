#ifndef _MAGNET_IOS_MAGNET_IOS_VIEW_CONTROLLER_H
#define _MAGNET_IOS_MAGNET_IOS_VIEW_CONTROLLER_H

#import <MagnetiOS/common.h>

// Magnet::iOSRenderer forward declaration
namespace Magnet { class iOSRenderer; }

@interface MagnetiOSViewController : UIViewController 
{
@private
	
	NSUInteger frameInterval;
	CADisplayLink *displayLink;
	
	BOOL supportsOrientationPortrait;
	BOOL supportsOrientationPortraitUpsideDown;
	BOOL supportsOrientationLandscapeLeft;
	BOOL supportsOrientationLandscapeRight;
}

@property (nonatomic) NSUInteger frameInterval;
@property (nonatomic, assign) CADisplayLink *displayLink;

@property (nonatomic, assign, getter=supportsOrientationPortrait) BOOL supportsOrientationPortrait;
@property (nonatomic, assign, getter=supportsOrientationPortraitUpsideDown) BOOL supportsOrientationPortraitUpsideDown;
@property (nonatomic, assign, getter=supportsOrientationLandscapeLeft) BOOL supportsOrientationLandscapeLeft;
@property (nonatomic, assign, getter=supportsOrientationLandscapeRight) BOOL supportsOrientationLandscapeRight;

@property (nonatomic, readonly, getter=currentOrientation) UIInterfaceOrientation currentOrientation;

- (void)setActive:(BOOL)active;
- (BOOL)isActive;

@end

#endif