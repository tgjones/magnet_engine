#ifndef _MAGNET_IOS_IOS_INPUT_ENGINE
#define _MAGNET_IOS_IOS_INPUT_ENGINE

#include <MagnetiOS/common.h>

namespace Magnet
{
	class iOSInputEngine : public InputEngine
	{
	public:
		
		void startup();
		
		void shutdown();
		
		void update();
	};
}

#endif