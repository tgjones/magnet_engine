#ifndef _MONKEY_IOS_APPLICATION_H
#define _MONKEY_IOS_APPLICATION_H

#import <MagnetiOS/common.h>

class MonkeyiOSApplication : public Magnet::iOSApplication
{
public:
	
	virtual void onPostStartup();
		
	virtual void onPostEngineStartup();
};

#endif