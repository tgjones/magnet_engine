#include <MagnetWin32/common.h>
#include "Win32InputEventReceiver.h"

using namespace Magnet;

void Win32InputEventReceiver::onEvent(Event& event)
{
	// Toggle the renderer
	if ((event.getDirective() == "input:toggleRenderer") &&
		((int)event.getArgument("mode") == (int)Magnet::IM_DIGITAL) &&
		((bool)event.getArgument("depressed")))
	{
		// Down cast application pointer
		Magnet::Win32Application* pApplication = static_cast<Magnet::Win32Application*>(g_pApplication);

		// Toggle the renderer between Direct3D 9 and OpenGL
		switch (pApplication->getRendererType())
		{
		case Magnet::WIN32_RENDERER_D3D9:
			pApplication->setRenderer(Magnet::WIN32_RENDERER_OPENGL);
			break;

		case Magnet::WIN32_RENDERER_OPENGL:
			pApplication->setRenderer(Magnet::WIN32_RENDERER_D3D9);
			break;

		case Magnet::WIN32_RENDERER_UNSPECIFIED:
		default:
			throw Magnet::RuntimeError("Unable to switch Win32 renderer");
		}
	}
}