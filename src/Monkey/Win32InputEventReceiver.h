#ifndef _MONKEY_WIN32_INPUT_HANDLER_H
#define _MONKEY_WIN32_INPUT_HANDLER_H

#include <MagnetWin32/common.h>

class Win32InputEventReceiver : public Magnet::EventReceiver
{
public:

	// Called by g_pEventManager to update input variables
	void onEvent(Magnet::Event& event);
};

#endif