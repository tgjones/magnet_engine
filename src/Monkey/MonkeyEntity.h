#ifndef _MONKEY_MONKEY_ENTITY_H
#define _MONKEY_MONKEY_ENTITY_H

#include <MagnetUtil/common.h>

class MonkeyEntity : public Magnet::DirectActorEntity
{
public:

	MonkeyEntity();

protected:

	// Entity::onUpdate() is run every frame
	void onUpdate(double lastFrameTime);
};

#endif