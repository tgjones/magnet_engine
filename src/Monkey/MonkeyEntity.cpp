#include <MagnetUtil/common.h>
#include "MonkeyEntity.h"

using namespace Magnet;

MonkeyEntity::MonkeyEntity()
{
	// Set the DirectActorEntity asset ID from media.xml
	this->setAsset("monkey");
}  

void MonkeyEntity::onUpdate(double lastFrameTime)
{ 
	// Spin the monkey relative to the elapsed time
	this->getOrientation() *= Matrix4x4::createRotationY((float)lastFrameTime);

	// Rotate camera
	static float cameraRoll = 0.f;
	g_pRenderer->getScreen()->getCamera()->setRoll(cameraRoll += (float)lastFrameTime);
}