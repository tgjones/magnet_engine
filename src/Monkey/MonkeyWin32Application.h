#ifndef _MONKEY_WIN32_APPLICATION_H
#define _MONKEY_WIN32_APPLICATION_H

#include <MagnetWin32/common.h>
#include "Win32InputEventReceiver.h"

class MonkeyWin32Application : public Magnet::Win32Application
{
public:

	virtual void onPostEngineStartup();

private:

	Win32InputEventReceiver m_inputEventReceiver;
};

#endif