#include "MonkeyWin32Application.h"
#include "MonkeyEntity.h"

#define _MAG_APP_CLASS MonkeyWin32Application
#include <MagnetUtil/main.h>

using namespace Magnet;

void MonkeyWin32Application::onPostEngineStartup()
{
	// Setup ambient light to game world
	AmbientLightEntity* pAmbientLight = new AmbientLightEntity();
	pAmbientLight->color.r = 0.1f;
	pAmbientLight->color.g = 0.1f;
	pAmbientLight->color.b = 0.1f;
	g_pGameWorld->spawnEntity(pAmbientLight);

	// Add directional light to game world
	DirectionalLightEntity* pDirectionalLight = new DirectionalLightEntity();
	pDirectionalLight->direction.x = 0.0f;
	pDirectionalLight->direction.y = 0.0f;
	pDirectionalLight->direction.z = -1.0f;
	pDirectionalLight->diffuse.r = 0.5f;
	pDirectionalLight->diffuse.g = 0.5f;
	pDirectionalLight->diffuse.b = 0.5f;
	pDirectionalLight->specular.r = 0.2f;
	pDirectionalLight->specular.g = 0.2f;
	pDirectionalLight->specular.b = 0.8f;
	g_pGameWorld->spawnEntity(pDirectionalLight);

	// Add the monkey to game world
	g_pGameWorld->spawnEntity(new MonkeyEntity());

	// Register interest in input events
	g_pEventManager->registerReceiver("input:toggleRenderer", m_inputEventReceiver);
}