#import "MonkeyiOSApplication.h"
#import "MonkeyEntity.h"

#define _MAG_APP_CLASS MonkeyiOSApplication
#include <MagnetUtil/main.h>

void MonkeyiOSApplication::onPostStartup()
{
	Magnet::iOSApplication* pApp = static_cast<Magnet::iOSApplication*>(g_pApplication);
	
	// Supports landscape orientations only
	// TODO: Put this in a config file
	[pApp->getViewController() setSupportsOrientationPortrait:NO];
	[pApp->getViewController() setSupportsOrientationPortraitUpsideDown:NO];
	[pApp->getViewController() setSupportsOrientationLandscapeLeft:YES];
	[pApp->getViewController() setSupportsOrientationLandscapeRight:YES];
}

void MonkeyiOSApplication::onPostEngineStartup()
{

	// Setup ambient light to game world
	Magnet::AmbientLightEntity* pAmbientLight = new Magnet::AmbientLightEntity();
	pAmbientLight->color.r = 0.1f;
	pAmbientLight->color.g = 0.1f;
	pAmbientLight->color.b = 0.1f;
	g_pGameWorld->spawnEntity(pAmbientLight);
	
	// Add directional light to game world
	Magnet::DirectionalLightEntity* pDirectionalLight = new Magnet::DirectionalLightEntity();
	pDirectionalLight->direction.x = 0.0f;
	pDirectionalLight->direction.y = 0.0f;
	pDirectionalLight->direction.z = -1.0f;
	pDirectionalLight->diffuse.r = 0.5f;
	pDirectionalLight->diffuse.g = 0.5f;
	pDirectionalLight->diffuse.b = 0.5f;
	pDirectionalLight->specular.r = 0.2f;
	pDirectionalLight->specular.g = 0.2f;
	pDirectionalLight->specular.b = 0.8f;
	g_pGameWorld->spawnEntity(pDirectionalLight);
	
	// Add the monkey to game world
	g_pGameWorld->spawnEntity(new MonkeyEntity());
}