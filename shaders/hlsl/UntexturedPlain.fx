// Transforms
float4x4 g_world                 : WORLD < string UIWidget = "None"; >;
float4x4 g_worldInverseTranspose : WORLDINVERSETRANSPOSE < string UIWidget = "None"; >;
float4x4 g_worldViewProjection   : WORLDVIEWPROJECTION < string UIWidget = "None"; >;

// Materials
float4 g_ambientMaterial  : COLOR < string UIName = "Ambient Material"; string UIWidget = "Color"; > = {0.1, 0.1, 0.1, 1};
float4 g_diffuseMaterial  : COLOR < string UIName = "Diffuse Material"; string UIWidget = "Color"; > = {1, 1, 1, 1};
float4 g_specularMaterial : COLOR < string UIName = "Specular Material"; string UIWidget = "Color"; > = {1, 1, 1, 1};

// Lights
float4 g_ambientLight  : AMBIENT < string UIWidget = "None"; string Object = "AmbientLight"; > = {0.1, 0.1, 0.1, 1};
float4 g_diffuseLight  : DIFFUSE < string UIWidget = "None"; string Object = "Light4"; > = {1, 1, 1, 1};
float4 g_specularLight : SPECULAR <	string UIWidget = "None"; string Object = "SpecularLight"; > = {1, 1, 1, 1};
	
// Specular power
float  g_specularPower < string UIWidget = "slider"; float UIMin = 0.0; float UIMax = 1.0; 
	float UIStep = 0.05; string UIName =  "Specular"; >;
	

float3 g_lightDirectionW : DIRECTION < string UIName = "Light Direction"; >;

float3 g_eyePosW : POSITION < string UIName = "Eye Position"; >;

// Vertex shader output struct
struct OutputVS
{
	float4 posH   : POSITION0;
	float4 color  : COLOR0;
};

// Vertex shader
OutputVS mainVS(float3 posL : POSITION0, float3 normalL : NORMAL0)
{
	OutputVS outputVS = (OutputVS)0;
	
	// Transform normal to world space
	float3 normalW = mul(float4(normalL, 0.0f), g_worldInverseTranspose).xyz;
	normalW = normalize(normalW);
	
	// Transform vertex position to world space
	float posW = mul(float4(posL, 1.0f), g_world).xyz;
	
	// Computer vector from the vertex to the eye position
	float3 toEye = normalize(g_eyePosW - posW);
	
	// Compute the reflection vector
	float3 r = reflect(-g_lightDirectionW, normalW);
	
	// Compute how much (if any) specular light makes it into the eye
	float t = pow(max(dot(r, toEye), 0.0f), g_specularPower);
	
	// Compute diffuse light intensity that strikes the vertex
	float s = max(dot(g_lightDirectionW, normalW), 0.0f);
	
	// Compute the different terms
	float3 specular = t * (g_specularMaterial * g_specularLight).rgb;
	float3 diffuse  = s * (g_diffuseMaterial * g_diffuseLight).rgb;
	float3 ambient  = g_ambientMaterial * g_ambientLight;
	
	// Sum terms and copy diffuse alpha
	outputVS.color.rgb = ambient + diffuse + specular;
	outputVS.color.a   = g_diffuseMaterial.a;
	
	// Transform vertex position to clip space
	outputVS.posH = mul(float4(posL.xyz, 1.0), g_worldViewProjection);
	
	// Return position and colour
	return outputVS;
}

// Pixel shader
float4 mainPS(float4 color : COLOR0) : COLOR
{
	// Pass-through pixel colour
	return color;
}

// Technique
Technique main
{
	Pass p0 
	{
		FillMode = Solid;
		VertexShader = compile vs_3_0 mainVS();
		PixelShader = compile ps_3_0 mainPS();
	}
}
