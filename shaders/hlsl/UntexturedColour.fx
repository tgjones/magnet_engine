/// Matrices
float4x4 g_mWorld                 : World;
float4x4 g_mWorldInverseTranspose : WorldInverseTranspose;
float4x4 g_mWorldViewProjection   : WorldViewProjection;

// TODO!!!!

float4 mainVS(float3 pos0 : POSITION0) : POSITION0
{
	return mul(float4(pos0.xyz, 1.0), g_mWorldViewProjection);
}

float4 mainPS() : COLOR 
{
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
}

Technique main
{
	Pass p0 
	{
		FillMode = Solid;
		VertexShader = compile vs_2_0 mainVS();
		PixelShader = compile ps_2_0 mainPS();
	}
}
